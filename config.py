import os
basefir = os.path.abspath(os.path.dirname(__file__))

class Config(object):
    DEBUG = False
    TESTING = False
    CSRF_ENABLED = True
    SECRET_KEY = 'mmcsm_na_web_is_awesome'
    VIRTUALENV_PATH = '/home/ymyung/anaconda3/envs/mmcsm_na'

class ProductionConfig(Config):
    VIRTUALENV_PATH = '/home/ymyung/anaconda3/envs/mmcsm_na'
    DEBUG = True

class DevelopmentConfig(Config):
    DEVELOPMENT = True

class TestingConfig(Config):
    TESTING = True
