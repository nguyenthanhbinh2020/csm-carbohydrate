from flask import Flask, request, session, redirect, url_for, abort, render_template, flash
from rq import Queue
from worker_single import conn
from flask_restful import Api

# ============= #
# CONFIGURATION #
# ============= #

JOB_TIMEOUT = 86400 # 24 hours
DEBUG = True

# DEFINE THE APP
app = Flask(__name__)
api = Api(app)

app.secret_key = 'csm_carbohydrate_web_is_awesome'
app.config.from_object('config.DevelopmentConfig')

UPLOAD_FOLDER = app.root_path + '/predictions/data'
CODE_FOLDER = app.root_path + '/predictions/code'
RESULT_FOLDER = app.root_path + '/static/results_file'
MODEL_FOLDER = app.root_path + '/predictions/code/models'
GB_MODEL = app.root_path + "/predictions/code/models/gb_model.sav"
DEFAULT_FOLDER = app.root_path
EXAMPLE_FOLDER = app.root_path + '/static/example'
PYMOL='/home/tbnguyen/software/pymol/pymol'
#FOLDX_FOLDER = '/home/ymyung/foldx'
#ROTABASE_FILE = '/home/ymyung/foldx/rotabase.txt'
#NRGTEN_ENVIRONMENT = '/home/ymyung/anaconda3/envs/nrgten'

app.config['UPLOAD_FOLDER'] = UPLOAD_FOLDER
app.config['CODE_FOLDER'] = CODE_FOLDER
app.config['MODEL_FOLDER'] = MODEL_FOLDER
app.config['EXAMPLE_FOLDER'] = EXAMPLE_FOLDER
app.config['JOB_TIMEOUT'] = JOB_TIMEOUT
app.config['RESULT_FOLDER'] = RESULT_FOLDER
app.config['GB_MODEL'] = GB_MODEL
app.config['PYMOL'] = PYMOL

#app.config['FOLDX_FOLDER'] = FOLDX_FOLDER
#app.config['ROTABASE_FILE_LOCATION'] = ROTABASE_FILE
#app.config['NRGTEN_ENVIRONMENT'] = NRGTEN_ENVIRONMENT

app.config["MAIL_SERVER"] = "smtp.gmail.com"
app.config["MAIL_PORT"] = 465
app.config["MAIL_USE_SSL"] = True
app.config["MAIL_USERNAME"] = 'biosigab@gmail.com'
app.config["MAIL_PASSWORD"] = 'Dm9QkUcamiYfoa'
app.config["JOB_TIMEOUT"] = JOB_TIMEOUT

#queue_design = Queue("mmcsm-na-design", connection=conn)
queue_single = Queue("csm-carbohydrate",connection=conn)

# ADD VIEWS
from csm_carbohydrate_app import main
