from Bio.PDB.Polypeptide import one_to_three, three_to_one
from flask_restful import Resource, reqparse
from flask import request, url_for, jsonify

import os
import pickle
import re
import time
import werkzeug

from csm_carbohydrate_app.utils import *
from csm_carbohydrate_app import app
from csm_carbohydrate_app.jobs import run_job
TAG_RE = re.compile(r'<[^>]+>')

ALLOWED_EXTENSIONS_PDB = set(['pdb'])
ALLOWED_EXTENSIONS_LIST = set(['txt','csv'])

def allowed_file_pdb(filename):
    return '.' in filename and filename.rsplit('.', 1)[1].lower() in ALLOWED_EXTENSIONS_PDB

def allowed_file_list(filename):
    return '.' in filename and filename.rsplit('.', 1)[1].lower() in ALLOWED_EXTENSIONS_LIST

class SinglePredictionApi(Resource):
    def post(self):
        parser = reqparse.RequestParser()
        parser.add_argument('pdb_file',type=file,default='',required=False,help="PDB file with the atom coordinates information of the protein-carbohydrate complex.")
        parser.add_argument('pdb_code',type=str,default='',required=False,help="PDB code")
        parser.add_argument('smiles_file',type=file,default='',required=False,help="SMILE format file of carbohydrates")
        parser.add_argument('smiles',type=str,default='',required=False,help="SMILE format of carbohydrates.")
        parser.add_argument('carbohydrate_chains',type=str,default='',required=True,help='Required, chain of carbohydrate')
        parser.add_argument('carbohydrate_names',type=str,default='',required=True,help='Required, names of carbohydrate seperated by "_"')
        parser.add_argument('email',type=str,required=False,default='',help="If provided, an message will be went when the result is ready.")
        args = parser.parse_args(strict=True)

        pdb_file = ''
        pdb_code = ''
        pdb_code = args.pdb_code
        smiles_file = '' 
        smiles_str = '' 
        smiles_str = args.smiles

        CHO_chain = args.carbohydrate_chains
        CHO_name = args.carbohydrate_names

        if pdb_code == '':
            pdb_file = request.files['pdb_file']
        if smiles_str == '':
            smiles_file = request.files['smiles_file']

        if pdb_file or pdb_code or smiles_file or smiles_str:
            email = args.email
        #if args.carbohydrate_chains and args.carbohydrate_names:
        if (pdb_file and smiles_file and CHO_chain and CHO_name) or (pdb_code and smiles_file and CHO_chain and CHO_name) or (pdb_file and smiles_str and CHO_chain and CHO_name) or (pdb_code and smiles_str and CHO_chain and CHO_name) :
            run_id = str(time.time()).replace('.','')
            cmd = "mkdir " + app.config['UPLOAD_FOLDER'] + "/" + str(run_id)
            os.system(cmd)
            folder = app.config['UPLOAD_FOLDER'] + "/" + str(run_id)

            if pdb_code:
                #pdb_code = pdb_code.replace(" ", "")
                pdb_code = pdb_code.upper()

                if downloadPDBfromRCSB(pdb_code, folder, 'wt.pdb') !=True:
                    msg = []
                    msg.append("Wrong PDB accession CODE was given")

                    return render_template('prediction_error.html', msg=msg)
            else:
                pdb_file.save(os.path.join(folder, "wt.pdb"))

            pdb = folder + '/wt.pdb'

            # Check number of chains
            num_chains = check_num_chains(pdb)
            if num_chains == 0:
                msg = []
                msg.append("No chains were found in the provided PDB file.")
                msg.append("Please provide a <b>PDB</b> file with proper chain identifiers.")
                return render_template('prediction_error.html', msg=msg)

            # Check PDB format
            format_ok = check_format(pdb)
            if format_ok == 0:
                msg = []
                msg.append("Provided file does not appear to be in PDB format.")
                msg.append(
                    "Please provide a valid <b>PDB</b> file, according to: <a href='http://www.wwpdb.org/docs.html'>http://www.wwpdb.org/docs.html</a>")
                return render_template('prediction_error.html', msg=msg)

            # Check multiple models
            models_ok = check_models(pdb)
            if models_ok > 1:
                msg = []
                msg.append("Provided PDB file has <b>multiple models</b>.")
                msg.append("Please provide a PDB file with a <i>single model</i>.")
                return render_template('prediction_error.html', msg=msg)
		    # ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
            if smiles_file or smiles_str:
                smiles_local_filename = run_id + ".smi"
                smiles_local_file = folder + "/" + smiles_local_filename

                num_smiles = 0
                # Process input smiles (either single smiles or file)
                if smiles_file:
                    filename = secure_filename(smiles_file.filename)
                    smiles_file.save(os.path.join(folder, smiles_local_filename))								
                    # Filters only first column
                    cmd = "awk '{print $1}' " + smiles_local_file + " > " + smiles_local_file + ".tmp ; mv " + smiles_local_file  + ".tmp " + smiles_local_file
                    os.system(cmd)

                    # Checking input file limits
                    cmd = "wc -l " + smiles_local_file + " | awk '{print $1}' "
                    num_smiles = int(subprocess.check_output(cmd, shell=True).strip())
					     
                    if num_smiles > 1:
                        msg = []
                        msg.append("Input files are limited to 1 SMILES.")
                        return render_template('prediction_error.html',msg=msg)
					     
                    if num_smiles == 0:
                        msg = []
                        msg.append("Please provide a valid SMILES file.")
                        return render_template('prediction_error.html',msg=msg)

                else:
                    num_smiles = 1
                    # Prints smiles to file
                    cmd = "echo \"SMILES\" > " + smiles_local_file + " ; echo \"" + smiles_str + "\" >> " + smiles_local_file
                    os.system(cmd)
                # Calculating properties
				     
                error_smiles = 0
				     
                # Exclude smiles that don't load properly in RDKit
                smiles_local_file_ok = smiles_local_file + ".ok"
                cmd = "cp " + smiles_local_file + " " + smiles_local_file_ok
                os.system(cmd)

                with open(smiles_local_file_ok) as my_smi_file:
							      smi_=my_smi_file.read()
							      smi=smi_.rstrip().split('\n')
							      my_smi_file.close()

                with open(smiles_local_file, 'w') as my_smi_ok_file:
                    for i in range(len(smi)):
                        if i==0:
                            my_smi_ok_file.write("%s\n" % smi[i])
                        else:
                            my_smi_ok_file.write("%s\n" % smi[i])	
                    my_smi_ok_file.flush()
                    my_smi_ok_file.close()

                if num_smiles == 0:
                    msg = []
                    msg.append("Please provide a valid SMILES file.")
                    return render_template('prediction_error.html',msg=msg)
	    
		    # ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
            infos_pickle = os.path.join(folder, "infos.p")
            #print ('infos_pickle', infos_pickle)
            infos = {'pdb':pdb,'job_status': 'NOT_READY', 'carbohydrate_names':CHO_name, 'carbohydrate_chains': CHO_chain, \
                    'email':email,'smiles':smi,'num_smiles':num_smiles, 'run_id': str(run_id), 'working_dir': folder}
            pickle.dump(infos, open(infos_pickle, 'wb'), protocol=2)

            job = queue_single.enqueue_call(func=run_job, args=(pdb, CHO_chain, CHO_name, smiles_local_file, run_id, infos_pickle),result_ttl=0,timeout=app.config['JOB_TIMEOUT'],job_id=str(run_id))
            #print ('input******', pdb, CHO_chain, CHO_name, smiles_local_file, run_id, infos_pickle)
            #job = queue_single.enqueue_call(func=job_additive_prediction, args=(pdb, infos_pickle, mutation_list, predtype),result_ttl=0,timeout=app.config['JOB_TIMEOUT'],job_id=str(run_id))

            return redirect(url_for('results_prediction', job_id=run_id))
        else:
            msg = []
            msg.append("Please provide a <b>PDB</b> file and a <b>mutation list</b>.")
            return render_template('prediction_error.html', msg=msg)

    def get(self):
        parser = reqparse.RequestParser()
        parser.add_argument('job_id',type=str,required=True,help='')
        args = parser.parse_args(strict=True)

        upload_infos = os.path.join(app.config['UPLOAD_FOLDER'] + '/' + args.job_id, 'infos.p')

        # Check Job Status in working directoriy
        try:
            working_infos = pickle.load(open(upload_infos, 'rb'))

            '''if working_infos['job_status'] == 'NOT_READY':
                if not job_still_processing(args.job_id,'fast'):
                    return {"job_id":args.job_id,"status":"ERROR"}
                else:
                    return {"job_id":args.job_id,"status":"PROCESSING"}
            '''
            if not working_infos['job_status'] == 'NOT_READY':
                folder = os.path.join(app.config['RESULT_FOLDER'], args.job_id)
                results_pickle = os.path.join(folder, "infos.p")
                results = pickle.load(open(results_pickle, 'rb'))

                return {"prediction":results['result'].round(2).tolist(), 
                        "smiles":results['smiles']
                        }
        except (IOError):
            return {"job_id":args.job_id,"status":"NOT EXIST"}



