import numpy as np
from subprocess import Popen, PIPE
from itertools import combinations
import re
import time
import pickle
import joblib
import os

start_time = time.time()
from csm_carbohydrate_app.utils import *
from csm_carbohydrate_app import Arpeggio_analyzer as Arana
#from csm_carbohydrate_app.predictions.code.distance import getClosestDistance
#from csm_carbohydrate_app.predictions.code.get_scores_webserver import get_aaindex_scores
#from csm_carbohydrate_app.predictions.code.distance_multiple_mutations_webserver import getDistanceBwMuts


np.warnings.filterwarnings('ignore')  # temporary
FNULL = open(os.devnull, 'w')
# The following slope and intercept are for Linear Transformation
transform_slope = 1
transform_intercept = 0

def run_job(pdb_local_file, CHO_chain, CHO_name, smiles_local_file, job_id, infos_pickle):#, mutation_list, predtype):

	wild_arpeggio_contResidues = []
	wild_arpeggio_PI = []
	wild_arpeggio_PIResidues = []
	work_dir = app.config['UPLOAD_FOLDER'] + '/' + job_id
	print("**********Started running JOB***********")
	CHO_name_combine = CHO_name
	infos = pickle.load(open(infos_pickle,'rb'))
	#print (1, infos_pickle)
	# Generate interaction from Arpeggio
	print("**********Started running Arpeggio***********")
	cmd = "python " + app.config['CODE_FOLDER'] + "/Arpeggio/arpeggio.py " + pdb_local_file + " -s /" + CHO_chain + "//"
	os.system(cmd)
	pdb_name = pdb_local_file.split(".pdb")[0]
	#print ("**********", pdb_name)

	cmd = 'mkdir ' + app.config['RESULT_FOLDER'] + '/' + job_id
	os.system(cmd)

	cmd = 'cp ' + pdb_name + ".pdb " + app.config['RESULT_FOLDER'] + '/' + job_id
	os.system(cmd)

	cmd = "python " + app.config['CODE_FOLDER'] + "/Arpeggio/write_list_file_Arana.py " + pdb_name + ".contacts " + pdb_name + ".Arana"
	os.system(cmd)

	cmd = "python " + app.config['CODE_FOLDER'] + "/Arpeggio/Arpeggio_analyzer_v2_infile.py " + pdb_name + ".Arana " + pdb_name + ".Arana.out INTER True"
	os.system(cmd)

	# Generate pse file
	cmd = "python " + app.config['CODE_FOLDER'] + "/Arpeggio/show_contacts.py -s " + pdb_name + ".pdb -bs" 
	os.system(cmd)

	# taken carbohydrate from protein-carbohydrate complex
	cmd = "python " + app.config['CODE_FOLDER'] + "/taken_carbohydrate.py " + pdb_local_file + " " + CHO_name_combine + " " + CHO_chain
	#print (cmd)
	os.system(cmd)


	# Generate signature by csm-lig
	print("**********Started running csm-lig***********")
	cmd = "python " + app.config['CODE_FOLDER'] + "/csm-lig/CSM-lig_Binh.py " + pdb_local_file + " " + smiles_local_file + " " + pdb_name + ".csm-lig " + app.config['CODE_FOLDER'] + "/csm-lig/ 10"
	os.system(cmd)

	# Generate signature by pdcsm
	print("**********Started running pdcsm***********")
		
	cmd = "python " + app.config['CODE_FOLDER'] + "/pdCSM/add_name_to_smi_file.py " + smiles_local_file + " " + pdb_name + "_.smi"
	os.system(cmd)

	cmd = "perl " + app.config['CODE_FOLDER'] + "/pdCSM/pdCSM_v6.pl " + pdb_name + "_.smi " + pdb_name + ".pdCSM  0.5 1 " + app.config['CODE_FOLDER'] + "/pdCSM/"
	#print (cmd)
	os.system(cmd)
	 
	print("**********Finished running graph signatures***********")
	
	# -----------------------------------------------------------------------------
	
	print("**********Get greedy features***********")
	cmd = "python " + app.config['CODE_FOLDER'] + "/combine_all_features.py " + pdb_name + ".Arana.out " + pdb_name + ".pdCSM " + pdb_name + ".csm-lig " + pdb_name + ".all_features"
	#print (cmd)
	os.system(cmd)

	all_feature_file = pdb_name + ".all_features"
	chosen_feature_file = pdb_name + ".chosen_features"
	
	cmd = "python " + app.config['CODE_FOLDER'] + "/chosen_features_from_greedy.py " + all_feature_file + " " + app.config['CODE_FOLDER'] + "/greedy_fs_GB.csv 20 " + chosen_feature_file
	#print (cmd)
	os.system(cmd)

	# -----------------------------------------------------------------------------
	print("**********Makes predictions***********")

	# Runs predictions
	features_chosen = pd.read_csv(chosen_feature_file, sep=',', quotechar='\'')
	GB_model = joblib.load(app.config['GB_MODEL'])
	#print("features_chosen", (features_chosen.to_dict()))

	#print("**********")
	prediction_GB = round(GB_model.predict(features_chosen), 2)
	print("**********Done predictions***********")
	#print("**********predictions***********", prediction_GB)
	
	#generate pse file   
	#pml2pse(pdb_name + ".pml")
	
	cmd = app.config['PYMOL'] + ' ' + pdb_name+'.pml'
	#print (cmd)
	os.system(cmd)

	#cmd = 'mv wt.pse ' + pdb_name+'.pse'
	#print (cmd)
	#os.system(cmd)

	cmd = "zip -j " + app.config['RESULT_FOLDER'] + '/' + job_id  + '/' + job_id + '.zip ' + pdb_name+'.pse'
   	print (cmd) 
	os.system(cmd)

	#result_data = result_data.append({'DG':prediction_GB})
	completed_infos = pickle.load(open(infos_pickle,'rb')) # Open the latest pickle file.
	#print ('completed_infos', completed_infos)
	completed_infos['result'] = prediction_GB
	completed_infos['run_id'] = job_id
	completed_infos['CHO_chain'] = CHO_chain
	completed_infos['CHO_name'] = CHO_name

	print ("CHO_chain, CHO_name", CHO_chain, CHO_name)

	wild_arpeggio_contResidues= Arana.getContactsViewer(work_dir) #(pdb_name + '.contacts')
	wild_arpeggio_PI,wild_arpeggio_PIResidues = Arana.getALLPI(work_dir) #(pdb_name +  '.ari')

    # Calculate feature for prediction page
	Arpeggio_features = (pd.read_csv(pdb_name + ".Arana.out", sep=',', quotechar='\'')).to_dict()
	'''print("Arpeggio_features", Arpeggio_features)
	final_Arpeggio_features = {}

	for key in Arpeggio_features.keys():
	    final_Arpeggio_features[key] = len(Arpeggio_features[key])'''

	completed_infos['Arpeggio_features'] = Arpeggio_features
	completed_infos['Arpeggio_wild_interactions'] = wild_arpeggio_contResidues
	completed_infos['Arpeggio_wild_PIinteractions'] = wild_arpeggio_PI
	completed_infos['Arpeggio_wild_PIinteracting_residues'] = wild_arpeggio_PIResidues
	#print ('Arpeggio_wild_interactions', completed_infos['Arpeggio_wild_interactions'])

	result_data = open(app.config['RESULT_FOLDER'] + '/' + job_id  + '/' + job_id + '.csv', 'w')
	result_data.write(str(infos['smiles']) + '\t' + str(round(prediction_GB, 2)) + '\n')
	completed_infos['job_status'] = "READY"

	#result_folder = app.config['RESULT_FOLDER'] 
	#result_pickle = result_folder + '/' + job_id + '_infos.p'
	#pickle.dump(completed_infos, open(result_pickle, 'wb'))
	result_pickle = os.path.join(infos_pickle)
	pickle.dump(completed_infos, open(result_pickle, 'wb'))
	#result_data.to_csv(os.path.join(result_folder,'result_file.csv'),index=True)

	#compress_cmd = "zip -j "+result_folder+'/interactions.zip '+result_folder+'/*.pse'
	#os.system(compress_cmd)
	if infos['email']:
		result_link = 'http://biosig.unimelb.edu.au/mmcsm_na/results_prediction/{}'.format(infos['run_id'])
		send_email_results(infos['email'], results_link=result_link)
	
	return True

def job_design(pdb_file, infos_pickle, predtype):
	list_result = pd.DataFrame()

