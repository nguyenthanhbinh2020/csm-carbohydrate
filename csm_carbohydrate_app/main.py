from flask import Flask, render_template, abort, request, flash, Response, redirect, url_for, send_file
from werkzeug.utils import secure_filename
from sqlalchemy import distinct, func, asc
from sqlalchemy.orm import aliased, scoped_session, sessionmaker
from sqlalchemy.sql.expression import and_
from redis import StrictRedis
from rq import push_connection, get_failed_queue, Queue
from rq.job import Job
from rq.handlers import move_to_failed_queue
import os
import re
import simplejson
import subprocess
import sys
import time

from csm_carbohydrate_app.utils import *
import pickle
from csm_carbohydrate_app import app, queue_single, api #queue_design, api
from csm_carbohydrate_app.jobs import run_job
from Bio.PDB import PDBList, PDBParser
from csm_carbohydrate_app.api import SinglePredictionApi

# VIEW FUNCTIONS - TOP NAVIGATION BAR

# Home
@app.route('/')
def index():
	return render_template('index.html')


@app.route('/api')
def api_docs():
    return render_template('api.html')

# ---------------------------------------------------------------------------------------------------------------------
# Antibody-antigen affinity change prediction
@app.route('/prediction')
def prediction_pages():
	return render_template('prediction.html')

# ---------------------------------------------------------------------------------------------------------------------
@app.route('/thanks')
def display_thanks():
	return render_template('thanks.html')

# ---------------------------------------------------------------------------------------------------------------------
@app.route('/help')
def display_help():
	return render_template('help.html')

# ---------------------------------------------------------------------------------------------------------------------
@app.route('/contact', methods=['GET', 'POST'])
def display_contact():
	if request.method == 'POST':
		name = request.form.get('name', None)
		email = request.form.get('email', None)
		message = request.form.get('message', None)

		if email == "":
			email = "Email not provided"

		msg = Message("mmCSM-AB", sender=email, recipients=['biosigab@gmail.com'])
		msg.html = "<b>From:</b> %s<br/><b>Name:</b> %s<br/><b>Message:</b> %s<br/>" % (email, name, message)

		mail = Mail()
		mail.init_app(app)

		mail.send(msg)
		return render_template('contact.html', form_sent=True)

	elif request.method == 'GET':
		return render_template('contact.html')

@app.route('/datasets')
def display_datasets():
	return render_template('datasets.html')

@app.route('/related')
def display_related():
	return render_template('about.html')

# ---------------------------------------------------------------------------------------------------------------------
# ERRORS
@app.errorhandler(400)
def bad_request(e):
	return render_template('400.html'), 400

@app.errorhandler(403)
def forbidden(e):
	return render_template('403.html'), 403

@app.errorhandler(404)
def page_not_found(e):
	return render_template('404.html'), 404

@app.errorhandler(405)
def method_not_allowed(e):
	return render_template('405.html'), 405

@app.errorhandler(410)
def resource_gone(e):
	return render_template('410.html'), 410

@app.errorhandler(500)
def internal_server_error(e):
	return render_template('500.html'), 500
# ---------------------------------------------------------------------------------------------------------------------

@app.route('/run_single_mode',methods=['POST'])
def run_single_mode():
    predtype='single'
    pdb_file = request.files.get('pdb_file', None)
    pdb_code = request.form.get('pdb_code', None)

    smiles_file = request.files.get('smiles_file', False)
    smiles_str = request.form.get('smiles', None)

    CHO_chain = request.form.get('carbohydrate_chains',None).replace(" ","")
    CHO_name = request.form.get('carbohydrate_names',None)

    if (pdb_file and smiles_file and CHO_chain and CHO_name) or (pdb_code and smiles_file and CHO_chain and CHO_name) or (pdb_file and smiles_str and CHO_chain and CHO_name) or (pdb_code and smiles_str and CHO_chain and CHO_name) :
        email = request.form.get('email', None)

        run_id = str(time.time()).replace('.','')
        cmd = "mkdir " + app.config['UPLOAD_FOLDER'] + "/" + str(run_id)
        os.system(cmd)
        folder = app.config['UPLOAD_FOLDER'] + "/" + str(run_id)

        if pdb_code:
            #pdb_code = pdb_code.replace(" ", "")
            pdb_code = pdb_code.upper()

            if downloadPDBfromRCSB(pdb_code, folder, 'wt.pdb') !=True:
                msg = []
                msg.append("Wrong PDB accession CODE was given")

                return render_template('prediction_error.html', msg=msg)
        else:
            pdb_file.save(os.path.join(folder, "wt.pdb"))

        pdb = folder + '/wt.pdb'


        # Check number of chains
        num_chains = check_num_chains(pdb)
        if num_chains == 0:
            msg = []
            msg.append("No chains were found in the provided PDB file.")
            msg.append("Please provide a <b>PDB</b> file with proper chain identifiers.")
            return render_template('prediction_error.html', msg=msg)

        # Check PDB format
        format_ok = check_format(pdb)
        if format_ok == 0:
            msg = []
            msg.append("Provided file does not appear to be in PDB format.")
            msg.append(
                "Please provide a valid <b>PDB</b> file, according to: <a href='http://www.wwpdb.org/docs.html'>http://www.wwpdb.org/docs.html</a>")
            return render_template('prediction_error.html', msg=msg)

        # Check multiple models
        models_ok = check_models(pdb)
        if models_ok > 1:
            msg = []
            msg.append("Provided PDB file has <b>multiple models</b>.")
            msg.append("Please provide a PDB file with a <i>single model</i>.")
            return render_template('prediction_error.html', msg=msg)
		# ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
        if smiles_file or smiles_str:
            smiles_local_filename = run_id + ".smi"
            smiles_local_file = folder + "/" + smiles_local_filename

            num_smiles = 0
            # Process input smiles (either single smiles or file)
            if smiles_file:
                filename = secure_filename(smiles_file.filename)
                smiles_file.save(os.path.join(folder, smiles_local_filename))								
                # Filters only first column
                cmd = "awk '{print $1}' " + smiles_local_file + " > " + smiles_local_file + ".tmp ; mv " + smiles_local_file  + ".tmp " + smiles_local_file
                os.system(cmd)

                # Checking input file limits
                cmd = "wc -l " + smiles_local_file + " | awk '{print $1}' "
                num_smiles = int(subprocess.check_output(cmd, shell=True).strip())
					 
                if num_smiles > 1:
                    msg = []
                    msg.append("Input files are limited to 1 SMILES.")
                    return render_template('prediction_error.html',msg=msg)
					 
                if num_smiles == 0:
                    msg = []
                    msg.append("Please provide a valid SMILES file.")
                    return render_template('prediction_error.html',msg=msg)

            else:
                num_smiles = 1
                # Prints smiles to file
                cmd = "echo \"SMILES\" > " + smiles_local_file + " ; echo \"" + smiles_str + "\" >> " + smiles_local_file
                os.system(cmd)
            # Calculating properties
				 
            error_smiles = 0
				 
            # Exclude smiles that don't load properly in RDKit
            smiles_local_file_ok = smiles_local_file + ".ok"
            cmd = "cp " + smiles_local_file + " " + smiles_local_file_ok
            os.system(cmd)

            with open(smiles_local_file_ok) as my_smi_file:
							  smi_=my_smi_file.read()
							  smi=smi_.rstrip().split('\n')
							  my_smi_file.close()

            with open(smiles_local_file, 'w') as my_smi_ok_file:
                for i in range(len(smi)):
                    if i==0:
                        my_smi_ok_file.write("%s\n" % smi[i])
                    else:
                        my_smi_ok_file.write("%s\n" % smi[i])	
                my_smi_ok_file.flush()
                my_smi_ok_file.close()

            if num_smiles == 0:
                msg = []
                msg.append("Please provide a valid SMILES file.")
                return render_template('prediction_error.html',msg=msg)
	
		# ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
        infos_pickle = os.path.join(folder, "infos.p")
        #print ('infos_pickle', infos_pickle)
        infos = {'pdb':pdb,'job_status': 'NOT_READY', 'carbohydrate_names':CHO_name, 'carbohydrate_chains': CHO_chain, \
                'email':email,'smiles':smi,'num_smiles':num_smiles, 'run_id': str(run_id), 'working_dir': folder}
        pickle.dump(infos, open(infos_pickle, 'wb'), protocol=2)

        job = queue_single.enqueue_call(func=run_job, args=(pdb, CHO_chain, CHO_name, smiles_local_file, run_id, infos_pickle),result_ttl=0,timeout=app.config['JOB_TIMEOUT'],job_id=str(run_id))
        #print ('input******', pdb, CHO_chain, CHO_name, smiles_local_file, run_id, infos_pickle)
        #job = queue_single.enqueue_call(func=job_additive_prediction, args=(pdb, infos_pickle, mutation_list, predtype),result_ttl=0,timeout=app.config['JOB_TIMEOUT'],job_id=str(run_id))

        return redirect(url_for('results_prediction', job_id=run_id))
    else:
        msg = []
        msg.append("Please provide a <b>PDB</b> file and a <b>mutation list</b>.")
        return render_template('prediction_error.html', msg=msg)



@app.route('/results_prediction/<job_id>', methods=['GET'])
def results_prediction(job_id):
    upload_infos = os.path.join(app.config['UPLOAD_FOLDER'] + '/' + job_id, 'infos.p')
    #print ('upload_infos', upload_infos)
    # Check Job Status in working directoriy
    working_infos = pickle.load(open(upload_infos, 'rb'))
    #print ('test****', working_infos)
    if working_infos['job_status'] == 'NOT_READY':
        #if job_still_processing(job_id):
        return render_template('results_wait.html', job_id=job_id)
        #else:
        #    send_email_failed('biosigab@gmail.com',results_link=job_id)

        #    if working_infos['email']:
        #        send_email_failed(working_infos['email'],results_link=job_id)

        #    msg = []
        #    msg.append("There is a problem with your submittion,{},".format(str(job_id)))
        #    msg.append("We will contact you once CSM-Carbohydrate is able to process your job!")
        #    return render_template('prediction_error.html', msg=msg)
    #print (working_infos['job_status'])
    if not working_infos['job_status'] == 'NOT_READY':
        # RESULT FOLDER
        #folder = os.path.join(app.config['RESULT_FOLDER'], job_id)
        #results_pickle = os.path.join(folder, "infos.p")
        #results = pickle.load(open(results_pickle, 'rb'))
        #results = open(app.config['RESULT_FOLDER'] + '/' + job_id + '.csv')

        try:
            Arpeggio_wild_interactions=working_infos['Arpeggio_wild_interactions']
            Arpeggio_wild_PIinteractions=working_infos['Arpeggio_wild_PIinteractions']
            Arpeggio_wild_PIinteracting_residues=working_infos['Arpeggio_wild_PIinteracting_residues']
            print ('*****', Arpeggio_wild_interactions)
            #print ('*****111', Arpeggio_wild_PIinteractions)
            #print ('*****222', Arpeggio_wild_PIinteracting_residues)
            #print ('*****222', working_infos['carbohydrate_names'])
            #print ('*****222', str(working_infos['carbohydrate_chains']))
            #print ('*****222', working_infos['smiles'])
            Arpeggio_features=working_infos['Arpeggio_features']
            print ('*****22', Arpeggio_features['d_WeakPolar'][0])
            #print ('*****2222', features_chosen.values())
            #print ('*****222', features_chosen['Neut:Pos-3.00'][0])
            return render_template('prediction_results_single.html',
                                    prediction=working_infos['result'],
                                    smiles=working_infos['smiles'],
                                    #prediction=results['result'], size=len(results['result']),
                                    run_id=working_infos['run_id'],
                                    CHO_name=str(working_infos['carbohydrate_names']),
                                    CHO_chain=str(working_infos['carbohydrate_chains']),
                                    Arpeggio_features=(working_infos['Arpeggio_features']),
                                    Arpeggio_wild_interactions=working_infos['Arpeggio_wild_interactions'],
                                    Arpeggio_wild_PIinteractions=working_infos['Arpeggio_wild_PIinteractions'],
                                    Arpeggio_wild_PIinteracting_residues=working_infos['Arpeggio_wild_PIinteracting_residues'],
                                    )
        except KeyError as e:
            return render_template('results_wait.html', job_id=job_id)


@app.route('/run_example_single', methods=['POST', 'GET'])
def run_example_single():
    results_pickle = os.path.join(app.config['EXAMPLE_FOLDER'] + '/1GX4/infos.p')
    results = pickle.load(open(results_pickle, 'rb'))
    Arpeggio_wild_interactions=results['Arpeggio_wild_interactions']
    Arpeggio_wild_PIinteractions=results['Arpeggio_wild_PIinteractions']
    Arpeggio_wild_PIinteracting_residues=results['Arpeggio_wild_PIinteracting_residues']
    Arpeggio_features=results['Arpeggio_features']
    print ('Arpeggio_features', Arpeggio_features)
    return render_template('prediction_example_single.html',
                           prediction=results['result'],
                           smiles=results['smiles'],
                           run_id=results['run_id'],
                           CHO_name=results['carbohydrate_names'],
                           CHO_chain=results['carbohydrate_chains'],
                           Arpeggio_features=results['Arpeggio_features'],
                           #features_chosen=results['features_chosen'],                                                      
                           Arpeggio_wild_interactions=results['Arpeggio_wild_interactions'],
                           Arpeggio_wild_PIinteractions=results['Arpeggio_wild_PIinteractions'],
                           Arpeggio_wild_PIinteracting_residues=results['Arpeggio_wild_PIinteracting_residues'],
                           )


@app.route('/datatable/<file_id>/<typeofdatatable>',methods=['GET'])
def get_datatable_json(file_id,typeofdatatable):
    job_id = file_id
    folder = app.config['RESULT_FOLDER'] + "/" + job_id
    file_to_download = typeofdatatable+".txt"
    try:
        return send_file('{}/{}'.format(folder,file_to_download), attachment_filename=file_id+"_"+typeofdatatable)
    except Exception as e:
        return str(e)


@app.route('/example_datatable/<type_of_run>/<typeofdatatable>',methods=['GET'])
def get_example_datatable_json(type_of_run,typeofdatatable):
    folder = os.path.join(app.config['EXAMPLE_FOLDER'],type_of_run)
    file_to_download = typeofdatatable+".txt"
    try:
        return send_file('{}/{}'.format(folder,file_to_download), attachment_filename=typeofdatatable)
    except Exception as e:
        return str(e)


api.add_resource(SinglePredictionApi, "/api/prediction_single",endpoint="api.prediction_single")
