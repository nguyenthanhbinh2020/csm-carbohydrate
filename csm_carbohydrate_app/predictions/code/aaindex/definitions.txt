RSA AND SST (KOSJ950100-RSA-SST)
    KOSJ950101 - Context-dependent optimal substitution matrices for exposed helix
    KOSJ950102 - Context-dependent optimal substitution matrices for exposed beta
    KOSJ950103 - Context-dependent optimal substitution matrices for exposed turn
    KOSJ950104 - Context-dependent optimal substitution matrices for exposed coil
    KOSJ950105 - Context-dependent optimal substitution matrices for buried helix
    KOSJ950106 - Context-dependent optimal substitution matrices for buried beta
    KOSJ950107 - Context-dependent optimal substitution matrices for buried turn
    KOSJ950108 - Context-dependent optimal substitution matrices for buried coil


SST (KOSJ950100-SST)
    KOSJ950109 - Context-dependent optimal substitution matrices for alpha helix
    KOSJ950110 - Context-dependent optimal substitution matrices for beta sheet
    KOSJ950111 - Context-dependent optimal substitution matrices for turn
    KOSJ950112 - Context-dependent optimal substitution matrices for coil


RSA (KOSJ950110 - OVEJ920100)
    KOSJ950113 - Context-dependent optimal substitution matrices for exposed residues
    KOSJ950114 - Context-dependent optimal substitution matrices for buried residues

    OVEJ920104 - Environment-specific amino acid substitution matrix for accessible residues
    OVEJ920105 - Environment-specific amino acid substitution matrix for inaccessible residues
