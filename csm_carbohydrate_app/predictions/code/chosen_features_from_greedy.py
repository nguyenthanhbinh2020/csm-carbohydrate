import sys
#python /home/local/BHRI/tbnguyen/Desktop/mCSM-carbohydrate/greedyfs_regression/dG/chosen_features_from_greedy.py /home/local/BHRI/tbnguyen/Desktop/mCSM-NA3/code_from_bitbucket/greedyfs_regressor/test_for_group/NA_8_2_2_Stab_2_7/Pearson_cutoff_0.15/all_features_mCSM_NA_0.5_8_2_2_mCSM_Stability_0.5_2_7.out_add_group.csv

fin             = sys.argv[1]
f_feature       = sys.argv[2]
no_features     = int(sys.argv[3])
count           = 0
fout            = open(sys.argv[4], 'w')#open(fin + '.chosen_features', 'w')
#print (sys.argv[4])
chosen_index    = []
#chosen_feature  = ['GROUP', 'Aro:Hydro-2.00', 'Neut:Pos-2.00', 'PEOE_VSA6', 'Acc:Pos-5.00', 'Inter-Acc:Acc-4.00', 'Inter-Hydro:Neg-9.00', 'Aro:Neg-8.00', 'Pos:Pos-8.00', 'Neut:Pos-3.00', 'Hydrophobic', 'Neg:Neg-8.00', 'Inter-Hydro:O_at-4.00', 'Aro:Aro-2.00', 'Inter-Neg:O_at-5.00', 'Inter-Acc:Aro-7.00', 'Inter-Don:Hydro-6.00', 'Neg:Neut-2.00', 'Hydro:Hydro-6.00', 'Hydro:Hydro-3.00', 'Inter-Aro:C_at-5.00', 'dG'] ### 20 features
###csm-lig: 18 features: 'Aro:Hydro-2.00', 'Neut:Pos-2.00', 'Acc:Pos-5.00', 'Inter-Acc:Acc-4.00', 'Inter-Hydro:Neg-9.00', 'Aro:Neg-8.00', 'Pos:Pos-8.00', 'Neut:Pos-3.00', 'Neg:Neg-8.00', 'Inter-Hydro:O_at-4.00', 'Aro:Aro-2.00', 'Inter-Neg:O_at-5.00', 'Inter-Acc:aro-7.00', 'Inter-Don:Hydro-6.00', 'Neg:Neut-2.00', 'Hydro:Hydro-6.00', 'Hydro:Hydro-3.00', 'Inter-Aro:C_at-5.00'
###arpeggio: 1 feature: 'Hydrophobic'
###pdCSM: 1 feature: 'PEOE_VSA6'
###csm-lig: 23 features: 'Aro:Hydro-2.00', 'Neg:Neg-5.00', 'Neut:Pos-3.00', 'Aro:Neg-7.00', 'Inter-Acc:Don-4.00', 'Aro:Aro-8.00', 'Inter-Don:C_at-10.00', 'Inter-Acc:Don-7.00', 'Acc:Neg-3.00', 'Neg:Neg-4.00', 'Inter-Acc:Don-10.00', 'Aro:Hydro-4.00', 'Hydro:Neg-9.00', 'Acc:Aro-5.00', 'Acc:Aro-8.00', 'Acc:Hydro-9.00', 'Aro:Aro-3.00', 'Neg:Neg-3.00', 'Aro:Don-9.00', 'Aro:Aro-5.00', 'Aro:Neg-8.00', 'Aro:Hydro-5.00', 'Inter-Don:O_at-7.00'
###arpeggio: 1 feature: 'Hydrophobic'
###pdCSM: 1 feature: 'SMR_VSA7'
def read_f_feature(f_feature, no_features):
    chosen_features = []
    count_feature   = 0
    for line in open(f_feature):
        count_feature += 1
        if line.startswith(",blind-test"):
            count_feature = 0
            continue
        cols = line.split('\n')[0].split(',')
        feature_name = cols[2]
        if count_feature <= no_features:
            chosen_features.append(feature_name)
    return chosen_features

chosen_features = read_f_feature(f_feature, no_features)
#print (len(chosen_features), chosen_features)
for line in open(fin):
    count += 1
    if count == 1:
        features = line.split('\n')[0].split(',')
        #print (features)
        for feature in chosen_features:
            index = features.index(feature)
            #print (index)
            chosen_index.append(index)
        #print (chosen_index)
        fout.write(','.join(str(x) for x in chosen_features) + '\n')
    if count > 1: 
        cols = line.split('\n')[0].split(',')
        chosen_col = []
        for k in chosen_index:
            #print (features[k])
            chosen_col.append(cols[k])
        #fout.write(str(count-1) + ',' + ','.join(str(x) for x in chosen_col) + '\n') ### ddG at the last column
        fout.write(','.join(str(x) for x in chosen_col) + '\n') ### ddG at the last column

fout.close()
