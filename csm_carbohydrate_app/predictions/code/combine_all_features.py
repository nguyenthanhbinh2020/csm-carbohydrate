import sys

f_Arana		= sys.argv[1]
f_pdcsm		= sys.argv[2]
f_csm_lig	= sys.argv[3]

#print (f_Arana)
fout		= open(sys.argv[4], 'w')

features	= []
values		= []

def read_file(fin, features, values):
	count = 0
	for line in open(fin):
		count += 1
		cols = line.split('\n')[0].split(',')
		if count == 1:
			for x in cols:
				features.append(x)
		if count == 2:
			for x in cols:
				values.append(x)

	return features, values

features_Ar, values_Ar = read_file(f_Arana, features, values)
features_pdcsm, values_pdcsm = read_file(f_pdcsm, features_Ar, values_Ar)
features_csmlig, values_csmlig = read_file(f_csm_lig, features_pdcsm, values_pdcsm)

#print (features_csmlig)
#print (values_csmlig)
fout.write(','.join(str(x) for x in features_csmlig) + '\n')
fout.write(','.join(str(y) for y in values_csmlig) + '\n')

