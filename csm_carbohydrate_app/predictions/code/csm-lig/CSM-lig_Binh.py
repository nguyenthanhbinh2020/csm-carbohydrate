#!/usr/bin/perl -w 
# ********************************************************
# *   ----------------------------------------------	 *
# * Thanh-Binh Nguyen - ntb3004@yahoo.com   *
# * Last modification :: 12/03/2021					  *
# *   ----------------------------------------------	 *
# ********************************************************

import sys
#import os
import subprocess

# -------------------------------------------------------------------------------------------------
# Atom classifications - Pharmacophores

AtomFeature = {
# Hydrophobics
"ALACB":["Hydro"], "ARGCB":["Hydro"], "ARGCG":["Hydro"], "ARGCD":["Hydro"], "ASNCB":["Hydro"], "ASPCB":["Hydro"], "CYSCB":["Hydro"], "GLNCB":["Hydro"], "GLNCG":["Hydro"], "GLUCB":["Hydro"], "GLUCG":["Hydro"], "HISCB":["Hydro"], "ILECB":["Hydro"], "ILECG1":["Hydro"], "ILECG2":["Hydro"], "ILECD1":["Hydro"], "LEUCB":["Hydro"], "LEUCG":["Hydro"], "LEUCD1":["Hydro"], "LEUCD2":["Hydro"], "LYSCB":["Hydro"], "LYSCG":["Hydro"], "LYSCD":["Hydro"], "METCB":["Hydro"], "METCG":["Hydro"], "METCE":["Hydro"], "PHECB":["Hydro"], "PROCB":["Hydro"], "PROCG":["Hydro"], "PROCD":["Hydro"], "THRCG2":["Hydro"], "TRPCB":["Hydro"], "TRPCZ":["Hydro"], "TYRCB":["Hydro"], "VALCB":["Hydro"], "VALCG1":["Hydro"], "VALCG2":["Hydro"],
# Acceptors
"ALAO":["Acc"], "ARGO":["Acc"], "ASNO":["Acc"], "ASNOD1":["Acc"], "ASPO":["Acc"], "CYSO":["Acc"], "GLNO":["Acc"], "GLNOE1":["Acc"], "GLUO":["Acc"], "GLYO":["Acc"], "HISO":["Acc"], "ILEO":["Acc"], "LEUO":["Acc"], "LYSO":["Acc"], "METO":["Acc"], "PHEO":["Acc"], "PROO":["Acc"], "SERO":["Acc"], "THRO":["Acc"], "TRPO":["Acc"], "TYRO":["Acc"], "VALO":["Acc"],
# Donors
"CYSS":["Don"], "ALAN":["Don"], "ARGN":["Don"], "ARGNE":["Don"], "ASNN":["Don"], "ASNND2":["Don"], "ASPN":["Don"], "CYSN":["Don"], "GLNN":["Don"], "GLNNE2":["Don"], "GLUN":["Don"], "GLYN":["Don"], "HISN":["Don"],  "ILEN":["Don"], "LEUN":["Don"], "LYSN":["Don"], "METN":["Don"], "PHEN":["Don"], "PRON":["Don"], "SERN":["Don"], "SEROG":["Don"], "THRN":["Don"], "THROG1":["Don"], "TRPN":["Don"], "TYRN":["Don"], "TYROH":["Don"], "VALN":["Don"],
# Aromatics

# Positives
 "LYSNZ":["Pos", "Don"], 
# Negatives
"ASPOD1":["Neg", "Acc"], "ASPOD2":["Neg", "Acc"], "GLUOE1":["Neg", "Acc"], "GLUOE2":["Neg", "Acc"], 
# Multiples
"ARGNH1":["Don", "Pos"], "ARGNH2":["Don", "Pos"], "HISND1":["Don", "Aro", "Pos"], "HISCD2":["Hydro", "Aro"], "HISCG":["Hydro", "Aro"], "HISCE1":["Hydro", "Aro"], "HISNE2":["Aro", "Don", "Pos"], "PHECG":["Hydro", "Aro"], "PHECD1":["Hydro", "Aro"], "PHECD2":["Hydro", "Aro"], "PHECE1":["Hydro", "Aro"], "PHECE2":["Hydro", "Aro"], "PHECZ":["Hydro", "Aro"], "TRPCG":["Hydro", "Aro"], "TRPCD1":["Hydro", "Aro"], "TRPCD2":["Hydro", "Aro"], "TRPNE1":["Don", "Aro"], "TRPCE2":["Hydro", "Aro"], "TRPCE3":["Hydro", "Aro"], "TRPCZ2":["Hydro", "Aro"], "TRPCZ3":["Hydro", "Aro"], "TRPCH2":["Hydro", "Aro"], "TYRCD1":["Hydro", "Aro"], "TYRCD2":["Hydro", "Aro"], "TYRCE1":["Hydro", "Aro"], "TYRCE2":["Hydro", "Aro"], "TYRCG":["Hydro", "Aro"], "TYRCZ":["Hydro", "Aro"]}
#print (AtomFeature)
# -------------------------------------------------------------------------------------------------
# Input parameters
pdb_file = sys.argv[1];
smi_file = sys.argv[2];
outFile = sys.argv[3];
cutoff_step = 1;#0.5;	# Cutoff step
# cutoff_limit = 15;	# Cutoff limit
code_path = sys.argv[4];
cutoff_limit = float(sys.argv[5]);	# Cutoff limit


# signType = 0;	# Signature type:
signType = 2;#2;	# Signature type:
					# 0:mCSM 	1 atom class
					# 1:mCSM-HP	2 atom classes {Hydro,Polar}
					# 2:mCSM-ALL	7 atom classes {Hydro,Pos,Neg,Acc,Don,Aro,Neutral}

mcsm_all = {"Hydro": 1, "Pos": 1, "Neg": 1,	"Acc": 1, "Don": 1, "Aro": 1, "Neut": 1}
add_lig_type = {"C_at":0, "O_at":0} ### C and O atoms of carbohydrates

mcsm_all_keys = sorted(mcsm_all.keys())
add_lig_type_keys = sorted(add_lig_type.keys())
'''if(scalar(@ARGV) != 5){
print "___________________________________________________________________________________
SINTAX:
	perl CSM-lig.py <pdb_file> <smi_file> <outfile> <path>
___________________________________________________________________________________\n";
	exit;
}
'''
OUTFILE =   open(outFile, 'w')

# Generating header for the output file

print ("---Generating signature header")

OUTFILE.write("LIG.MOL_WEIGHT,LIG.ATOM_COUNT,LIG.LOGP,LIG.NUM_ACCEPTORS,LIG.NUM_DONORS,LIG.NUM_HETEROATOMS,LIG.NUM_ROTATABLE_BONDS,LIG.NUM_RINGS,LIG.LABUTE_ASA,LIG.TPSA,")

# mCSM
n_step  = int((cutoff_limit -1 )/cutoff_step)
#print ("n_step", n_step)
if signType == 0:
	for i in range(n_step+1):
		x = cutoff_limit - cutoff_step * i
		OUTFILE.write(str(format(x, '.2f')) + ',')
	for i in range(n_step+1):
		x = cutoff_limit - cutoff_step * i
		OUTFILE.write('Inter-' + str(format(x, '.2f')) + ',')

# mCSM-HP
elif signType == 1:
	for i in range(n_step+1):
		#print (i)
		x = cutoff_limit - cutoff_step * i
		OUTFILE.write("HH:" + str(format(x, '.2f')) + ',' + "PP:" + str(format(x, '.2f')) + ',' + "HP:" + str(format(x, '.2f')) + ',')
	for i in range(n_step+1):
		x = cutoff_limit - cutoff_step * i
		OUTFILE.write("Inter-HH:" + str(format(x, '.2f')) + ',' + "Inter-PP:" + str(format(x, '.2f')) + ',' + "Inter-HP:" + str(format(x, '.2f')) + ',')

# mCSM-ALL

elif signType == 2:
	for k in range(n_step+1):
		x = cutoff_limit - cutoff_step * k
		for i in range(len(mcsm_all_keys)):
			a = mcsm_all_keys[i]
			for j in range(i, len(mcsm_all_keys)):
				b = mcsm_all_keys[j]
				OUTFILE.write(str(a) + ':' + str(b) + '-' + str(format(x, '.2f'))  + ',')
	for k in range(n_step+1):
		x = cutoff_limit - cutoff_step * k
		for i in range(len(mcsm_all_keys)):
			a = mcsm_all_keys[i]
			for j in range(i, len(mcsm_all_keys)):
				b = mcsm_all_keys[j]
				OUTFILE.write("Inter-" + str(a) + ':' + str(b) + '-' + str(format(x, '.2f'))  + ',')
			for m in range(len(add_lig_type_keys)):
				c = add_lig_type_keys[m]
				OUTFILE.write("Inter-" + str(a) + ':' + str(c) + '-' + str(format(x, '.2f'))  + ',')

OUTFILE.write("LOG_AFFIN\n")

#print ("---Header generated")

#print ("---Generating signatures")

affin = "999"

# ____________________________________________________________________________________________________________________

print "---Calculating ligand properties\t***$pdb_file***\n";

def read_smile(smi_file):
	for line in open(smi_file):
		smiles = line.split('\n')[0].split()[0]
	return smiles

smiles	= read_smile(smi_file)

#Calculating ligand properties
#LIG.MOL_WEIGHT,LIG.ATOM_COUNT,LIG.LOGP,LIG.NUM_ACCEPTORS,LIG.NUM_DONORS,LIG.NUM_HETEROATOMS,LIG.NUM_ROTATABLE_BONDS,LIG.NUM_RINGS,LIG.LABUTE_ASA,LIG.TPSA
#cmd = os.system('python ' + code_path + 'gen_descriptors.py "' + smiles + '"')
descriptors = subprocess.check_output('python ' + code_path + 'gen_descriptors.py "' + smiles + '"', shell=True).split('\n')[1].split(',')
OUTFILE.write(','.join(str(x) for x in descriptors) + ',')

# ==================================================================================================
counter = 0

atom_selected = 0

# ==================================================================================================
#### distance calculation function
def distance(A, B):
	dx  = float(A[0]) - float(B[0])
	dy  = float(A[1]) - float(B[1])
	dz  = float(A[2]) - float(B[2])
	#print (A,B)
	#print (dx*dx+ dy*dy+ dz*dz)
	dist_   = (dx*dx + dy*dy + dz*dz)**0.5
	#print (dist_)
	return dist_

# ==================================================================================================
het_pdb = pdb_file + ".het.pdb"
het_mol = pdb_file + ".het.mol"

print "---Filtering atoms\n";

# Filtering atoms 
PDB = pdb_file
def read_pdb(PDB):
	het_counter = 0
	max_index   = 0
	het_keys	= {}
	coord		= {}
	atom_index  = {}
	is_lig_atom = {}
	res_name	= {}
	res_num		= {}
	atom_name   = {}
	k			= 0
	for line in open(PDB):
		if not (line.startswith("HETATM") or line.startswith("ATOM")):
			continue
		k += 1
		if line.startswith("HETATM"):
			het_counter += 1
			
		res_ind = line[22:26].strip()
		name	= line[17:20].strip()
		x_coor  = line[30:38].strip()
		y_coor  = line[38:46].strip()
		z_coor	   = line[46:54].strip()
		chain_id= line[21]
		if name != "HOH":
			atm_name= line[12:16].strip()
			coord[k] = [x_coor, y_coor, z_coor]

			atom_index[k]=line[6:11].strip()
			is_lig_atom[k] = 0
			res_name[k] = line[17:20].strip()
			atom_name[k] = line[12:16].strip()
			res_num[k]=line[22:26].strip()
			if int(line[6:11].strip()) > max_index:
				max_index   = int(line[6:11].strip())
			if line.startswith("HETATM"):
				is_lig_atom[k] = 1
				het_keys[het_counter] = name + atm_name
	return het_keys, coord, atom_index, is_lig_atom, res_name, atom_name, max_index, k, res_num

het_keys, coord, atom_index, is_lig_atom, res_name, atom_name, max_index, n_line_from_pdb, res_num = read_pdb(PDB)
#print ("het_keys", het_keys)
#print ("coord", coord)
#print ("atom_index", atom_index)
#print ("is_lig_atom", is_lig_atom)
#print ("res_name", res_name)
#print ("atom_name", atom_name)

print "---Calculating pharmacophores for ligand\n";

# Getting pharmacophores for ligand

#system("sed -i \"s/\\s\+\$//g\" $het_pdb");
#system("sed -i \"s/[0-9]-\$//g\" $het_pdb");

subprocess.check_output('babel -ipdb ' + het_pdb + ' -omol -O ' + het_mol + ' -p 7.4 2> /dev/null', shell=True)
#print ('babel -ipdb ' + het_pdb + ' -omol -O ' + het_mol + ' -p 7.4 2> /dev/null')
pharm   =   subprocess.check_output('python ' + code_path + 'gen_pharmacophores.py ' + het_mol + ' > ' + het_mol + '.pharm', shell=True)
pharm_file  = het_mol + '.pharm'
#print ("pharm", pharm_file)

# Get pharmacophores
def read_pharmacophores(pharm_file, het_keys):
	atom_class		= {'Hydrophobe':'Hydro', 'Aromatic': 'Aro', 'Ionizable': 'Pos', 'NegIonizable':'Neg', 'Acceptor':'Acc', 'Donor':'Don', 'PosIonizable': 'Pos'}
	new_het_keys	= {}
	for line in open(pharm_file):
		class_ = line.split('\n')[0].split()
		#print (class_)
		atom_ind_list = int(class_[1].split(',')[0].split('(')[1]) + 1
		#print ('***', het_keys[atom_ind_list])
		if class_[0] not in ['LumpedHydrophobe', 'ZnBinder']:
			try:
				new_het_keys[het_keys[atom_ind_list]].append(atom_class[class_[0]])
			except KeyError:
				new_het_keys[het_keys[atom_ind_list]]=[]
				new_het_keys[het_keys[atom_ind_list]].append(atom_class[class_[0]])

	for value in het_keys.values():
		if value not in new_het_keys.keys():
			 new_het_keys[value] = ['Neut']
	for key in new_het_keys.keys():
		if key[3] == 'C':
			new_het_keys[key].append('C_at')
		if key[3] == 'O':
			new_het_keys[key].append('O_at')
	return new_het_keys
#print ("het_keys", het_keys)
new_het_keys	= read_pharmacophores(pharm_file, het_keys)
#print ("het_keys", new_het_keys)
	
# ==================================================================================================

#--------------------------------------------
# mCSM
edgeCount = 0
edgeCount_interChain = 0
# mCSM-HP
edgeCountHH = 0
edgeCountPP = 0
edgeCountHP = 0
edgeCountHH_interChain = 0
edgeCountPP_interChain = 0
edgeCountHP_interChain = 0
# mCSM-ALL
edgeCountTipo2 = {}
keys		= sorted(mcsm_all_keys)
#print ('keys', keys)
for key1 in sorted(mcsm_all_keys):
	for key2 in sorted(mcsm_all_keys):
		edgeCountTipo2[key1 + ':' + key2] = 0
		edgeCountTipo2[key1 + ':' + key2 + ':inter'] = 0
		if key1 != key2:
			edgeCountTipo2[key2 + ':' + key1] = 0
			edgeCountTipo2[key2 + ':' + key1 + ':inter'] = 0
	for key2 in sorted(add_lig_type_keys):
			edgeCountTipo2[key1 + ':' + key2 + ':inter'] = 0
#print ("edgeCountTipo2", edgeCountTipo2)
#--------------------------------------------

atom_selected = max_index;
dist = {}


# Cutoff limits
cutoff_1 = 1.0;
cutoff_2 = cutoff_limit;

print "---Starting cutoff scanning\n";
		
#print (n_line_from_pdb)
#print (AtomFeature)
# Pair-wise distance calculation of atoms in environment
all_dis =   {}
for i in range(1,n_line_from_pdb):
	for j in range(i+1, n_line_from_pdb+1):
		dist_ = distance(coord[i], coord[j])
		all_dis[res_num[i], res_name[i], atom_name[i], res_num[j], res_name[j], atom_name[j]] = dist_
		#print ("dis", dist_)
		if cutoff_1 <= dist_ <= cutoff_2:
			if is_lig_atom[i] == is_lig_atom[j]:
				edgeCount += 1
			else:
				edgeCount_interChain += 1
		key1 = res_name[i]+atom_name[i]
		key2 = res_name[j]+atom_name[j]
		check_hydro_1   = 0
		check_hydro_2   = 0
		if signType == 1:
			if key1 not in AtomFeature.keys() and key1 not in new_het_keys.keys():
				AtomFeature[key1] =['Neut']
			if key2 not in AtomFeature.keys() and key2 not in new_het_keys.keys():
				AtomFeature[key2] =['Neut']
			if key1 in new_het_keys.keys():
				AtomFeature[key1] =new_het_keys[key1]
			if key2 in new_het_keys.keys():
				AtomFeature[key2] =new_het_keys[key2]
			if key1 in AtomFeature.keys() and key2 in AtomFeature.keys(): ### because no mainchain atoms
				for at_feat1 in AtomFeature[key1]:
					if at_feat1 == 'Hydro':
						check_hydro_1 = 1
						break
				for at_feat2 in AtomFeature[key2]:
					if at_feat2 == 'Hydro':
						check_hydro_2 = 1
						break
				if check_hydro_1 == check_hydro_2 == 1:
					if is_lig_atom[i] == is_lig_atom[j]:
						edgeCountHH += 1
					else:
						edgeCountHH_interChain += 1
				elif  check_hydro_1 != 1 and check_hydro_2 != 1: 
					if is_lig_atom[i] == is_lig_atom[j]:
						edgeCountPP += 1
					else:
						edgeCountPP_interChain += 1
				else:
					if is_lig_atom[i] == is_lig_atom[j]:
						edgeCountHP += 1
					else:
						edgeCountHP_interChain += 1
		elif signType == 2:
			#print (key1,key2)
			if key1 not in AtomFeature.keys() and key1 not in new_het_keys.keys():
				AtomFeature[key1] =['Neut']
				#print ('key1', key1)
			if key2 not in AtomFeature.keys()and key2 not in new_het_keys.keys():
				AtomFeature[key2] =['Neut']
			if key1 in new_het_keys.keys():
				AtomFeature[key1] =new_het_keys[key1]
			if key2 in new_het_keys.keys():
				AtomFeature[key2] =new_het_keys[key2]
				#print ('key2', key2)
			#print ('**', is_lig_atom, i)
			for key_set1 in AtomFeature[key1]:
				for key_set2 in AtomFeature[key2]:
					if is_lig_atom[i] == is_lig_atom[j] and key_set2 not in ["C_at", "O_at"] and key_set1 not in ["C_at", "O_at"] :
						#print ('AtomFeature', AtomFeature)
						#print ('***', key_set1, key_set2)
						edgeCountTipo2[key_set1 + ':' + key_set2] += 1
						if  key_set1 != key_set2 and key_set2 not in ["C_at", "O_at"] and key_set1 not in ["C_at", "O_at"] :
							edgeCountTipo2[key_set2 + ':' + key_set1] += 1
					else:
						if key_set1 not in ["C_at", "O_at"]:		 
							edgeCountTipo2[key_set1 + ':' + key_set2 + ':inter'] += 1
						if  key_set1 != key_set2 and key_set2 not in ["C_at", "O_at"]:
							edgeCountTipo2[key_set2 + ':' + key_set1 + ':inter'] += 1


#print ("edgeCountTipo2", edgeCountTipo2)
# Perform cutoff scanning
all_edgeCount_scanning  = {}
all_edgeCount_interChain_scanning = {}
all_edgeCountHH_scanning = {}
all_edgeCountPP_scanning = {}
all_edgeCountHP_scanning = {}
all_edgeCountHH_interChain_scanning = {}
all_edgeCountPP_interChain_scanning = {}
all_edgeCountHP_interChain_scanning = {}
all_edgeCountTipo2_scanning = {}

for x in range(n_step+1):
	cutoff_temp = cutoff_limit - cutoff_step * x
	#print (x, cutoff_temp)
	# mCSM
	edgeCount_scanning = 0
	edgeCount_interChain_scanning = 0
	# mCSM-HP
	edgeCountHH_scanning = 0
	edgeCountPP_scanning = 0
	edgeCountHP_scanning = 0
	edgeCountHH_interChain_scanning = 0
	edgeCountPP_interChain_scanning = 0
	edgeCountHP_interChain_scanning = 0
	edgeCountTipo2_scanning = {}

	# mCSM-ALL
	for key1 in sorted(mcsm_all_keys):
		for key2 in sorted(mcsm_all_keys):
			edgeCountTipo2_scanning[key1 + ':' + key2] = 0
			edgeCountTipo2_scanning[key1 + ':' + key2 + ':inter'] = 0
			if key1 != key2:
				edgeCountTipo2_scanning[key2 + ':' + key1] = 0
				edgeCountTipo2_scanning[key2 + ':' + key1 + ':inter'] = 0	
		for key3 in sorted(add_lig_type_keys):
			edgeCountTipo2_scanning[key1 + ':' + key3 + ':inter'] = 0	
			
	for i in range(1,n_line_from_pdb):
		for j in range(i+1, n_line_from_pdb+1):
			#print (atom_name[i], atom_name[j])
			dist_ = all_dis[res_num[i], res_name[i], atom_name[i], res_num[j], res_name[j], atom_name[j]]
			#if atom_name[j] == 'O5' or atom_name[i] == 'O5':
				#print ('***', dist_, res_num[i], res_name[i], atom_name[i], res_num[j], res_name[j], atom_name[j], AtomFeature[res_name[i]+atom_name[i]],AtomFeature[res_name[j]+atom_name[j]])
			if cutoff_1 <= dist_ <= cutoff_temp:
				#print ('*', cutoff_temp, dist_, res_num[i], res_name[i], atom_name[i], res_num[j], res_name[j], atom_name[j], AtomFeature[res_name[i]+atom_name[i]],AtomFeature[res_name[j]+atom_name[j]])
				if is_lig_atom[i] == is_lig_atom[j]:
					edgeCount_scanning += 1
				else:
					edgeCount_interChain_scanning += 1
				key1 = res_name[i]+atom_name[i]
				key2 = res_name[j]+atom_name[j]
				check_hydro_1   = 0
				check_hydro_2   = 0
				if signType == 1:
					if key1 not in AtomFeature.keys() and key1 not in new_het_keys.keys():
						AtomFeature[key1] =['Neut']
					if key2 not in AtomFeature.keys() and key2 not in new_het_keys.keys():
						AtomFeature[key2] =['Neut']
					if key1 in new_het_keys.keys():
						AtomFeature[key1] =new_het_keys[key1]
					if key2 in new_het_keys.keys():
						AtomFeature[key2] =new_het_keys[key2]
					#print (key1 , key2, AtomFeature.keys())
					if key1 in AtomFeature.keys() and key2 in AtomFeature.keys(): ### because no mainchain atoms
						#print ('***')
						for at_feat1 in AtomFeature[key1]:
							if at_feat1 == 'Hydro':
								check_hydro_1 = 1
								break
						for at_feat2 in AtomFeature[key2]:
							if at_feat2 == 'Hydro':
								check_hydro_2 = 1
								break
						#print ('***', check_hydro_1, check_hydro_2, res_num[i], res_name[i], atom_name[i], res_num[j], res_name[j], atom_name[j])
						if check_hydro_1 == check_hydro_2 == 1:
							if is_lig_atom[i] == is_lig_atom[j]:
								edgeCountHH_scanning += 1
							else:
								edgeCountHH_interChain_scanning += 1
						elif check_hydro_1 != 1 and check_hydro_2 != 1:
							if is_lig_atom[i] == is_lig_atom[j]:
								edgeCountPP_scanning += 1
							else:
								edgeCountPP_interChain_scanning += 1
						else:
							if is_lig_atom[i] == is_lig_atom[j]:
								edgeCountHP_scanning += 1
							else:
								edgeCountHP_interChain_scanning += 1
				elif signType == 2:
					#print (key1,key2)
					#if key1 not in AtomFeature.keys():
					#	AtomFeature[key1] ='Neut'
					#if key2 not in AtomFeature.keys():
					#	AtomFeature[key2] ='Neut'
					#print ('**', is_lig_atom, i)
					#key_set1 = AtomFeature[key1]
					#key_set2 = AtomFeature[key2]
					for key_set1 in AtomFeature[key1]:
						for key_set2 in AtomFeature[key2]:
							#print ('***', key_set1, key_set2)
							if is_lig_atom[i] == is_lig_atom[j] and key_set2 not in ["C_at", "O_at"] and key_set1 not in ["C_at", "O_at"] :
								#print ('AtomFeature', AtomFeature)
								#print ('***', key_set1, key_set2)
								edgeCountTipo2_scanning[key_set1 + ':' + key_set2] += 1
								if  key_set1 != key_set2 and key_set2 not in ["C_at", "O_at"] and key_set1 not in ["C_at", "O_at"] :
									edgeCountTipo2_scanning[key_set2 + ':' + key_set1] += 1
							else:
								if key_set1 not in ["C_at", "O_at"]: 
									edgeCountTipo2_scanning[key_set1 + ':' + key_set2 + ':inter'] += 1
								if  key_set1 != key_set2 and key_set2 not in ["C_at", "O_at"]:
									edgeCountTipo2_scanning[key_set2 + ':' + key_set1 + ':inter'] += 1
	all_edgeCount_scanning[cutoff_temp] = edgeCount_scanning
	all_edgeCount_interChain_scanning[cutoff_temp] = edgeCount_interChain_scanning
	all_edgeCountHH_scanning[cutoff_temp] = edgeCountHH_scanning
	all_edgeCountPP_scanning[cutoff_temp] = edgeCountPP_scanning
	all_edgeCountHP_scanning[cutoff_temp] = edgeCountHP_scanning
	all_edgeCountHH_interChain_scanning[cutoff_temp] = edgeCountHH_interChain_scanning
	all_edgeCountPP_interChain_scanning[cutoff_temp] = edgeCountPP_interChain_scanning
	all_edgeCountHP_interChain_scanning[cutoff_temp] = edgeCountHP_interChain_scanning
	all_edgeCountTipo2_scanning[cutoff_temp] = edgeCountTipo2_scanning
#print (edgeCountHH_scanning, edgeCountPP_scanning, edgeCountHP_scanning, edgeCountHH_interChain_scanning, edgeCountPP_interChain_scanning, edgeCountHP_interChain_scanning)
if signType == 0:
	for x in sorted(all_edgeCount_scanning.keys(), reverse=True):
		OUTFILE.write(str(format(all_edgeCount_scanning[x], '.2f')) + ',')
	for i in sorted(all_edgeCount_interChain_scanning.keys(), reverse=True):
		OUTFILE.write(str(format(all_edgeCount_interChain_scanning[x], '.2f')) + ',')
###**************** Order of writing the output, why it is not correct in signtype==1 but correct in signtype==2???? *****************###
###**************** signType==0 *****************###
# mCSM-HP
elif signType == 1:
	for x in sorted(all_edgeCountHH_scanning.keys(), reverse=True):
		OUTFILE.write(str(all_edgeCountHH_scanning[x]) + ',' + str(all_edgeCountPP_scanning[x]) + ',' + str(all_edgeCountHP_scanning[x]) + ',')
	for i in sorted(all_edgeCountHH_interChain_scanning.keys(), reverse=True):
		OUTFILE.write(str(all_edgeCountHH_interChain_scanning[x]) + ',' + str(all_edgeCountPP_interChain_scanning[x]) + ',' + str(all_edgeCountHP_interChain_scanning[x]) + ',')

elif signType == 2:
	for cutoff_temp in sorted(all_edgeCountTipo2_scanning.keys(), reverse=True):
		edgeCountTipo2_scanning = all_edgeCountTipo2_scanning[cutoff_temp]
		for i in range(len(mcsm_all_keys)):
			a = mcsm_all_keys[i]
			for j in range(i, len(mcsm_all_keys)):
				b = mcsm_all_keys[j]
				#print (a, b, cutoff_temp, str(edgeCountTipo2_scanning[str(a) + ':' + str(b)]))
				OUTFILE.write(str(edgeCountTipo2_scanning[str(a) + ':' + str(b)])  + ',')
	for cutoff_temp in sorted(all_edgeCountTipo2_scanning.keys(), reverse=True):
		edgeCountTipo2_scanning = all_edgeCountTipo2_scanning[cutoff_temp]
		#print (1, len(edgeCountTipo2_scanning))
		for i in range(len(mcsm_all_keys)):
			a = mcsm_all_keys[i]
			for j in range(i, len(mcsm_all_keys)):
				b = mcsm_all_keys[j]
				OUTFILE.write(str(edgeCountTipo2_scanning[str(a) + ':' + str(b)+ ':inter'])  + ',') 
			for m in range(len(add_lig_type_keys)):
				c = add_lig_type_keys[m]
				OUTFILE.write(str(edgeCountTipo2_scanning[str(a) + ':' + str(c)+ ':inter'])  + ',') 

OUTFILE.write(str(affin) + '\n')



