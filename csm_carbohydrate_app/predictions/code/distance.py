## YooChan Myung ##
##

import os
from Bio.PDB import *
import warnings
from Bio import BiopythonWarning
warnings.simplefilter('ignore',BiopythonWarning) ## Ignore warnings about B-factor and so on...

def getFullDistance(input_pdb,partner_chains,position,mut_chain):

    ## assign
    icode = " "

    if position[-1].isalpha():
        icode = position[-1].upper()
        res_num = int(position[0:-1])
    else:
        res_num = int(position)
    # partner_chains = partner_chains.split(',')
    prtner_chains=list(partner_chains)
    ## code start
    p = PDBParser()
    structure = p.get_structure('input',input_pdb)
    model = structure[0]

    # from mut_chain
    chain = model[mut_chain]
    mut_residue = chain[(' ',res_num,icode)]


    ##for partner chains

    distance=[]

    filename = os.path.split(input_pdb)[1]

    for each_chain in partner_chains:

        for residue in model[each_chain]:

            for atom_of_partner in residue:
                for atom_of_mut in mut_residue:
                    if residue.get_full_id()[3][2].isalpha() :

                        distance.append([filename,each_chain,residue.get_resname(),str(residue.get_full_id()[3][1])+residue.get_full_id()[3][2],atom_of_partner.get_name(),mut_chain,mut_residue.get_resname(),atom_of_mut.get_name(),(atom_of_partner - atom_of_mut)])

                    else:

                        distance.append([filename,each_chain, residue.get_resname(),str(residue.get_full_id()[3][1]),atom_of_partner.get_name(),mut_chain,mut_residue.get_resname(), atom_of_mut.get_name(),(atom_of_partner - atom_of_mut)])

    distance.sort(key=lambda k: k[8])

    distance.insert(0,['filename','p_chain','p_res','p_res_num','p_atom','m_chain','m_res','m_atom','distance'])


    return distance


def getClosestDistance(input_pdb, partner_chains, position, mut_chain):
    # print("processing...>>",input_pdb.split('/')[-1])
    return (getFullDistance(input_pdb, partner_chains, position, mut_chain)[1])[-1]


