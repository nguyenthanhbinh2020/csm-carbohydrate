#!/usr/bin/python

# ********************************************************
# *               University of Cambridge                *
# *   ----------------------------------------------     *
# *                                                      *
# * Douglas Eduardo Valente Pires - dpires@dcc.ufmg.br   *
# * www.dcc.ufmg.br/~dpires                              *
# * Last modification :: 04/07/2014                      *
# *   ----------------------------------------------     *
# ********************************************************

import os
import sys
#from rdkit import Chem
from rdkit.Chem import AllChem as Chem
from rdkit.Chem import Descriptors
from rdkit.Chem import ChemicalFeatures
from rdkit import RDConfig
from rdkit.Chem import Draw


# MAIN
if __name__ == '__main__':
	
	fdefName = os.path.join(RDConfig.RDDataDir,'BaseFeatures.fdef')
	factory = ChemicalFeatures.BuildFeatureFactory(fdefName)

	mol2file = sys.argv[1]
	smiles = sys.argv[2]
	#molecule = Chem.MolFromMol2File(mol2file)
	molecule2 = Chem.MolFromSmiles(smiles)

	if molecule2:

		feature_set = factory.GetFeaturesForMol(molecule2)

		for feature in feature_set:
			print feature.GetFamily() + "\t" + str(feature.GetAtomIds())

		#Draw.MolToFile(molecule2,'mol.png', size=(500, 500))
