import sys

smi_file	= sys.argv[1]
fout        = open(sys.argv[2], 'w')
data		= []
for line in open(smi_file):
	cols 	= line.split('\n')[0].split()
	smile	= cols[0]
	data.append('SMILES\tCLASS\n')
	data.append(smile + '\t1\n')

for x in data:
	fout.write(x)

fout.close()
