import sys

input_pdb           = sys.argv[1]
chosen_lig          = sys.argv[2]
chosen_lig_chain    = sys.argv[3]

#print(chosen_lig, chosen_prot_chain, chosen_lig_chain)
chosen_lig          = chosen_lig.split('_')
chosen_lig_chain    = chosen_lig_chain.split('_')


def read_pdb(input_pdb, chosen_lig, chosen_lig_chain):
    chosen_model    = []
    chosen_at       = []
    for line in open(input_pdb):
        if not (line.startswith('ATOM') or line.startswith('HETATM')):
            continue
        #print (line)
        conformation    = line[16]
        res_name        = line[17:20]
        chain           = line[21]
        at_name         = line[12:16].strip()
        res_num         = line[22:26].strip()
        id_             = [res_num, chain, at_name]
        
        if chain in chosen_lig_chain:
             if res_name in chosen_lig:
                if id_ not in chosen_at:#if line not in chosen_model:
                    chosen_model.append(line[:16] + ' ' + line[17:21] + 'X' + line[22:])
                    chosen_at.append(id_)
    return chosen_model

chosen_model    = read_pdb(input_pdb, chosen_lig, chosen_lig_chain)
fout            = open(input_pdb + '.het.pdb', 'w')
for x in chosen_model:
    fout.write(x)
fout.close()
