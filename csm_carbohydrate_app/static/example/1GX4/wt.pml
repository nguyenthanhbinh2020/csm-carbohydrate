reinitialize
set valence, 1
set stick_rad, 0.15
set line_width, 1
set mesh_width, 0.3
set label_size, 10
set sphere_scale, 0.15
set dash_round_ends, 0
set dash_gap, 0.25
set dash_length, 0.05
set_color maize, (251, 236, 93)
set_color cream, (255, 253, 208)
set_color electric_purple, (191, 0, 255)
set_color medium_purple, (147, 112, 219)
set_color amber, (255, 191, 0)
set_color peach, (255, 229, 180)
set_color electric_blue, (125, 249, 255)
set_color baby_blue, (224, 255, 255)
set_color emerald, (80, 200, 120)
set_color moss_green, (173, 200, 173)
set_color rose, (255, 0, 127)
set_color brilliant_rose, (246, 83, 166)
set_color thulian_pink, (222, 111, 161)
set_color neon_red, (255, 0, 102)
set cartoon_ladder_mode, 1
set cartoon_ring_finder, 1
set cartoon_ring_mode, 3
set cartoon_nucleic_acid_mode, 4
set cartoon_ring_transparency, 0.5
set line_smooth, 1
set antialias, 2
set cartoon_fancy_helices, 1
set depth_cue, 1
set specular,1
set surface_quality, 1
set stick_quality, 15
set sphere_quality, 2
set cartoon_sampling, 14
set ribbon_sampling, 10
set transparency_mode, 2
set stick_ball, 1
set stick_ball_ratio, 1.5
rebuild
load /home/tbnguyen/Desktop/csm_carbohydrate/mmCSM-NA-change/csm_carbohydrate_app/predictions/data/162684640047/wt.pdb
select binding_site, None
set defer_update, 1
distance undefined-proximal, X/1/O5, X/2/C1
show sticks, byres X/1/O5
show sticks, byres X/2/C1
select binding_site, binding_site + byres X/1/O5
select binding_site, binding_site + byres X/2/C1
distance undefined-proximal, X/1/C2, X/2/C1
show sticks, byres X/1/C2
show sticks, byres X/2/C1
select binding_site, binding_site + byres X/1/C2
select binding_site, binding_site + byres X/2/C1
distance undefined-proximal, X/1/C2, X/2/O5
show sticks, byres X/1/C2
show sticks, byres X/2/O5
select binding_site, binding_site + byres X/1/C2
select binding_site, binding_site + byres X/2/O5
distance undefined-vdw, X/1/C3, X/2/C1
show sticks, byres X/1/C3
show sticks, byres X/2/C1
select binding_site, binding_site + byres X/1/C3
select binding_site, binding_site + byres X/2/C1
distance weakpolar-proximal, X/1/C3, X/2/O5
show sticks, byres X/1/C3
show sticks, byres X/2/O5
select binding_site, binding_site + byres X/1/C3
select binding_site, binding_site + byres X/2/O5
distance undefined-proximal, X/1/C3, X/2/C5
show sticks, byres X/1/C3
show sticks, byres X/2/C5
select binding_site, binding_site + byres X/1/C3
select binding_site, binding_site + byres X/2/C5
distance undefined-proximal, X/1/C3, X/2/O6
show sticks, byres X/1/C3
show sticks, byres X/2/O6
select binding_site, binding_site + byres X/1/C3
select binding_site, binding_site + byres X/2/O6
distance undefined-vdwclash, X/1/C4, X/2/C1
show sticks, byres X/1/C4
show sticks, byres X/2/C1
select binding_site, binding_site + byres X/1/C4
select binding_site, binding_site + byres X/2/C1
distance weakpolar-vdwclash, X/1/C4, X/2/O5
show sticks, byres X/1/C4
show sticks, byres X/2/O5
select binding_site, binding_site + byres X/1/C4
select binding_site, binding_site + byres X/2/O5
distance undefined-proximal, X/1/C4, X/2/C5
show sticks, byres X/1/C4
show sticks, byres X/2/C5
select binding_site, binding_site + byres X/1/C4
select binding_site, binding_site + byres X/2/C5
distance undefined-proximal, X/1/C4, X/2/O6
show sticks, byres X/1/C4
show sticks, byres X/2/O6
select binding_site, binding_site + byres X/1/C4
select binding_site, binding_site + byres X/2/O6
distance undefined-proximal, A/356/CZ3, X/2/C1
show sticks, byres A/356/CZ3
show sticks, byres X/2/C1
select binding_site, binding_site + byres A/356/CZ3
select binding_site, binding_site + byres X/2/C1
distance undefined-proximal, X/1/C5, X/2/C1
show sticks, byres X/1/C5
show sticks, byres X/2/C1
select binding_site, binding_site + byres X/1/C5
select binding_site, binding_site + byres X/2/C1
distance undefined-proximal, X/1/C5, X/2/O5
show sticks, byres X/1/C5
show sticks, byres X/2/O5
select binding_site, binding_site + byres X/1/C5
select binding_site, binding_site + byres X/2/O5
distance undefined-proximal, X/1/C6, X/2/C1
show sticks, byres X/1/C6
show sticks, byres X/2/C1
select binding_site, binding_site + byres X/1/C6
select binding_site, binding_site + byres X/2/C1
distance undefined-vdwclash, X/1/O4, X/2/O5
show sticks, byres X/1/O4
show sticks, byres X/2/O5
select binding_site, binding_site + byres X/1/O4
select binding_site, binding_site + byres X/2/O5
distance undefined-proximal, X/1/O4, X/2/C5
show sticks, byres X/1/O4
show sticks, byres X/2/C5
select binding_site, binding_site + byres X/1/O4
select binding_site, binding_site + byres X/2/C5
distance undefined-proximal, X/1/O4, X/2/O6
show sticks, byres X/1/O4
show sticks, byres X/2/O6
select binding_site, binding_site + byres X/1/O4
select binding_site, binding_site + byres X/2/O6
distance undefined-proximal, X/1/O4, X/2/C6
show sticks, byres X/1/O4
show sticks, byres X/2/C6
select binding_site, binding_site + byres X/1/O4
select binding_site, binding_site + byres X/2/C6
distance undefined-proximal, X/1/C3, X/2/C2
show sticks, byres X/1/C3
show sticks, byres X/2/C2
select binding_site, binding_site + byres X/1/C3
select binding_site, binding_site + byres X/2/C2
distance undefined-proximal, X/1/C4, X/2/O2
show sticks, byres X/1/C4
show sticks, byres X/2/O2
select binding_site, binding_site + byres X/1/C4
select binding_site, binding_site + byres X/2/O2
distance undefined-proximal, X/1/C4, X/2/C2
show sticks, byres X/1/C4
show sticks, byres X/2/C2
select binding_site, binding_site + byres X/1/C4
select binding_site, binding_site + byres X/2/C2
distance undefined-proximal, X/1/C4, X/2/C3
show sticks, byres X/1/C4
show sticks, byres X/2/C3
select binding_site, binding_site + byres X/1/C4
select binding_site, binding_site + byres X/2/C3
distance undefined-proximal, A/356/CE3, X/2/O2
show sticks, byres A/356/CE3
show sticks, byres X/2/O2
select binding_site, binding_site + byres A/356/CE3
select binding_site, binding_site + byres X/2/O2
distance undefined-proximal, X/1/O6, X/2/O2
show sticks, byres X/1/O6
show sticks, byres X/2/O2
select binding_site, binding_site + byres X/1/O6
select binding_site, binding_site + byres X/2/O2
distance undefined-proximal, A/356/CZ3, X/2/O2
show sticks, byres A/356/CZ3
show sticks, byres X/2/O2
select binding_site, binding_site + byres A/356/CZ3
select binding_site, binding_site + byres X/2/O2
distance undefined-proximal, A/356/CZ3, X/2/C2
show sticks, byres A/356/CZ3
show sticks, byres X/2/C2
select binding_site, binding_site + byres A/356/CZ3
select binding_site, binding_site + byres X/2/C2
distance undefined-proximal, X/1/C5, X/2/O2
show sticks, byres X/1/C5
show sticks, byres X/2/O2
select binding_site, binding_site + byres X/1/C5
select binding_site, binding_site + byres X/2/O2
distance undefined-proximal, X/1/C5, X/2/C2
show sticks, byres X/1/C5
show sticks, byres X/2/C2
select binding_site, binding_site + byres X/1/C5
select binding_site, binding_site + byres X/2/C2
distance undefined-proximal, A/359/CB, A/361/OH
show sticks, byres A/359/CB
show sticks, byres A/361/OH
select binding_site, binding_site + byres A/359/CB
select binding_site, binding_site + byres A/361/OH
distance undefined-proximal, A/359/CB, A/361/CZ
show sticks, byres A/359/CB
show sticks, byres A/361/CZ
select binding_site, binding_site + byres A/359/CB
select binding_site, binding_site + byres A/361/CZ
distance undefined-proximal, A/359/CG, A/361/OH
show sticks, byres A/359/CG
show sticks, byres A/361/OH
select binding_site, binding_site + byres A/359/CG
select binding_site, binding_site + byres A/361/OH
distance undefined-proximal, A/359/CG, A/361/CZ
show sticks, byres A/359/CG
show sticks, byres A/361/CZ
select binding_site, binding_site + byres A/359/CG
select binding_site, binding_site + byres A/361/CZ
distance weakhbond-proximal, X/1/C6, X/2/O2
show sticks, byres X/1/C6
show sticks, byres X/2/O2
select binding_site, binding_site + byres X/1/C6
select binding_site, binding_site + byres X/2/O2
distance undefined-proximal, X/1/C6, X/2/C2
show sticks, byres X/1/C6
show sticks, byres X/2/C2
select binding_site, binding_site + byres X/1/C6
select binding_site, binding_site + byres X/2/C2
distance undefined-proximal, A/359/CD, X/2/O2
show sticks, byres A/359/CD
show sticks, byres X/2/O2
select binding_site, binding_site + byres A/359/CD
select binding_site, binding_site + byres X/2/O2
distance undefined-proximal, A/359/CD, A/361/OH
show sticks, byres A/359/CD
show sticks, byres A/361/OH
select binding_site, binding_site + byres A/359/CD
select binding_site, binding_site + byres A/361/OH
distance undefined-proximal, A/359/CD, A/361/CZ
show sticks, byres A/359/CD
show sticks, byres A/361/CZ
select binding_site, binding_site + byres A/359/CD
select binding_site, binding_site + byres A/361/CZ
distance undefined-proximal, A/359/CE, X/2/O2
show sticks, byres A/359/CE
show sticks, byres X/2/O2
select binding_site, binding_site + byres A/359/CE
select binding_site, binding_site + byres X/2/O2
distance undefined-proximal, A/359/CE, X/2/C2
show sticks, byres A/359/CE
show sticks, byres X/2/C2
select binding_site, binding_site + byres A/359/CE
select binding_site, binding_site + byres X/2/C2
distance undefined-proximal, A/359/CE, A/361/OH
show sticks, byres A/359/CE
show sticks, byres A/361/OH
select binding_site, binding_site + byres A/359/CE
select binding_site, binding_site + byres A/361/OH
distance undefined-proximal, A/359/CE, A/361/CZ
show sticks, byres A/359/CE
show sticks, byres A/361/CZ
select binding_site, binding_site + byres A/359/CE
select binding_site, binding_site + byres A/361/CZ
distance polar-vdwclash, X/1/O4, X/2/O2
show sticks, byres X/1/O4
show sticks, byres X/2/O2
select binding_site, binding_site + byres X/1/O4
select binding_site, binding_site + byres X/2/O2
distance weakpolar-vdwclash, X/1/O4, X/2/C2
show sticks, byres X/1/O4
show sticks, byres X/2/C2
select binding_site, binding_site + byres X/1/O4
select binding_site, binding_site + byres X/2/C2
distance undefined-proximal, X/1/O4, X/2/C3
show sticks, byres X/1/O4
show sticks, byres X/2/C3
select binding_site, binding_site + byres X/1/O4
select binding_site, binding_site + byres X/2/C3
distance undefined-proximal, A/359/NZ, X/2/O2
show sticks, byres A/359/NZ
show sticks, byres X/2/O2
select binding_site, binding_site + byres A/359/NZ
select binding_site, binding_site + byres X/2/O2
distance undefined-proximal, A/359/NZ, X/2/C2
show sticks, byres A/359/NZ
show sticks, byres X/2/C2
select binding_site, binding_site + byres A/359/NZ
select binding_site, binding_site + byres X/2/C2
distance undefined-proximal, A/359/NZ, A/361/OH
show sticks, byres A/359/NZ
show sticks, byres A/361/OH
select binding_site, binding_site + byres A/359/NZ
select binding_site, binding_site + byres A/361/OH
distance undefined-proximal, A/359/NZ, A/361/CZ
show sticks, byres A/359/NZ
show sticks, byres A/361/CZ
select binding_site, binding_site + byres A/359/NZ
select binding_site, binding_site + byres A/361/CZ
distance undefined-proximal, X/1/O4, X/2/O3
show sticks, byres X/1/O4
show sticks, byres X/2/O3
select binding_site, binding_site + byres X/1/O4
select binding_site, binding_site + byres X/2/O3
distance undefined-proximal, X/1/O4, X/2/O4
show sticks, byres X/1/O4
show sticks, byres X/2/O4
select binding_site, binding_site + byres X/1/O4
select binding_site, binding_site + byres X/2/O4
distance undefined-proximal, X/1/O4, X/2/C4
show sticks, byres X/1/O4
show sticks, byres X/2/C4
select binding_site, binding_site + byres X/1/O4
select binding_site, binding_site + byres X/2/C4
distance undefined-proximal, A/359/NZ, X/2/O3
show sticks, byres A/359/NZ
show sticks, byres X/2/O3
select binding_site, binding_site + byres A/359/NZ
select binding_site, binding_site + byres X/2/O3
distance undefined-proximal, X/1/C2, A/250/NE1
show sticks, byres X/1/C2
show sticks, byres A/250/NE1
select binding_site, binding_site + byres X/1/C2
select binding_site, binding_site + byres A/250/NE1
distance undefined-proximal, X/1/C2, A/250/CZ2
show sticks, byres X/1/C2
show sticks, byres A/250/CZ2
select binding_site, binding_site + byres X/1/C2
select binding_site, binding_site + byres A/250/CZ2
distance undefined-proximal, X/1/O7, A/250/NE1
show sticks, byres X/1/O7
show sticks, byres A/250/NE1
select binding_site, binding_site + byres X/1/O7
select binding_site, binding_site + byres A/250/NE1
distance undefined-proximal, X/1/O7, A/250/CE2
show sticks, byres X/1/O7
show sticks, byres A/250/CE2
select binding_site, binding_site + byres X/1/O7
select binding_site, binding_site + byres A/250/CE2
distance undefined-proximal, X/1/O7, A/250/CZ2
show sticks, byres X/1/O7
show sticks, byres A/250/CZ2
select binding_site, binding_site + byres X/1/O7
select binding_site, binding_site + byres A/250/CZ2
distance undefined-proximal, X/1/O7, A/250/CH2
show sticks, byres X/1/O7
show sticks, byres A/250/CH2
select binding_site, binding_site + byres X/1/O7
select binding_site, binding_site + byres A/250/CH2
distance undefined-proximal, X/1/C3, A/250/NE1
show sticks, byres X/1/C3
show sticks, byres A/250/NE1
select binding_site, binding_site + byres X/1/C3
select binding_site, binding_site + byres A/250/NE1
distance weakpolar-proximal, X/1/O3, X/2/C1
show sticks, byres X/1/O3
show sticks, byres X/2/C1
select binding_site, binding_site + byres X/1/O3
select binding_site, binding_site + byres X/2/C1
distance polar-vdwclash, X/1/O3, X/2/O5
show sticks, byres X/1/O3
show sticks, byres X/2/O5
select binding_site, binding_site + byres X/1/O3
select binding_site, binding_site + byres X/2/O5
distance undefined-proximal, X/1/O3, X/2/C5
show sticks, byres X/1/O3
show sticks, byres X/2/C5
select binding_site, binding_site + byres X/1/O3
select binding_site, binding_site + byres X/2/C5
distance polar-proximal, X/1/O3, X/2/O6
show sticks, byres X/1/O3
show sticks, byres X/2/O6
select binding_site, binding_site + byres X/1/O3
select binding_site, binding_site + byres X/2/O6
distance undefined-proximal, X/1/O3, X/2/C6
show sticks, byres X/1/O3
show sticks, byres X/2/C6
select binding_site, binding_site + byres X/1/O3
select binding_site, binding_site + byres X/2/C6
distance undefined-proximal, A/250/CD1, X/2/O6
show sticks, byres A/250/CD1
show sticks, byres X/2/O6
select binding_site, binding_site + byres A/250/CD1
select binding_site, binding_site + byres X/2/O6
distance undefined-proximal, A/356/CH2, X/2/C1
show sticks, byres A/356/CH2
show sticks, byres X/2/C1
select binding_site, binding_site + byres A/356/CH2
select binding_site, binding_site + byres X/2/C1
distance undefined-proximal, A/356/CH2, X/2/O5
show sticks, byres A/356/CH2
show sticks, byres X/2/O5
select binding_site, binding_site + byres A/356/CH2
select binding_site, binding_site + byres X/2/O5
distance undefined-proximal, A/247/CD, X/2/O5
show sticks, byres A/247/CD
show sticks, byres X/2/O5
select binding_site, binding_site + byres A/247/CD
select binding_site, binding_site + byres X/2/O5
distance undefined-proximal, A/247/NE2, X/2/C1
show sticks, byres A/247/NE2
show sticks, byres X/2/C1
select binding_site, binding_site + byres A/247/NE2
select binding_site, binding_site + byres X/2/C1
distance hbond-vdw, A/247/NE2, X/2/O5
show sticks, byres A/247/NE2
show sticks, byres X/2/O5
select binding_site, binding_site + byres A/247/NE2
select binding_site, binding_site + byres X/2/O5
distance polar-vdw, A/247/NE2, X/2/O5
show sticks, byres A/247/NE2
show sticks, byres X/2/O5
select binding_site, binding_site + byres A/247/NE2
select binding_site, binding_site + byres X/2/O5
distance undefined-proximal, A/247/NE2, X/2/C5
show sticks, byres A/247/NE2
show sticks, byres X/2/C5
select binding_site, binding_site + byres A/247/NE2
select binding_site, binding_site + byres X/2/C5
distance undefined-proximal, A/247/NE2, X/2/O6
show sticks, byres A/247/NE2
show sticks, byres X/2/O6
select binding_site, binding_site + byres A/247/NE2
select binding_site, binding_site + byres X/2/O6
distance undefined-proximal, A/247/NE2, X/2/C6
show sticks, byres A/247/NE2
show sticks, byres X/2/C6
select binding_site, binding_site + byres A/247/NE2
select binding_site, binding_site + byres X/2/C6
distance undefined-proximal, X/1/O3, X/2/C2
show sticks, byres X/1/O3
show sticks, byres X/2/C2
select binding_site, binding_site + byres X/1/O3
select binding_site, binding_site + byres X/2/C2
distance undefined-proximal, A/356/CE2, A/280/NE2
show sticks, byres A/356/CE2
show sticks, byres A/280/NE2
select binding_site, binding_site + byres A/356/CE2
select binding_site, binding_site + byres A/280/NE2
distance undefined-proximal, A/356/CZ2, A/280/NE2
show sticks, byres A/356/CZ2
show sticks, byres A/280/NE2
select binding_site, binding_site + byres A/356/CZ2
select binding_site, binding_site + byres A/280/NE2
distance undefined-proximal, A/356/CZ2, X/2/C2
show sticks, byres A/356/CZ2
show sticks, byres X/2/C2
select binding_site, binding_site + byres A/356/CZ2
select binding_site, binding_site + byres X/2/C2
distance undefined-proximal, A/356/CH2, X/2/O2
show sticks, byres A/356/CH2
show sticks, byres X/2/O2
select binding_site, binding_site + byres A/356/CH2
select binding_site, binding_site + byres X/2/O2
distance undefined-proximal, A/356/CH2, A/280/NE2
show sticks, byres A/356/CH2
show sticks, byres A/280/NE2
select binding_site, binding_site + byres A/356/CH2
select binding_site, binding_site + byres A/280/NE2
distance undefined-proximal, A/356/CH2, X/2/C2
show sticks, byres A/356/CH2
show sticks, byres X/2/C2
select binding_site, binding_site + byres A/356/CH2
select binding_site, binding_site + byres X/2/C2
distance undefined-proximal, A/247/NE2, A/280/NE2
show sticks, byres A/247/NE2
show sticks, byres A/280/NE2
select binding_site, binding_site + byres A/247/NE2
select binding_site, binding_site + byres A/280/NE2
distance undefined-proximal, A/247/NE2, X/2/C2
show sticks, byres A/247/NE2
show sticks, byres X/2/C2
select binding_site, binding_site + byres A/247/NE2
select binding_site, binding_site + byres X/2/C2
distance undefined-proximal, A/247/NE2, X/2/C3
show sticks, byres A/247/NE2
show sticks, byres X/2/C3
select binding_site, binding_site + byres A/247/NE2
select binding_site, binding_site + byres X/2/C3
distance undefined-proximal, A/247/CD, X/2/O4
show sticks, byres A/247/CD
show sticks, byres X/2/O4
select binding_site, binding_site + byres A/247/CD
select binding_site, binding_site + byres X/2/O4
distance hbond-vdwclash, A/247/NE2, X/2/O4
show sticks, byres A/247/NE2
show sticks, byres X/2/O4
select binding_site, binding_site + byres A/247/NE2
select binding_site, binding_site + byres X/2/O4
distance polar-vdwclash, A/247/NE2, X/2/O4
show sticks, byres A/247/NE2
show sticks, byres X/2/O4
select binding_site, binding_site + byres A/247/NE2
select binding_site, binding_site + byres X/2/O4
distance undefined-proximal, A/247/NE2, X/2/C4
show sticks, byres A/247/NE2
show sticks, byres X/2/C4
select binding_site, binding_site + byres A/247/NE2
select binding_site, binding_site + byres X/2/C4
distance undefined-proximal, X/1/C8, A/250/NE1
show sticks, byres X/1/C8
show sticks, byres A/250/NE1
select binding_site, binding_site + byres X/1/C8
select binding_site, binding_site + byres A/250/NE1
distance undefined-proximal, X/1/C8, A/250/CE2
show sticks, byres X/1/C8
show sticks, byres A/250/CE2
select binding_site, binding_site + byres X/1/C8
select binding_site, binding_site + byres A/250/CE2
distance undefined-proximal, X/1/C8, A/250/CZ3
show sticks, byres X/1/C8
show sticks, byres A/250/CZ3
select binding_site, binding_site + byres X/1/C8
select binding_site, binding_site + byres A/250/CZ3
distance hydrophobic-proximal, X/1/C8, A/250/CZ2
show sticks, byres X/1/C8
show sticks, byres A/250/CZ2
select binding_site, binding_site + byres X/1/C8
select binding_site, binding_site + byres A/250/CZ2
distance hydrophobic-proximal, X/1/C8, A/250/CH2
show sticks, byres X/1/C8
show sticks, byres A/250/CH2
select binding_site, binding_site + byres X/1/C8
select binding_site, binding_site + byres A/250/CH2
distance undefined-proximal, X/1/N2, A/250/NE1
show sticks, byres X/1/N2
show sticks, byres A/250/NE1
select binding_site, binding_site + byres X/1/N2
select binding_site, binding_site + byres A/250/NE1
distance undefined-proximal, X/1/N2, A/250/CE2
show sticks, byres X/1/N2
show sticks, byres A/250/CE2
select binding_site, binding_site + byres X/1/N2
select binding_site, binding_site + byres A/250/CE2
distance undefined-proximal, X/1/N2, A/250/CZ2
show sticks, byres X/1/N2
show sticks, byres A/250/CZ2
select binding_site, binding_site + byres X/1/N2
select binding_site, binding_site + byres A/250/CZ2
distance undefined-proximal, X/1/C7, A/250/NE1
show sticks, byres X/1/C7
show sticks, byres A/250/NE1
select binding_site, binding_site + byres X/1/C7
select binding_site, binding_site + byres A/250/NE1
distance undefined-proximal, X/1/C7, A/250/CE2
show sticks, byres X/1/C7
show sticks, byres A/250/CE2
select binding_site, binding_site + byres X/1/C7
select binding_site, binding_site + byres A/250/CE2
distance undefined-proximal, X/1/C7, A/250/CZ2
show sticks, byres X/1/C7
show sticks, byres A/250/CZ2
select binding_site, binding_site + byres X/1/C7
select binding_site, binding_site + byres A/250/CZ2
distance undefined-proximal, X/1/C7, A/250/CH2
show sticks, byres X/1/C7
show sticks, byres A/250/CH2
select binding_site, binding_site + byres X/1/C7
select binding_site, binding_site + byres A/250/CH2
distance hbond-vdwclash, X/1/O3, A/250/NE1
show sticks, byres X/1/O3
show sticks, byres A/250/NE1
select binding_site, binding_site + byres X/1/O3
select binding_site, binding_site + byres A/250/NE1
distance polar-vdwclash, X/1/O3, A/250/NE1
show sticks, byres X/1/O3
show sticks, byres A/250/NE1
select binding_site, binding_site + byres X/1/O3
select binding_site, binding_site + byres A/250/NE1
distance undefined-proximal, X/1/O3, A/250/CE2
show sticks, byres X/1/O3
show sticks, byres A/250/CE2
select binding_site, binding_site + byres X/1/O3
select binding_site, binding_site + byres A/250/CE2
distance undefined-proximal, X/1/O3, A/250/CZ2
show sticks, byres X/1/O3
show sticks, byres A/250/CZ2
select binding_site, binding_site + byres X/1/O3
select binding_site, binding_site + byres A/250/CZ2
distance undefined-proximal, A/247/OE1, A/250/NE1
show sticks, byres A/247/OE1
show sticks, byres A/250/NE1
select binding_site, binding_site + byres A/247/OE1
select binding_site, binding_site + byres A/250/NE1
distance undefined-proximal, A/247/CD, A/250/NE1
show sticks, byres A/247/CD
show sticks, byres A/250/NE1
select binding_site, binding_site + byres A/247/CD
select binding_site, binding_site + byres A/250/NE1
distance undefined-proximal, A/247/NE2, A/250/NE1
show sticks, byres A/247/NE2
show sticks, byres A/250/NE1
select binding_site, binding_site + byres A/247/NE2
select binding_site, binding_site + byres A/250/NE1
distance undefined-proximal, A/250/CG, A/278/CD1
show sticks, byres A/250/CG
show sticks, byres A/278/CD1
select binding_site, binding_site + byres A/250/CG
select binding_site, binding_site + byres A/278/CD1
distance undefined-proximal, A/250/CD1, A/247/CG
show sticks, byres A/250/CD1
show sticks, byres A/247/CG
select binding_site, binding_site + byres A/250/CD1
select binding_site, binding_site + byres A/247/CG
distance undefined-proximal, A/250/CD1, A/278/CD1
show sticks, byres A/250/CD1
show sticks, byres A/278/CD1
select binding_site, binding_site + byres A/250/CD1
select binding_site, binding_site + byres A/278/CD1
distance undefined-proximal, A/250/CD1, A/278/CE1
show sticks, byres A/250/CD1
show sticks, byres A/278/CE1
select binding_site, binding_site + byres A/250/CD1
select binding_site, binding_site + byres A/278/CE1
distance undefined-proximal, A/356/CZ2, A/247/CG
show sticks, byres A/356/CZ2
show sticks, byres A/247/CG
select binding_site, binding_site + byres A/356/CZ2
select binding_site, binding_site + byres A/247/CG
distance undefined-proximal, A/280/CE1, A/247/CG
show sticks, byres A/280/CE1
show sticks, byres A/247/CG
select binding_site, binding_site + byres A/280/CE1
select binding_site, binding_site + byres A/247/CG
distance undefined-proximal, A/280/ND1, A/247/CG
show sticks, byres A/280/ND1
show sticks, byres A/247/CG
select binding_site, binding_site + byres A/280/ND1
select binding_site, binding_site + byres A/247/CG
distance undefined-proximal, A/247/CD, A/280/CG
show sticks, byres A/247/CD
show sticks, byres A/280/CG
select binding_site, binding_site + byres A/247/CD
select binding_site, binding_site + byres A/280/CG
distance undefined-proximal, A/247/CD, A/280/CD2
show sticks, byres A/247/CD
show sticks, byres A/280/CD2
select binding_site, binding_site + byres A/247/CD
select binding_site, binding_site + byres A/280/CD2
distance undefined-proximal, A/247/CD, A/278/CD1
show sticks, byres A/247/CD
show sticks, byres A/278/CD1
select binding_site, binding_site + byres A/247/CD
select binding_site, binding_site + byres A/278/CD1
distance undefined-proximal, A/247/CD, A/278/CE1
show sticks, byres A/247/CD
show sticks, byres A/278/CE1
select binding_site, binding_site + byres A/247/CD
select binding_site, binding_site + byres A/278/CE1
distance undefined-proximal, A/247/CB, A/280/CG
show sticks, byres A/247/CB
show sticks, byres A/280/CG
select binding_site, binding_site + byres A/247/CB
select binding_site, binding_site + byres A/280/CG
distance hydrophobic-proximal, A/247/CB, A/280/CB
show sticks, byres A/247/CB
show sticks, byres A/280/CB
select binding_site, binding_site + byres A/247/CB
select binding_site, binding_site + byres A/280/CB
distance undefined-proximal, A/247/CB, A/280/CD2
show sticks, byres A/247/CB
show sticks, byres A/280/CD2
select binding_site, binding_site + byres A/247/CB
select binding_site, binding_site + byres A/280/CD2
distance undefined-proximal, A/247/CB, A/280/CA
show sticks, byres A/247/CB
show sticks, byres A/280/CA
select binding_site, binding_site + byres A/247/CB
select binding_site, binding_site + byres A/280/CA
distance hydrophobic-proximal, A/247/CB, A/278/CD1
show sticks, byres A/247/CB
show sticks, byres A/278/CD1
select binding_site, binding_site + byres A/247/CB
select binding_site, binding_site + byres A/278/CD1
distance hydrophobic-proximal, A/247/CB, A/278/CE1
show sticks, byres A/247/CB
show sticks, byres A/278/CE1
select binding_site, binding_site + byres A/247/CB
select binding_site, binding_site + byres A/278/CE1
distance undefined-proximal, A/247/NE2, A/280/CD2
show sticks, byres A/247/NE2
show sticks, byres A/280/CD2
select binding_site, binding_site + byres A/247/NE2
select binding_site, binding_site + byres A/280/CD2
distance undefined-proximal, A/247/NE2, A/278/CE1
show sticks, byres A/247/NE2
show sticks, byres A/278/CE1
select binding_site, binding_site + byres A/247/NE2
select binding_site, binding_site + byres A/278/CE1
distance undefined-proximal, A/247/NE2, A/317/OE1
show sticks, byres A/247/NE2
show sticks, byres A/317/OE1
select binding_site, binding_site + byres A/247/NE2
select binding_site, binding_site + byres A/317/OE1
distance undefined-proximal, X/1/C8, A/249/CZ3
show sticks, byres X/1/C8
show sticks, byres A/249/CZ3
select binding_site, binding_site + byres X/1/C8
select binding_site, binding_site + byres A/249/CZ3
distance undefined-proximal, A/249/CE3, X/1/O1
show sticks, byres A/249/CE3
show sticks, byres X/1/O1
select binding_site, binding_site + byres A/249/CE3
select binding_site, binding_site + byres X/1/O1
distance undefined-proximal, A/249/CE3, X/1/C1
show sticks, byres A/249/CE3
show sticks, byres X/1/C1
select binding_site, binding_site + byres A/249/CE3
select binding_site, binding_site + byres X/1/C1
distance undefined-proximal, A/249/CE3, X/1/C2
show sticks, byres A/249/CE3
show sticks, byres X/1/C2
select binding_site, binding_site + byres A/249/CE3
select binding_site, binding_site + byres X/1/C2
distance undefined-proximal, A/249/CE3, X/1/C3
show sticks, byres A/249/CE3
show sticks, byres X/1/C3
select binding_site, binding_site + byres A/249/CE3
select binding_site, binding_site + byres X/1/C3
distance undefined-proximal, A/249/CD2, X/1/O1
show sticks, byres A/249/CD2
show sticks, byres X/1/O1
select binding_site, binding_site + byres A/249/CD2
select binding_site, binding_site + byres X/1/O1
distance undefined-proximal, A/249/CD2, X/1/C1
show sticks, byres A/249/CD2
show sticks, byres X/1/C1
select binding_site, binding_site + byres A/249/CD2
select binding_site, binding_site + byres X/1/C1
distance undefined-proximal, A/249/CD2, X/1/O5
show sticks, byres A/249/CD2
show sticks, byres X/1/O5
select binding_site, binding_site + byres A/249/CD2
select binding_site, binding_site + byres X/1/O5
distance undefined-proximal, A/249/CD2, X/1/C2
show sticks, byres A/249/CD2
show sticks, byres X/1/C2
select binding_site, binding_site + byres A/249/CD2
select binding_site, binding_site + byres X/1/C2
distance undefined-proximal, A/249/CD2, X/1/C3
show sticks, byres A/249/CD2
show sticks, byres X/1/C3
select binding_site, binding_site + byres A/249/CD2
select binding_site, binding_site + byres X/1/C3
distance undefined-proximal, A/249/CD2, X/1/C4
show sticks, byres A/249/CD2
show sticks, byres X/1/C4
select binding_site, binding_site + byres A/249/CD2
select binding_site, binding_site + byres X/1/C4
distance undefined-proximal, A/249/CB, X/1/C2
show sticks, byres A/249/CB
show sticks, byres X/1/C2
select binding_site, binding_site + byres A/249/CB
select binding_site, binding_site + byres X/1/C2
distance undefined-proximal, A/249/CB, X/1/C3
show sticks, byres A/249/CB
show sticks, byres X/1/C3
select binding_site, binding_site + byres A/249/CB
select binding_site, binding_site + byres X/1/C3
distance undefined-proximal, X/1/N2, A/249/CH2
show sticks, byres X/1/N2
show sticks, byres A/249/CH2
select binding_site, binding_site + byres X/1/N2
select binding_site, binding_site + byres A/249/CH2
distance undefined-proximal, X/1/N2, A/249/CZ3
show sticks, byres X/1/N2
show sticks, byres A/249/CZ3
select binding_site, binding_site + byres X/1/N2
select binding_site, binding_site + byres A/249/CZ3
distance undefined-proximal, X/1/N2, A/249/CZ2
show sticks, byres X/1/N2
show sticks, byres A/249/CZ2
select binding_site, binding_site + byres X/1/N2
select binding_site, binding_site + byres A/249/CZ2
distance undefined-proximal, X/1/N2, A/249/CE2
show sticks, byres X/1/N2
show sticks, byres A/249/CE2
select binding_site, binding_site + byres X/1/N2
select binding_site, binding_site + byres A/249/CE2
distance undefined-proximal, X/1/C7, A/249/CZ3
show sticks, byres X/1/C7
show sticks, byres A/249/CZ3
select binding_site, binding_site + byres X/1/C7
select binding_site, binding_site + byres A/249/CZ3
distance undefined-proximal, A/249/NE1, X/1/C1
show sticks, byres A/249/NE1
show sticks, byres X/1/C1
select binding_site, binding_site + byres A/249/NE1
select binding_site, binding_site + byres X/1/C1
distance undefined-proximal, A/249/NE1, X/1/O5
show sticks, byres A/249/NE1
show sticks, byres X/1/O5
select binding_site, binding_site + byres A/249/NE1
select binding_site, binding_site + byres X/1/O5
distance undefined-proximal, A/249/NE1, X/1/C2
show sticks, byres A/249/NE1
show sticks, byres X/1/C2
select binding_site, binding_site + byres A/249/NE1
select binding_site, binding_site + byres X/1/C2
distance undefined-proximal, A/249/NE1, X/1/C3
show sticks, byres A/249/NE1
show sticks, byres X/1/C3
select binding_site, binding_site + byres A/249/NE1
select binding_site, binding_site + byres X/1/C3
distance undefined-proximal, A/249/NE1, X/1/C4
show sticks, byres A/249/NE1
show sticks, byres X/1/C4
select binding_site, binding_site + byres A/249/NE1
select binding_site, binding_site + byres X/1/C4
distance undefined-proximal, A/249/CG, X/1/C1
show sticks, byres A/249/CG
show sticks, byres X/1/C1
select binding_site, binding_site + byres A/249/CG
select binding_site, binding_site + byres X/1/C1
distance undefined-proximal, A/249/CG, X/1/C2
show sticks, byres A/249/CG
show sticks, byres X/1/C2
select binding_site, binding_site + byres A/249/CG
select binding_site, binding_site + byres X/1/C2
distance undefined-proximal, A/249/CG, X/1/C3
show sticks, byres A/249/CG
show sticks, byres X/1/C3
select binding_site, binding_site + byres A/249/CG
select binding_site, binding_site + byres X/1/C3
distance undefined-proximal, A/249/CG, X/1/C4
show sticks, byres A/249/CG
show sticks, byres X/1/C4
select binding_site, binding_site + byres A/249/CG
select binding_site, binding_site + byres X/1/C4
distance undefined-proximal, A/249/CD1, X/1/C1
show sticks, byres A/249/CD1
show sticks, byres X/1/C1
select binding_site, binding_site + byres A/249/CD1
select binding_site, binding_site + byres X/1/C1
distance undefined-proximal, A/249/CD1, X/1/C2
show sticks, byres A/249/CD1
show sticks, byres X/1/C2
select binding_site, binding_site + byres A/249/CD1
select binding_site, binding_site + byres X/1/C2
distance undefined-proximal, A/249/CD1, X/1/C3
show sticks, byres A/249/CD1
show sticks, byres X/1/C3
select binding_site, binding_site + byres A/249/CD1
select binding_site, binding_site + byres X/1/C3
distance undefined-proximal, A/249/CD1, X/1/C4
show sticks, byres A/249/CD1
show sticks, byres X/1/C4
select binding_site, binding_site + byres A/249/CD1
select binding_site, binding_site + byres X/1/C4
distance undefined-proximal, A/356/CH2, X/1/C4
show sticks, byres A/356/CH2
show sticks, byres X/1/C4
select binding_site, binding_site + byres A/356/CH2
select binding_site, binding_site + byres X/1/C4
distance undefined-proximal, A/247/NE2, X/1/C3
show sticks, byres A/247/NE2
show sticks, byres X/1/C3
select binding_site, binding_site + byres A/247/NE2
select binding_site, binding_site + byres X/1/C3
distance undefined-proximal, A/247/NE2, X/1/C4
show sticks, byres A/247/NE2
show sticks, byres X/1/C4
select binding_site, binding_site + byres A/247/NE2
select binding_site, binding_site + byres X/1/C4
distance undefined-proximal, A/249/CD2, A/340/OD2
show sticks, byres A/249/CD2
show sticks, byres A/340/OD2
select binding_site, binding_site + byres A/249/CD2
select binding_site, binding_site + byres A/340/OD2
distance undefined-proximal, A/249/CD2, X/1/C5
show sticks, byres A/249/CD2
show sticks, byres X/1/C5
select binding_site, binding_site + byres A/249/CD2
select binding_site, binding_site + byres X/1/C5
distance undefined-proximal, A/249/NE1, A/340/CG
show sticks, byres A/249/NE1
show sticks, byres A/340/CG
select binding_site, binding_site + byres A/249/NE1
select binding_site, binding_site + byres A/340/CG
distance hbond-vdwclash, A/249/NE1, A/340/OD2
show sticks, byres A/249/NE1
show sticks, byres A/340/OD2
select binding_site, binding_site + byres A/249/NE1
select binding_site, binding_site + byres A/340/OD2
distance polar-vdwclash, A/249/NE1, A/340/OD2
show sticks, byres A/249/NE1
show sticks, byres A/340/OD2
select binding_site, binding_site + byres A/249/NE1
select binding_site, binding_site + byres A/340/OD2
distance undefined-proximal, A/249/NE1, A/356/CE3
show sticks, byres A/249/NE1
show sticks, byres A/356/CE3
select binding_site, binding_site + byres A/249/NE1
select binding_site, binding_site + byres A/356/CE3
distance aromatic-proximal, A/249/NE1, A/356/CZ3
show sticks, byres A/249/NE1
show sticks, byres A/356/CZ3
select binding_site, binding_site + byres A/249/NE1
select binding_site, binding_site + byres A/356/CZ3
distance undefined-proximal, A/249/NE1, X/1/C5
show sticks, byres A/249/NE1
show sticks, byres X/1/C5
select binding_site, binding_site + byres A/249/NE1
select binding_site, binding_site + byres X/1/C5
distance undefined-proximal, A/249/CG, X/1/C5
show sticks, byres A/249/CG
show sticks, byres X/1/C5
select binding_site, binding_site + byres A/249/CG
select binding_site, binding_site + byres X/1/C5
distance undefined-proximal, A/249/CD1, A/340/CG
show sticks, byres A/249/CD1
show sticks, byres A/340/CG
select binding_site, binding_site + byres A/249/CD1
select binding_site, binding_site + byres A/340/CG
distance undefined-proximal, A/249/CD1, A/340/OD2
show sticks, byres A/249/CD1
show sticks, byres A/340/OD2
select binding_site, binding_site + byres A/249/CD1
select binding_site, binding_site + byres A/340/OD2
distance undefined-proximal, A/249/CD1, A/356/CZ3
show sticks, byres A/249/CD1
show sticks, byres A/356/CZ3
select binding_site, binding_site + byres A/249/CD1
select binding_site, binding_site + byres A/356/CZ3
distance undefined-proximal, A/249/CD1, X/1/C5
show sticks, byres A/249/CD1
show sticks, byres X/1/C5
select binding_site, binding_site + byres A/249/CD1
select binding_site, binding_site + byres X/1/C5
distance undefined-proximal, A/356/CD2, A/340/CG
show sticks, byres A/356/CD2
show sticks, byres A/340/CG
select binding_site, binding_site + byres A/356/CD2
select binding_site, binding_site + byres A/340/CG
distance undefined-proximal, A/356/CD2, A/340/OD2
show sticks, byres A/356/CD2
show sticks, byres A/340/OD2
select binding_site, binding_site + byres A/356/CD2
select binding_site, binding_site + byres A/340/OD2
distance undefined-proximal, A/356/CE2, A/340/CG
show sticks, byres A/356/CE2
show sticks, byres A/340/CG
select binding_site, binding_site + byres A/356/CE2
select binding_site, binding_site + byres A/340/CG
distance undefined-proximal, A/356/CE2, A/340/OD2
show sticks, byres A/356/CE2
show sticks, byres A/340/OD2
select binding_site, binding_site + byres A/356/CE2
select binding_site, binding_site + byres A/340/OD2
distance undefined-proximal, A/356/CZ2, A/340/CG
show sticks, byres A/356/CZ2
show sticks, byres A/340/CG
select binding_site, binding_site + byres A/356/CZ2
select binding_site, binding_site + byres A/340/CG
distance undefined-proximal, A/356/CZ2, A/340/OD2
show sticks, byres A/356/CZ2
show sticks, byres A/340/OD2
select binding_site, binding_site + byres A/356/CZ2
select binding_site, binding_site + byres A/340/OD2
distance undefined-proximal, A/249/NE1, X/1/O4
show sticks, byres A/249/NE1
show sticks, byres X/1/O4
select binding_site, binding_site + byres A/249/NE1
select binding_site, binding_site + byres X/1/O4
distance undefined-proximal, A/249/CG, X/1/O4
show sticks, byres A/249/CG
show sticks, byres X/1/O4
select binding_site, binding_site + byres A/249/CG
select binding_site, binding_site + byres X/1/O4
distance undefined-proximal, A/249/CD1, X/1/O4
show sticks, byres A/249/CD1
show sticks, byres X/1/O4
select binding_site, binding_site + byres A/249/CD1
select binding_site, binding_site + byres X/1/O4
distance undefined-proximal, A/356/CZ2, X/1/O4
show sticks, byres A/356/CZ2
show sticks, byres X/1/O4
select binding_site, binding_site + byres A/356/CZ2
select binding_site, binding_site + byres X/1/O4
distance undefined-proximal, A/356/CH2, A/340/CG
show sticks, byres A/356/CH2
show sticks, byres A/340/CG
select binding_site, binding_site + byres A/356/CH2
select binding_site, binding_site + byres A/340/CG
distance undefined-proximal, A/356/CH2, A/340/OD2
show sticks, byres A/356/CH2
show sticks, byres A/340/OD2
select binding_site, binding_site + byres A/356/CH2
select binding_site, binding_site + byres A/340/OD2
distance undefined-proximal, A/356/CH2, X/1/C5
show sticks, byres A/356/CH2
show sticks, byres X/1/C5
select binding_site, binding_site + byres A/356/CH2
select binding_site, binding_site + byres X/1/C5
distance undefined-proximal, A/247/NE2, A/356/CZ3
show sticks, byres A/247/NE2
show sticks, byres A/356/CZ3
select binding_site, binding_site + byres A/247/NE2
select binding_site, binding_site + byres A/356/CZ3
distance undefined-proximal, A/356/CH2, X/1/O4
show sticks, byres A/356/CH2
show sticks, byres X/1/O4
select binding_site, binding_site + byres A/356/CH2
select binding_site, binding_site + byres X/1/O4
distance undefined-proximal, A/247/CD, X/1/O4
show sticks, byres A/247/CD
show sticks, byres X/1/O4
select binding_site, binding_site + byres A/247/CD
select binding_site, binding_site + byres X/1/O4
distance undefined-proximal, A/247/NE2, X/1/O4
show sticks, byres A/247/NE2
show sticks, byres X/1/O4
select binding_site, binding_site + byres A/247/NE2
select binding_site, binding_site + byres X/1/O4
distance undefined-proximal, A/342/CD2, A/340/OD2
show sticks, byres A/342/CD2
show sticks, byres A/340/OD2
select binding_site, binding_site + byres A/342/CD2
select binding_site, binding_site + byres A/340/OD2
distance undefined-proximal, A/249/CH2, A/340/OD2
show sticks, byres A/249/CH2
show sticks, byres A/340/OD2
select binding_site, binding_site + byres A/249/CH2
select binding_site, binding_site + byres A/340/OD2
distance undefined-proximal, A/249/CZ2, A/340/CG
show sticks, byres A/249/CZ2
show sticks, byres A/340/CG
select binding_site, binding_site + byres A/249/CZ2
select binding_site, binding_site + byres A/340/CG
distance weakpolar-proximal, A/249/CZ2, A/340/OD2
show sticks, byres A/249/CZ2
show sticks, byres A/340/OD2
select binding_site, binding_site + byres A/249/CZ2
select binding_site, binding_site + byres A/340/OD2
distance undefined-proximal, A/249/CZ2, X/1/O6
show sticks, byres A/249/CZ2
show sticks, byres X/1/O6
select binding_site, binding_site + byres A/249/CZ2
select binding_site, binding_site + byres X/1/O6
distance undefined-proximal, A/249/CZ2, X/1/C5
show sticks, byres A/249/CZ2
show sticks, byres X/1/C5
select binding_site, binding_site + byres A/249/CZ2
select binding_site, binding_site + byres X/1/C5
distance undefined-proximal, A/249/CE2, A/340/CG
show sticks, byres A/249/CE2
show sticks, byres A/340/CG
select binding_site, binding_site + byres A/249/CE2
select binding_site, binding_site + byres A/340/CG
distance undefined-proximal, A/249/CE2, A/340/OD2
show sticks, byres A/249/CE2
show sticks, byres A/340/OD2
select binding_site, binding_site + byres A/249/CE2
select binding_site, binding_site + byres A/340/OD2
distance undefined-proximal, A/249/CE2, X/1/C5
show sticks, byres A/249/CE2
show sticks, byres X/1/C5
select binding_site, binding_site + byres A/249/CE2
select binding_site, binding_site + byres X/1/C5
distance undefined-proximal, X/1/C4, A/356/CZ3
show sticks, byres X/1/C4
show sticks, byres A/356/CZ3
select binding_site, binding_site + byres X/1/C4
select binding_site, binding_site + byres A/356/CZ3
distance undefined-proximal, X/1/O1, A/249/CH2
show sticks, byres X/1/O1
show sticks, byres A/249/CH2
select binding_site, binding_site + byres X/1/O1
select binding_site, binding_site + byres A/249/CH2
distance undefined-proximal, X/1/O1, A/249/CZ3
show sticks, byres X/1/O1
show sticks, byres A/249/CZ3
select binding_site, binding_site + byres X/1/O1
select binding_site, binding_site + byres A/249/CZ3
distance undefined-proximal, X/1/O1, A/249/CZ2
show sticks, byres X/1/O1
show sticks, byres A/249/CZ2
select binding_site, binding_site + byres X/1/O1
select binding_site, binding_site + byres A/249/CZ2
distance undefined-proximal, X/1/O1, A/249/CE2
show sticks, byres X/1/O1
show sticks, byres A/249/CE2
select binding_site, binding_site + byres X/1/O1
select binding_site, binding_site + byres A/249/CE2
distance undefined-proximal, X/1/C1, A/249/CH2
show sticks, byres X/1/C1
show sticks, byres A/249/CH2
select binding_site, binding_site + byres X/1/C1
select binding_site, binding_site + byres A/249/CH2
distance undefined-proximal, X/1/C1, A/249/CZ3
show sticks, byres X/1/C1
show sticks, byres A/249/CZ3
select binding_site, binding_site + byres X/1/C1
select binding_site, binding_site + byres A/249/CZ3
distance undefined-proximal, X/1/C1, A/249/CZ2
show sticks, byres X/1/C1
show sticks, byres A/249/CZ2
select binding_site, binding_site + byres X/1/C1
select binding_site, binding_site + byres A/249/CZ2
distance undefined-proximal, X/1/C1, A/249/CE2
show sticks, byres X/1/C1
show sticks, byres A/249/CE2
select binding_site, binding_site + byres X/1/C1
select binding_site, binding_site + byres A/249/CE2
distance undefined-proximal, X/1/O5, A/249/CH2
show sticks, byres X/1/O5
show sticks, byres A/249/CH2
select binding_site, binding_site + byres X/1/O5
select binding_site, binding_site + byres A/249/CH2
distance undefined-proximal, X/1/O5, A/249/CZ2
show sticks, byres X/1/O5
show sticks, byres A/249/CZ2
select binding_site, binding_site + byres X/1/O5
select binding_site, binding_site + byres A/249/CZ2
distance undefined-proximal, X/1/O5, A/249/CE2
show sticks, byres X/1/O5
show sticks, byres A/249/CE2
select binding_site, binding_site + byres X/1/O5
select binding_site, binding_site + byres A/249/CE2
distance undefined-proximal, X/1/C2, A/249/CZ3
show sticks, byres X/1/C2
show sticks, byres A/249/CZ3
select binding_site, binding_site + byres X/1/C2
select binding_site, binding_site + byres A/249/CZ3
distance undefined-proximal, X/1/C2, A/249/CZ2
show sticks, byres X/1/C2
show sticks, byres A/249/CZ2
select binding_site, binding_site + byres X/1/C2
select binding_site, binding_site + byres A/249/CZ2
distance undefined-proximal, X/1/C2, A/249/CE2
show sticks, byres X/1/C2
show sticks, byres A/249/CE2
select binding_site, binding_site + byres X/1/C2
select binding_site, binding_site + byres A/249/CE2
distance undefined-proximal, X/1/C3, A/249/CE2
show sticks, byres X/1/C3
show sticks, byres A/249/CE2
select binding_site, binding_site + byres X/1/C3
select binding_site, binding_site + byres A/249/CE2
distance undefined-proximal, X/1/C4, A/249/CE2
show sticks, byres X/1/C4
show sticks, byres A/249/CE2
select binding_site, binding_site + byres X/1/C4
select binding_site, binding_site + byres A/249/CE2
distance undefined-proximal, A/249/CH2, A/342/NE2
show sticks, byres A/249/CH2
show sticks, byres A/342/NE2
select binding_site, binding_site + byres A/249/CH2
select binding_site, binding_site + byres A/342/NE2
distance aromatic-proximal, A/249/CH2, A/342/CD2
show sticks, byres A/249/CH2
show sticks, byres A/342/CD2
select binding_site, binding_site + byres A/249/CH2
select binding_site, binding_site + byres A/342/CD2
distance undefined-proximal, A/249/CZ2, A/342/NE2
show sticks, byres A/249/CZ2
show sticks, byres A/342/NE2
select binding_site, binding_site + byres A/249/CZ2
select binding_site, binding_site + byres A/342/NE2
distance aromatic-proximal, A/249/CZ2, A/342/CD2
show sticks, byres A/249/CZ2
show sticks, byres A/342/CD2
select binding_site, binding_site + byres A/249/CZ2
select binding_site, binding_site + byres A/342/CD2
distance undefined-proximal, A/359/CG, X/1/O6
show sticks, byres A/359/CG
show sticks, byres X/1/O6
select binding_site, binding_site + byres A/359/CG
select binding_site, binding_site + byres X/1/O6
distance undefined-proximal, X/1/C6, A/356/CZ3
show sticks, byres X/1/C6
show sticks, byres A/356/CZ3
select binding_site, binding_site + byres X/1/C6
select binding_site, binding_site + byres A/356/CZ3
distance undefined-proximal, A/359/CE, A/356/CE3
show sticks, byres A/359/CE
show sticks, byres A/356/CE3
select binding_site, binding_site + byres A/359/CE
select binding_site, binding_site + byres A/356/CE3
distance undefined-proximal, A/359/CE, A/356/CZ3
show sticks, byres A/359/CE
show sticks, byres A/356/CZ3
select binding_site, binding_site + byres A/359/CE
select binding_site, binding_site + byres A/356/CZ3
distance undefined-proximal, X/1/O4, A/356/CZ3
show sticks, byres X/1/O4
show sticks, byres A/356/CZ3
select binding_site, binding_site + byres X/1/O4
select binding_site, binding_site + byres A/356/CZ3
distance undefined-proximal, A/359/NZ, A/356/CE3
show sticks, byres A/359/NZ
show sticks, byres A/356/CE3
select binding_site, binding_site + byres A/359/NZ
select binding_site, binding_site + byres A/356/CE3
distance undefined-proximal, A/359/NZ, A/356/CZ3
show sticks, byres A/359/NZ
show sticks, byres A/356/CZ3
select binding_site, binding_site + byres A/359/NZ
select binding_site, binding_site + byres A/356/CZ3
distance undefined-proximal, A/356/CE3, A/340/CG
show sticks, byres A/356/CE3
show sticks, byres A/340/CG
select binding_site, binding_site + byres A/356/CE3
select binding_site, binding_site + byres A/340/CG
distance undefined-proximal, A/356/CE3, A/340/OD2
show sticks, byres A/356/CE3
show sticks, byres A/340/OD2
select binding_site, binding_site + byres A/356/CE3
select binding_site, binding_site + byres A/340/OD2
distance undefined-proximal, X/1/O6, A/340/OD2
show sticks, byres X/1/O6
show sticks, byres A/340/OD2
select binding_site, binding_site + byres X/1/O6
select binding_site, binding_site + byres A/340/OD2
distance undefined-proximal, A/356/CZ3, A/340/CG
show sticks, byres A/356/CZ3
show sticks, byres A/340/CG
select binding_site, binding_site + byres A/356/CZ3
select binding_site, binding_site + byres A/340/CG
distance undefined-proximal, A/356/CZ3, A/340/OD2
show sticks, byres A/356/CZ3
show sticks, byres A/340/OD2
select binding_site, binding_site + byres A/356/CZ3
select binding_site, binding_site + byres A/340/OD2
distance undefined-proximal, A/356/CZ3, X/1/O6
show sticks, byres A/356/CZ3
show sticks, byres X/1/O6
select binding_site, binding_site + byres A/356/CZ3
select binding_site, binding_site + byres X/1/O6
distance undefined-proximal, X/1/C5, A/356/CZ3
show sticks, byres X/1/C5
show sticks, byres A/356/CZ3
select binding_site, binding_site + byres X/1/C5
select binding_site, binding_site + byres A/356/CZ3
distance undefined-proximal, X/1/C8, A/249/CG
show sticks, byres X/1/C8
show sticks, byres A/249/CG
select binding_site, binding_site + byres X/1/C8
select binding_site, binding_site + byres A/249/CG
distance undefined-proximal, A/249/C, A/247/OE1
show sticks, byres A/249/C
show sticks, byres A/247/OE1
select binding_site, binding_site + byres A/249/C
select binding_site, binding_site + byres A/247/OE1
distance undefined-proximal, A/249/CB, A/247/OE1
show sticks, byres A/249/CB
show sticks, byres A/247/OE1
select binding_site, binding_site + byres A/249/CB
select binding_site, binding_site + byres A/247/OE1
distance undefined-proximal, A/249/CB, A/247/CD
show sticks, byres A/249/CB
show sticks, byres A/247/CD
select binding_site, binding_site + byres A/249/CB
select binding_site, binding_site + byres A/247/CD
distance undefined-proximal, A/249/CB, A/247/NE2
show sticks, byres A/249/CB
show sticks, byres A/247/NE2
select binding_site, binding_site + byres A/249/CB
select binding_site, binding_site + byres A/247/NE2
distance undefined-proximal, X/1/N2, A/249/NE1
show sticks, byres X/1/N2
show sticks, byres A/249/NE1
select binding_site, binding_site + byres X/1/N2
select binding_site, binding_site + byres A/249/NE1
distance undefined-proximal, X/1/N2, A/249/CG
show sticks, byres X/1/N2
show sticks, byres A/249/CG
select binding_site, binding_site + byres X/1/N2
select binding_site, binding_site + byres A/249/CG
distance undefined-proximal, X/1/N2, A/249/CD1
show sticks, byres X/1/N2
show sticks, byres A/249/CD1
select binding_site, binding_site + byres X/1/N2
select binding_site, binding_site + byres A/249/CD1
distance undefined-proximal, X/1/C7, A/249/CG
show sticks, byres X/1/C7
show sticks, byres A/249/CG
select binding_site, binding_site + byres X/1/C7
select binding_site, binding_site + byres A/249/CG
distance undefined-proximal, X/1/O3, A/249/CG
show sticks, byres X/1/O3
show sticks, byres A/249/CG
select binding_site, binding_site + byres X/1/O3
select binding_site, binding_site + byres A/249/CG
distance undefined-proximal, X/1/O3, A/249/CD1
show sticks, byres X/1/O3
show sticks, byres A/249/CD1
select binding_site, binding_site + byres X/1/O3
select binding_site, binding_site + byres A/249/CD1
distance undefined-proximal, A/250/CG, A/247/OE1
show sticks, byres A/250/CG
show sticks, byres A/247/OE1
select binding_site, binding_site + byres A/250/CG
select binding_site, binding_site + byres A/247/OE1
distance undefined-proximal, A/250/CG, A/247/CD
show sticks, byres A/250/CG
show sticks, byres A/247/CD
select binding_site, binding_site + byres A/250/CG
select binding_site, binding_site + byres A/247/CD
distance undefined-proximal, X/1/O3, A/247/OE1
show sticks, byres X/1/O3
show sticks, byres A/247/OE1
select binding_site, binding_site + byres X/1/O3
select binding_site, binding_site + byres A/247/OE1
distance undefined-proximal, X/1/O3, A/247/CD
show sticks, byres X/1/O3
show sticks, byres A/247/CD
select binding_site, binding_site + byres X/1/O3
select binding_site, binding_site + byres A/247/CD
distance undefined-proximal, X/1/O3, A/247/NE2
show sticks, byres X/1/O3
show sticks, byres A/247/NE2
select binding_site, binding_site + byres X/1/O3
select binding_site, binding_site + byres A/247/NE2
distance undefined-proximal, A/250/CD1, A/247/OE1
show sticks, byres A/250/CD1
show sticks, byres A/247/OE1
select binding_site, binding_site + byres A/250/CD1
select binding_site, binding_site + byres A/247/OE1
distance undefined-proximal, A/250/CD1, A/247/CD
show sticks, byres A/250/CD1
show sticks, byres A/247/CD
select binding_site, binding_site + byres A/250/CD1
select binding_site, binding_site + byres A/247/CD
distance undefined-proximal, A/250/CD1, A/247/CB
show sticks, byres A/250/CD1
show sticks, byres A/247/CB
select binding_site, binding_site + byres A/250/CD1
select binding_site, binding_site + byres A/247/CB
distance undefined-proximal, A/250/CD1, A/247/NE2
show sticks, byres A/250/CD1
show sticks, byres A/247/NE2
select binding_site, binding_site + byres A/250/CD1
select binding_site, binding_site + byres A/247/NE2
distance undefined-proximal, X/1/N2, A/249/CE3
show sticks, byres X/1/N2
show sticks, byres A/249/CE3
select binding_site, binding_site + byres X/1/N2
select binding_site, binding_site + byres A/249/CE3
distance undefined-proximal, X/1/N2, A/249/CD2
show sticks, byres X/1/N2
show sticks, byres A/249/CD2
select binding_site, binding_site + byres X/1/N2
select binding_site, binding_site + byres A/249/CD2
distance undefined-proximal, X/1/N2, A/249/CB
show sticks, byres X/1/N2
show sticks, byres A/249/CB
select binding_site, binding_site + byres X/1/N2
select binding_site, binding_site + byres A/249/CB
distance undefined-proximal, X/1/C7, A/249/CE3
show sticks, byres X/1/C7
show sticks, byres A/249/CE3
select binding_site, binding_site + byres X/1/C7
select binding_site, binding_site + byres A/249/CE3
distance undefined-proximal, X/1/C7, A/249/CD2
show sticks, byres X/1/C7
show sticks, byres A/249/CD2
select binding_site, binding_site + byres X/1/C7
select binding_site, binding_site + byres A/249/CD2
distance undefined-proximal, X/1/C7, A/249/CB
show sticks, byres X/1/C7
show sticks, byres A/249/CB
select binding_site, binding_site + byres X/1/C7
select binding_site, binding_site + byres A/249/CB
distance undefined-proximal, X/1/O3, A/249/CD2
show sticks, byres X/1/O3
show sticks, byres A/249/CD2
select binding_site, binding_site + byres X/1/O3
select binding_site, binding_site + byres A/249/CD2
distance undefined-proximal, X/1/O3, A/249/CB
show sticks, byres X/1/O3
show sticks, byres A/249/CB
select binding_site, binding_site + byres X/1/O3
select binding_site, binding_site + byres A/249/CB
distance undefined-proximal, A/250/CD2, X/1/C8
show sticks, byres A/250/CD2
show sticks, byres X/1/C8
select binding_site, binding_site + byres A/250/CD2
select binding_site, binding_site + byres X/1/C8
distance undefined-proximal, A/250/CE3, X/1/C8
show sticks, byres A/250/CE3
show sticks, byres X/1/C8
select binding_site, binding_site + byres A/250/CE3
select binding_site, binding_site + byres X/1/C8
distance hydrophobic-proximal, X/1/C8, A/249/CE3
show sticks, byres X/1/C8
show sticks, byres A/249/CE3
select binding_site, binding_site + byres X/1/C8
select binding_site, binding_site + byres A/249/CE3
distance undefined-proximal, X/1/C8, A/249/CD2
show sticks, byres X/1/C8
show sticks, byres A/249/CD2
select binding_site, binding_site + byres X/1/C8
select binding_site, binding_site + byres A/249/CD2
distance undefined-proximal, X/1/C8, A/249/O
show sticks, byres X/1/C8
show sticks, byres A/249/O
select binding_site, binding_site + byres X/1/C8
select binding_site, binding_site + byres A/249/O
distance hydrophobic-proximal, X/1/C8, A/249/CB
show sticks, byres X/1/C8
show sticks, byres A/249/CB
select binding_site, binding_site + byres X/1/C8
select binding_site, binding_site + byres A/249/CB
distance undefined-proximal, A/250/CD1, X/1/O3
show sticks, byres A/250/CD1
show sticks, byres X/1/O3
select binding_site, binding_site + byres A/250/CD1
select binding_site, binding_site + byres X/1/O3
distance aromatic-proximal, A/356/CH2, A/249/NE1
show sticks, byres A/356/CH2
show sticks, byres A/249/NE1
select binding_site, binding_site + byres A/356/CH2
select binding_site, binding_site + byres A/249/NE1
distance undefined-proximal, A/356/CH2, A/249/CG
show sticks, byres A/356/CH2
show sticks, byres A/249/CG
select binding_site, binding_site + byres A/356/CH2
select binding_site, binding_site + byres A/249/CG
distance aromatic-proximal, A/356/CH2, A/249/CD1
show sticks, byres A/356/CH2
show sticks, byres A/249/CD1
select binding_site, binding_site + byres A/356/CH2
select binding_site, binding_site + byres A/249/CD1
distance undefined-proximal, A/247/OE1, A/249/NE1
show sticks, byres A/247/OE1
show sticks, byres A/249/NE1
select binding_site, binding_site + byres A/247/OE1
select binding_site, binding_site + byres A/249/NE1
distance undefined-proximal, A/247/OE1, A/249/CA
show sticks, byres A/247/OE1
show sticks, byres A/249/CA
select binding_site, binding_site + byres A/247/OE1
select binding_site, binding_site + byres A/249/CA
distance undefined-proximal, A/247/OE1, A/249/CG
show sticks, byres A/247/OE1
show sticks, byres A/249/CG
select binding_site, binding_site + byres A/247/OE1
select binding_site, binding_site + byres A/249/CG
distance undefined-proximal, A/247/OE1, A/249/CD1
show sticks, byres A/247/OE1
show sticks, byres A/249/CD1
select binding_site, binding_site + byres A/247/OE1
select binding_site, binding_site + byres A/249/CD1
distance undefined-proximal, A/247/OE1, A/356/CZ2
show sticks, byres A/247/OE1
show sticks, byres A/356/CZ2
select binding_site, binding_site + byres A/247/OE1
select binding_site, binding_site + byres A/356/CZ2
distance undefined-proximal, A/280/CE1, A/356/CD2
show sticks, byres A/280/CE1
show sticks, byres A/356/CD2
select binding_site, binding_site + byres A/280/CE1
select binding_site, binding_site + byres A/356/CD2
distance aromatic-proximal, A/280/CE1, A/356/CE2
show sticks, byres A/280/CE1
show sticks, byres A/356/CE2
select binding_site, binding_site + byres A/280/CE1
select binding_site, binding_site + byres A/356/CE2
distance aromatic-vdw, A/280/CE1, A/356/CZ2
show sticks, byres A/280/CE1
show sticks, byres A/356/CZ2
select binding_site, binding_site + byres A/280/CE1
select binding_site, binding_site + byres A/356/CZ2
distance undefined-proximal, A/280/ND1, A/356/CE2
show sticks, byres A/280/ND1
show sticks, byres A/356/CE2
select binding_site, binding_site + byres A/280/ND1
select binding_site, binding_site + byres A/356/CE2
distance undefined-proximal, A/280/ND1, A/356/CZ2
show sticks, byres A/280/ND1
show sticks, byres A/356/CZ2
select binding_site, binding_site + byres A/280/ND1
select binding_site, binding_site + byres A/356/CZ2
distance undefined-proximal, A/247/CD, A/249/CA
show sticks, byres A/247/CD
show sticks, byres A/249/CA
select binding_site, binding_site + byres A/247/CD
select binding_site, binding_site + byres A/249/CA
distance undefined-proximal, A/247/CD, A/249/CG
show sticks, byres A/247/CD
show sticks, byres A/249/CG
select binding_site, binding_site + byres A/247/CD
select binding_site, binding_site + byres A/249/CG
distance undefined-proximal, A/247/CD, A/249/CD1
show sticks, byres A/247/CD
show sticks, byres A/249/CD1
select binding_site, binding_site + byres A/247/CD
select binding_site, binding_site + byres A/249/CD1
distance undefined-proximal, A/247/CD, A/356/CZ2
show sticks, byres A/247/CD
show sticks, byres A/356/CZ2
select binding_site, binding_site + byres A/247/CD
select binding_site, binding_site + byres A/356/CZ2
distance undefined-proximal, A/247/NE2, A/249/CG
show sticks, byres A/247/NE2
show sticks, byres A/249/CG
select binding_site, binding_site + byres A/247/NE2
select binding_site, binding_site + byres A/249/CG
distance undefined-proximal, A/247/NE2, A/249/CD1
show sticks, byres A/247/NE2
show sticks, byres A/249/CD1
select binding_site, binding_site + byres A/247/NE2
select binding_site, binding_site + byres A/249/CD1
distance undefined-proximal, A/247/NE2, A/356/CZ2
show sticks, byres A/247/NE2
show sticks, byres A/356/CZ2
select binding_site, binding_site + byres A/247/NE2
select binding_site, binding_site + byres A/356/CZ2
distance undefined-proximal, A/356/CZ2, A/249/NE1
show sticks, byres A/356/CZ2
show sticks, byres A/249/NE1
select binding_site, binding_site + byres A/356/CZ2
select binding_site, binding_site + byres A/249/NE1
distance undefined-proximal, A/356/CZ2, A/249/CD1
show sticks, byres A/356/CZ2
show sticks, byres A/249/CD1
select binding_site, binding_site + byres A/356/CZ2
select binding_site, binding_site + byres A/249/CD1
distance undefined-proximal, A/247/OE1, A/356/CH2
show sticks, byres A/247/OE1
show sticks, byres A/356/CH2
select binding_site, binding_site + byres A/247/OE1
select binding_site, binding_site + byres A/356/CH2
distance undefined-proximal, A/280/CE1, A/356/CH2
show sticks, byres A/280/CE1
show sticks, byres A/356/CH2
select binding_site, binding_site + byres A/280/CE1
select binding_site, binding_site + byres A/356/CH2
distance undefined-proximal, A/247/CD, A/356/CH2
show sticks, byres A/247/CD
show sticks, byres A/356/CH2
select binding_site, binding_site + byres A/247/CD
select binding_site, binding_site + byres A/356/CH2
distance undefined-proximal, A/247/CD, A/280/ND1
show sticks, byres A/247/CD
show sticks, byres A/280/ND1
select binding_site, binding_site + byres A/247/CD
select binding_site, binding_site + byres A/280/ND1
distance undefined-proximal, A/247/CB, A/280/ND1
show sticks, byres A/247/CB
show sticks, byres A/280/ND1
select binding_site, binding_site + byres A/247/CB
select binding_site, binding_site + byres A/280/ND1
distance undefined-proximal, A/247/NE2, A/356/CH2
show sticks, byres A/247/NE2
show sticks, byres A/356/CH2
select binding_site, binding_site + byres A/247/NE2
select binding_site, binding_site + byres A/356/CH2
distance undefined-proximal, A/250/NE1, X/2/O5
show sticks, byres A/250/NE1
show sticks, byres X/2/O5
select binding_site, binding_site + byres A/250/NE1
select binding_site, binding_site + byres X/2/O5
distance undefined-proximal, A/250/NE1, X/2/O6
show sticks, byres A/250/NE1
show sticks, byres X/2/O6
select binding_site, binding_site + byres A/250/NE1
select binding_site, binding_site + byres X/2/O6
distance undefined-proximal, A/250/NE1, X/2/C6
show sticks, byres A/250/NE1
show sticks, byres X/2/C6
select binding_site, binding_site + byres A/250/NE1
select binding_site, binding_site + byres X/2/C6
distance undefined-proximal, A/250/NE1, A/259/CG2
show sticks, byres A/250/NE1
show sticks, byres A/259/CG2
select binding_site, binding_site + byres A/250/NE1
select binding_site, binding_site + byres A/259/CG2
distance undefined-proximal, A/250/CE2, X/2/O6
show sticks, byres A/250/CE2
show sticks, byres X/2/O6
select binding_site, binding_site + byres A/250/CE2
select binding_site, binding_site + byres X/2/O6
distance undefined-proximal, A/250/CE2, A/259/CG2
show sticks, byres A/250/CE2
show sticks, byres A/259/CG2
select binding_site, binding_site + byres A/250/CE2
select binding_site, binding_site + byres A/259/CG2
distance undefined-proximal, A/250/CZ2, X/2/O6
show sticks, byres A/250/CZ2
show sticks, byres X/2/O6
select binding_site, binding_site + byres A/250/CZ2
select binding_site, binding_site + byres X/2/O6
distance hydrophobic-proximal, A/250/CZ2, A/259/CG2
show sticks, byres A/250/CZ2
show sticks, byres A/259/CG2
select binding_site, binding_site + byres A/250/CZ2
select binding_site, binding_site + byres A/259/CG2
distance hydrophobic-proximal, A/250/CH2, A/259/CG2
show sticks, byres A/250/CH2
show sticks, byres A/259/CG2
select binding_site, binding_site + byres A/250/CH2
select binding_site, binding_site + byres A/259/CG2
distance undefined-proximal, A/259/OG1, X/2/C5
show sticks, byres A/259/OG1
show sticks, byres X/2/C5
select binding_site, binding_site + byres A/259/OG1
select binding_site, binding_site + byres X/2/C5
distance polar-vdwclash, A/259/OG1, X/2/O6
show sticks, byres A/259/OG1
show sticks, byres X/2/O6
select binding_site, binding_site + byres A/259/OG1
select binding_site, binding_site + byres X/2/O6
distance weakpolar-proximal, A/259/OG1, X/2/C6
show sticks, byres A/259/OG1
show sticks, byres X/2/C6
select binding_site, binding_site + byres A/259/OG1
select binding_site, binding_site + byres X/2/C6
distance undefined-proximal, A/259/OG1, A/314/CE2
show sticks, byres A/259/OG1
show sticks, byres A/314/CE2
select binding_site, binding_site + byres A/259/OG1
select binding_site, binding_site + byres A/314/CE2
distance undefined-proximal, A/259/OG1, A/314/NE1
show sticks, byres A/259/OG1
show sticks, byres A/314/NE1
select binding_site, binding_site + byres A/259/OG1
select binding_site, binding_site + byres A/314/NE1
distance undefined-proximal, A/259/OG1, A/314/CD1
show sticks, byres A/259/OG1
show sticks, byres A/314/CD1
select binding_site, binding_site + byres A/259/OG1
select binding_site, binding_site + byres A/314/CD1
distance undefined-proximal, A/259/OG1, A/314/CG
show sticks, byres A/259/OG1
show sticks, byres A/314/CG
select binding_site, binding_site + byres A/259/OG1
select binding_site, binding_site + byres A/314/CG
distance undefined-proximal, A/278/CZ, X/2/O6
show sticks, byres A/278/CZ
show sticks, byres X/2/O6
select binding_site, binding_site + byres A/278/CZ
select binding_site, binding_site + byres X/2/O6
distance undefined-proximal, A/278/CZ, X/2/C6
show sticks, byres A/278/CZ
show sticks, byres X/2/C6
select binding_site, binding_site + byres A/278/CZ
select binding_site, binding_site + byres X/2/C6
distance undefined-proximal, A/259/CB, X/2/O6
show sticks, byres A/259/CB
show sticks, byres X/2/O6
select binding_site, binding_site + byres A/259/CB
select binding_site, binding_site + byres X/2/O6
distance undefined-proximal, A/259/CB, X/2/C6
show sticks, byres A/259/CB
show sticks, byres X/2/C6
select binding_site, binding_site + byres A/259/CB
select binding_site, binding_site + byres X/2/C6
distance undefined-proximal, A/259/CB, A/314/NE1
show sticks, byres A/259/CB
show sticks, byres A/314/NE1
select binding_site, binding_site + byres A/259/CB
select binding_site, binding_site + byres A/314/NE1
distance undefined-proximal, A/259/CB, A/314/CD1
show sticks, byres A/259/CB
show sticks, byres A/314/CD1
select binding_site, binding_site + byres A/259/CB
select binding_site, binding_site + byres A/314/CD1
distance undefined-proximal, A/259/C, A/314/CD1
show sticks, byres A/259/C
show sticks, byres A/314/CD1
select binding_site, binding_site + byres A/259/C
select binding_site, binding_site + byres A/314/CD1
distance undefined-proximal, A/259/O, A/314/NE1
show sticks, byres A/259/O
show sticks, byres A/314/NE1
select binding_site, binding_site + byres A/259/O
select binding_site, binding_site + byres A/314/NE1
distance undefined-proximal, A/259/O, A/314/CD1
show sticks, byres A/259/O
show sticks, byres A/314/CD1
select binding_site, binding_site + byres A/259/O
select binding_site, binding_site + byres A/314/CD1
distance undefined-proximal, A/259/O, A/314/CG
show sticks, byres A/259/O
show sticks, byres A/314/CG
select binding_site, binding_site + byres A/259/O
select binding_site, binding_site + byres A/314/CG
distance undefined-proximal, A/278/CE1, X/2/O6
show sticks, byres A/278/CE1
show sticks, byres X/2/O6
select binding_site, binding_site + byres A/278/CE1
select binding_site, binding_site + byres X/2/O6
distance undefined-proximal, A/278/CE1, X/2/C6
show sticks, byres A/278/CE1
show sticks, byres X/2/C6
select binding_site, binding_site + byres A/278/CE1
select binding_site, binding_site + byres X/2/C6
distance undefined-proximal, A/317/OE1, X/2/C5
show sticks, byres A/317/OE1
show sticks, byres X/2/C5
select binding_site, binding_site + byres A/317/OE1
select binding_site, binding_site + byres X/2/C5
distance undefined-proximal, A/317/OE1, X/2/O6
show sticks, byres A/317/OE1
show sticks, byres X/2/O6
select binding_site, binding_site + byres A/317/OE1
select binding_site, binding_site + byres X/2/O6
distance undefined-proximal, A/317/OE1, X/2/C6
show sticks, byres A/317/OE1
show sticks, byres X/2/C6
select binding_site, binding_site + byres A/317/OE1
select binding_site, binding_site + byres X/2/C6
distance undefined-proximal, A/317/CD, X/2/C6
show sticks, byres A/317/CD
show sticks, byres X/2/C6
select binding_site, binding_site + byres A/317/CD
select binding_site, binding_site + byres X/2/C6
distance undefined-proximal, A/278/OH, X/2/C5
show sticks, byres A/278/OH
show sticks, byres X/2/C5
select binding_site, binding_site + byres A/278/OH
select binding_site, binding_site + byres X/2/C5
distance undefined-proximal, A/278/OH, X/2/O6
show sticks, byres A/278/OH
show sticks, byres X/2/O6
select binding_site, binding_site + byres A/278/OH
select binding_site, binding_site + byres X/2/O6
distance undefined-proximal, A/278/OH, X/2/C6
show sticks, byres A/278/OH
show sticks, byres X/2/C6
select binding_site, binding_site + byres A/278/OH
select binding_site, binding_site + byres X/2/C6
distance undefined-proximal, A/317/OE1, A/314/CG
show sticks, byres A/317/OE1
show sticks, byres A/314/CG
select binding_site, binding_site + byres A/317/OE1
select binding_site, binding_site + byres A/314/CG
distance undefined-proximal, A/278/OH, A/314/NE1
show sticks, byres A/278/OH
show sticks, byres A/314/NE1
select binding_site, binding_site + byres A/278/OH
select binding_site, binding_site + byres A/314/NE1
distance undefined-proximal, A/278/OH, A/314/CD1
show sticks, byres A/278/OH
show sticks, byres A/314/CD1
select binding_site, binding_site + byres A/278/OH
select binding_site, binding_site + byres A/314/CD1
distance undefined-proximal, A/278/OH, A/314/CG
show sticks, byres A/278/OH
show sticks, byres A/314/CG
select binding_site, binding_site + byres A/278/OH
select binding_site, binding_site + byres A/314/CG
distance undefined-proximal, A/247/CG, A/280/NE2
show sticks, byres A/247/CG
show sticks, byres A/280/NE2
select binding_site, binding_site + byres A/247/CG
select binding_site, binding_site + byres A/280/NE2
distance undefined-proximal, A/247/CG, X/2/O4
show sticks, byres A/247/CG
show sticks, byres X/2/O4
select binding_site, binding_site + byres A/247/CG
select binding_site, binding_site + byres X/2/O4
distance undefined-proximal, A/280/CD2, X/2/O3
show sticks, byres A/280/CD2
show sticks, byres X/2/O3
select binding_site, binding_site + byres A/280/CD2
select binding_site, binding_site + byres X/2/O3
distance undefined-proximal, A/280/CD2, X/2/O4
show sticks, byres A/280/CD2
show sticks, byres X/2/O4
select binding_site, binding_site + byres A/280/CD2
select binding_site, binding_site + byres X/2/O4
distance undefined-proximal, A/278/CE1, X/2/O4
show sticks, byres A/278/CE1
show sticks, byres X/2/O4
select binding_site, binding_site + byres A/278/CE1
select binding_site, binding_site + byres X/2/O4
distance undefined-proximal, A/317/OE2, A/280/NE2
show sticks, byres A/317/OE2
show sticks, byres A/280/NE2
select binding_site, binding_site + byres A/317/OE2
select binding_site, binding_site + byres A/280/NE2
distance undefined-proximal, A/317/OE1, X/2/C3
show sticks, byres A/317/OE1
show sticks, byres X/2/C3
select binding_site, binding_site + byres A/317/OE1
select binding_site, binding_site + byres X/2/C3
distance undefined-proximal, A/317/OE2, X/2/O3
show sticks, byres A/317/OE2
show sticks, byres X/2/O3
select binding_site, binding_site + byres A/317/OE2
select binding_site, binding_site + byres X/2/O3
distance polar-proximal, A/317/OE2, X/2/O4
show sticks, byres A/317/OE2
show sticks, byres X/2/O4
select binding_site, binding_site + byres A/317/OE2
select binding_site, binding_site + byres X/2/O4
distance undefined-proximal, A/317/OE2, X/2/C4
show sticks, byres A/317/OE2
show sticks, byres X/2/C4
select binding_site, binding_site + byres A/317/OE2
select binding_site, binding_site + byres X/2/C4
distance undefined-proximal, A/317/OE1, X/2/O3
show sticks, byres A/317/OE1
show sticks, byres X/2/O3
select binding_site, binding_site + byres A/317/OE1
select binding_site, binding_site + byres X/2/O3
distance polar-vdwclash, A/317/OE1, X/2/O4
show sticks, byres A/317/OE1
show sticks, byres X/2/O4
select binding_site, binding_site + byres A/317/OE1
select binding_site, binding_site + byres X/2/O4
distance weakpolar-proximal, A/317/OE1, X/2/C4
show sticks, byres A/317/OE1
show sticks, byres X/2/C4
select binding_site, binding_site + byres A/317/OE1
select binding_site, binding_site + byres X/2/C4
distance undefined-proximal, A/317/OE1, A/314/CB
show sticks, byres A/317/OE1
show sticks, byres A/314/CB
select binding_site, binding_site + byres A/317/OE1
select binding_site, binding_site + byres A/314/CB
distance undefined-proximal, A/317/CD, X/2/O4
show sticks, byres A/317/CD
show sticks, byres X/2/O4
select binding_site, binding_site + byres A/317/CD
select binding_site, binding_site + byres X/2/O4
distance undefined-proximal, A/317/CD, X/2/C4
show sticks, byres A/317/CD
show sticks, byres X/2/C4
select binding_site, binding_site + byres A/317/CD
select binding_site, binding_site + byres X/2/C4
distance undefined-proximal, A/317/CD, A/314/CB
show sticks, byres A/317/CD
show sticks, byres A/314/CB
select binding_site, binding_site + byres A/317/CD
select binding_site, binding_site + byres A/314/CB
distance undefined-proximal, A/278/OH, X/2/O4
show sticks, byres A/278/OH
show sticks, byres X/2/O4
select binding_site, binding_site + byres A/278/OH
select binding_site, binding_site + byres X/2/O4
distance undefined-proximal, A/278/OH, A/314/CB
show sticks, byres A/278/OH
show sticks, byres A/314/CB
select binding_site, binding_site + byres A/278/OH
select binding_site, binding_site + byres A/314/CB
distance undefined-proximal, A/317/CG, X/2/O4
show sticks, byres A/317/CG
show sticks, byres X/2/O4
select binding_site, binding_site + byres A/317/CG
select binding_site, binding_site + byres X/2/O4
distance undefined-proximal, A/317/CG, A/314/CB
show sticks, byres A/317/CG
show sticks, byres A/314/CB
select binding_site, binding_site + byres A/317/CG
select binding_site, binding_site + byres A/314/CB
distance hydrophobic-proximal, A/317/CB, A/314/CB
show sticks, byres A/317/CB
show sticks, byres A/314/CB
select binding_site, binding_site + byres A/317/CB
select binding_site, binding_site + byres A/314/CB
distance undefined-proximal, X/2/C1, A/314/CZ3
show sticks, byres X/2/C1
show sticks, byres A/314/CZ3
select binding_site, binding_site + byres X/2/C1
select binding_site, binding_site + byres A/314/CZ3
distance undefined-proximal, X/2/O5, A/314/CZ3
show sticks, byres X/2/O5
show sticks, byres A/314/CZ3
select binding_site, binding_site + byres X/2/O5
select binding_site, binding_site + byres A/314/CZ3
distance undefined-proximal, X/2/C5, A/314/CZ3
show sticks, byres X/2/C5
show sticks, byres A/314/CZ3
select binding_site, binding_site + byres X/2/C5
select binding_site, binding_site + byres A/314/CZ3
distance undefined-proximal, X/2/C5, A/314/CE3
show sticks, byres X/2/C5
show sticks, byres A/314/CE3
select binding_site, binding_site + byres X/2/C5
select binding_site, binding_site + byres A/314/CE3
distance undefined-proximal, X/2/C6, A/314/CZ3
show sticks, byres X/2/C6
show sticks, byres A/314/CZ3
select binding_site, binding_site + byres X/2/C6
select binding_site, binding_site + byres A/314/CZ3
distance undefined-proximal, X/2/C6, A/314/CE3
show sticks, byres X/2/C6
show sticks, byres A/314/CE3
select binding_site, binding_site + byres X/2/C6
select binding_site, binding_site + byres A/314/CE3
distance undefined-proximal, X/2/C6, A/314/CB
show sticks, byres X/2/C6
show sticks, byres A/314/CB
select binding_site, binding_site + byres X/2/C6
select binding_site, binding_site + byres A/314/CB
distance undefined-proximal, A/314/CH2, X/2/C3
show sticks, byres A/314/CH2
show sticks, byres X/2/C3
select binding_site, binding_site + byres A/314/CH2
select binding_site, binding_site + byres X/2/C3
distance undefined-proximal, A/314/CD2, X/2/C3
show sticks, byres A/314/CD2
show sticks, byres X/2/C3
select binding_site, binding_site + byres A/314/CD2
select binding_site, binding_site + byres X/2/C3
distance undefined-proximal, A/314/CH2, X/2/C4
show sticks, byres A/314/CH2
show sticks, byres X/2/C4
select binding_site, binding_site + byres A/314/CH2
select binding_site, binding_site + byres X/2/C4
distance undefined-proximal, A/314/CZ2, X/2/C4
show sticks, byres A/314/CZ2
show sticks, byres X/2/C4
select binding_site, binding_site + byres A/314/CZ2
select binding_site, binding_site + byres X/2/C4
distance undefined-proximal, A/314/CE2, X/2/C4
show sticks, byres A/314/CE2
show sticks, byres X/2/C4
select binding_site, binding_site + byres A/314/CE2
select binding_site, binding_site + byres X/2/C4
distance undefined-proximal, A/314/CD2, X/2/C4
show sticks, byres A/314/CD2
show sticks, byres X/2/C4
select binding_site, binding_site + byres A/314/CD2
select binding_site, binding_site + byres X/2/C4
distance undefined-proximal, A/314/CG, X/2/C4
show sticks, byres A/314/CG
show sticks, byres X/2/C4
select binding_site, binding_site + byres A/314/CG
select binding_site, binding_site + byres X/2/C4
distance undefined-proximal, A/314/CH2, X/2/C1
show sticks, byres A/314/CH2
show sticks, byres X/2/C1
select binding_site, binding_site + byres A/314/CH2
select binding_site, binding_site + byres X/2/C1
distance undefined-proximal, A/314/CH2, X/2/O5
show sticks, byres A/314/CH2
show sticks, byres X/2/O5
select binding_site, binding_site + byres A/314/CH2
select binding_site, binding_site + byres X/2/O5
distance undefined-proximal, A/314/CH2, X/2/C5
show sticks, byres A/314/CH2
show sticks, byres X/2/C5
select binding_site, binding_site + byres A/314/CH2
select binding_site, binding_site + byres X/2/C5
distance undefined-proximal, A/314/CH2, X/2/O6
show sticks, byres A/314/CH2
show sticks, byres X/2/O6
select binding_site, binding_site + byres A/314/CH2
select binding_site, binding_site + byres X/2/O6
distance undefined-proximal, A/314/CH2, X/2/C6
show sticks, byres A/314/CH2
show sticks, byres X/2/C6
select binding_site, binding_site + byres A/314/CH2
select binding_site, binding_site + byres X/2/C6
distance undefined-proximal, A/314/CZ2, X/2/C1
show sticks, byres A/314/CZ2
show sticks, byres X/2/C1
select binding_site, binding_site + byres A/314/CZ2
select binding_site, binding_site + byres X/2/C1
distance undefined-proximal, A/314/CZ2, X/2/O5
show sticks, byres A/314/CZ2
show sticks, byres X/2/O5
select binding_site, binding_site + byres A/314/CZ2
select binding_site, binding_site + byres X/2/O5
distance undefined-proximal, A/314/CZ2, X/2/C5
show sticks, byres A/314/CZ2
show sticks, byres X/2/C5
select binding_site, binding_site + byres A/314/CZ2
select binding_site, binding_site + byres X/2/C5
distance undefined-proximal, A/314/CZ2, X/2/O6
show sticks, byres A/314/CZ2
show sticks, byres X/2/O6
select binding_site, binding_site + byres A/314/CZ2
select binding_site, binding_site + byres X/2/O6
distance undefined-proximal, A/314/CZ2, X/2/C6
show sticks, byres A/314/CZ2
show sticks, byres X/2/C6
select binding_site, binding_site + byres A/314/CZ2
select binding_site, binding_site + byres X/2/C6
distance undefined-proximal, A/314/CE2, X/2/O5
show sticks, byres A/314/CE2
show sticks, byres X/2/O5
select binding_site, binding_site + byres A/314/CE2
select binding_site, binding_site + byres X/2/O5
distance undefined-proximal, A/314/CE2, X/2/C5
show sticks, byres A/314/CE2
show sticks, byres X/2/C5
select binding_site, binding_site + byres A/314/CE2
select binding_site, binding_site + byres X/2/C5
distance undefined-proximal, A/314/CE2, X/2/O6
show sticks, byres A/314/CE2
show sticks, byres X/2/O6
select binding_site, binding_site + byres A/314/CE2
select binding_site, binding_site + byres X/2/O6
distance undefined-proximal, A/314/CE2, X/2/C6
show sticks, byres A/314/CE2
show sticks, byres X/2/C6
select binding_site, binding_site + byres A/314/CE2
select binding_site, binding_site + byres X/2/C6
distance undefined-proximal, A/314/NE1, X/2/C5
show sticks, byres A/314/NE1
show sticks, byres X/2/C5
select binding_site, binding_site + byres A/314/NE1
select binding_site, binding_site + byres X/2/C5
distance undefined-proximal, A/314/NE1, X/2/O6
show sticks, byres A/314/NE1
show sticks, byres X/2/O6
select binding_site, binding_site + byres A/314/NE1
select binding_site, binding_site + byres X/2/O6
distance undefined-proximal, A/314/NE1, X/2/C6
show sticks, byres A/314/NE1
show sticks, byres X/2/C6
select binding_site, binding_site + byres A/314/NE1
select binding_site, binding_site + byres X/2/C6
distance undefined-proximal, A/314/NE1, A/259/CG2
show sticks, byres A/314/NE1
show sticks, byres A/259/CG2
select binding_site, binding_site + byres A/314/NE1
select binding_site, binding_site + byres A/259/CG2
distance undefined-proximal, A/314/CD2, X/2/C5
show sticks, byres A/314/CD2
show sticks, byres X/2/C5
select binding_site, binding_site + byres A/314/CD2
select binding_site, binding_site + byres X/2/C5
distance undefined-proximal, A/314/CD2, X/2/O6
show sticks, byres A/314/CD2
show sticks, byres X/2/O6
select binding_site, binding_site + byres A/314/CD2
select binding_site, binding_site + byres X/2/O6
distance undefined-proximal, A/314/CD2, X/2/C6
show sticks, byres A/314/CD2
show sticks, byres X/2/C6
select binding_site, binding_site + byres A/314/CD2
select binding_site, binding_site + byres X/2/C6
distance undefined-proximal, A/314/CD1, X/2/C5
show sticks, byres A/314/CD1
show sticks, byres X/2/C5
select binding_site, binding_site + byres A/314/CD1
select binding_site, binding_site + byres X/2/C5
distance undefined-proximal, A/314/CD1, X/2/O6
show sticks, byres A/314/CD1
show sticks, byres X/2/O6
select binding_site, binding_site + byres A/314/CD1
select binding_site, binding_site + byres X/2/O6
distance undefined-proximal, A/314/CD1, X/2/C6
show sticks, byres A/314/CD1
show sticks, byres X/2/C6
select binding_site, binding_site + byres A/314/CD1
select binding_site, binding_site + byres X/2/C6
distance undefined-proximal, A/314/CG, X/2/C5
show sticks, byres A/314/CG
show sticks, byres X/2/C5
select binding_site, binding_site + byres A/314/CG
select binding_site, binding_site + byres X/2/C5
distance undefined-proximal, A/314/CG, X/2/O6
show sticks, byres A/314/CG
show sticks, byres X/2/O6
select binding_site, binding_site + byres A/314/CG
select binding_site, binding_site + byres X/2/O6
distance undefined-proximal, A/314/CG, X/2/C6
show sticks, byres A/314/CG
show sticks, byres X/2/C6
select binding_site, binding_site + byres A/314/CG
select binding_site, binding_site + byres X/2/C6
distance undefined-proximal, A/259/CG2, X/2/O6
show sticks, byres A/259/CG2
show sticks, byres X/2/O6
select binding_site, binding_site + byres A/259/CG2
select binding_site, binding_site + byres X/2/O6
distance undefined-proximal, A/259/CG2, X/2/C6
show sticks, byres A/259/CG2
show sticks, byres X/2/C6
select binding_site, binding_site + byres A/259/CG2
select binding_site, binding_site + byres X/2/C6
distance undefined-proximal, X/2/O3, A/280/NE2
show sticks, byres X/2/O3
show sticks, byres A/280/NE2
select binding_site, binding_site + byres X/2/O3
select binding_site, binding_site + byres A/280/NE2
distance undefined-proximal, X/2/O4, A/280/NE2
show sticks, byres X/2/O4
show sticks, byres A/280/NE2
select binding_site, binding_site + byres X/2/O4
select binding_site, binding_site + byres A/280/NE2
distance undefined-proximal, A/314/CZ3, X/2/C2
show sticks, byres A/314/CZ3
show sticks, byres X/2/C2
select binding_site, binding_site + byres A/314/CZ3
select binding_site, binding_site + byres X/2/C2
distance undefined-proximal, A/314/CZ3, X/2/C3
show sticks, byres A/314/CZ3
show sticks, byres X/2/C3
select binding_site, binding_site + byres A/314/CZ3
select binding_site, binding_site + byres X/2/C3
distance undefined-proximal, A/314/CE3, X/2/C3
show sticks, byres A/314/CE3
show sticks, byres X/2/C3
select binding_site, binding_site + byres A/314/CE3
select binding_site, binding_site + byres X/2/C3
distance undefined-proximal, A/361/OH, X/2/O2
show sticks, byres A/361/OH
show sticks, byres X/2/O2
select binding_site, binding_site + byres A/361/OH
select binding_site, binding_site + byres X/2/O2
distance undefined-proximal, A/314/CZ3, X/2/O3
show sticks, byres A/314/CZ3
show sticks, byres X/2/O3
select binding_site, binding_site + byres A/314/CZ3
select binding_site, binding_site + byres X/2/O3
distance undefined-proximal, A/314/CZ3, X/2/C4
show sticks, byres A/314/CZ3
show sticks, byres X/2/C4
select binding_site, binding_site + byres A/314/CZ3
select binding_site, binding_site + byres X/2/C4
distance undefined-proximal, A/314/CE3, X/2/O3
show sticks, byres A/314/CE3
show sticks, byres X/2/O3
select binding_site, binding_site + byres A/314/CE3
select binding_site, binding_site + byres X/2/O3
distance undefined-proximal, A/314/CE3, X/2/C4
show sticks, byres A/314/CE3
show sticks, byres X/2/C4
select binding_site, binding_site + byres A/314/CE3
select binding_site, binding_site + byres X/2/C4
distance undefined-proximal, A/259/OG1, A/278/CD1
show sticks, byres A/259/OG1
show sticks, byres A/278/CD1
select binding_site, binding_site + byres A/259/OG1
select binding_site, binding_site + byres A/278/CD1
distance undefined-proximal, A/259/OG1, A/278/CE1
show sticks, byres A/259/OG1
show sticks, byres A/278/CE1
select binding_site, binding_site + byres A/259/OG1
select binding_site, binding_site + byres A/278/CE1
distance polar-proximal, A/259/OG1, A/278/OH
show sticks, byres A/259/OG1
show sticks, byres A/278/OH
select binding_site, binding_site + byres A/259/OG1
select binding_site, binding_site + byres A/278/OH
distance undefined-vdw, A/278/CZ, A/317/OE1
show sticks, byres A/278/CZ
show sticks, byres A/317/OE1
select binding_site, binding_site + byres A/278/CZ
select binding_site, binding_site + byres A/317/OE1
distance undefined-proximal, A/278/CZ, A/317/CD
show sticks, byres A/278/CZ
show sticks, byres A/317/CD
select binding_site, binding_site + byres A/278/CZ
select binding_site, binding_site + byres A/317/CD
distance undefined-proximal, A/278/CZ, A/317/CG
show sticks, byres A/278/CZ
show sticks, byres A/317/CG
select binding_site, binding_site + byres A/278/CZ
select binding_site, binding_site + byres A/317/CG
distance undefined-proximal, A/259/CB, A/278/OH
show sticks, byres A/259/CB
show sticks, byres A/278/OH
select binding_site, binding_site + byres A/259/CB
select binding_site, binding_site + byres A/278/OH
distance undefined-proximal, A/278/CE2, A/317/OE1
show sticks, byres A/278/CE2
show sticks, byres A/317/OE1
select binding_site, binding_site + byres A/278/CE2
select binding_site, binding_site + byres A/317/OE1
distance undefined-proximal, A/259/C, A/278/OH
show sticks, byres A/259/C
show sticks, byres A/278/OH
select binding_site, binding_site + byres A/259/C
select binding_site, binding_site + byres A/278/OH
distance undefined-proximal, A/259/O, A/278/OH
show sticks, byres A/259/O
show sticks, byres A/278/OH
select binding_site, binding_site + byres A/259/O
select binding_site, binding_site + byres A/278/OH
distance undefined-proximal, A/278/CZ, A/259/OG1
show sticks, byres A/278/CZ
show sticks, byres A/259/OG1
select binding_site, binding_site + byres A/278/CZ
select binding_site, binding_site + byres A/259/OG1
distance undefined-proximal, A/278/CE2, A/259/OG1
show sticks, byres A/278/CE2
show sticks, byres A/259/OG1
select binding_site, binding_site + byres A/278/CE2
select binding_site, binding_site + byres A/259/OG1
distance undefined-proximal, A/259/OG1, A/250/NE1
show sticks, byres A/259/OG1
show sticks, byres A/250/NE1
select binding_site, binding_site + byres A/259/OG1
select binding_site, binding_site + byres A/250/NE1
distance undefined-proximal, A/259/OG1, A/250/CE2
show sticks, byres A/259/OG1
show sticks, byres A/250/CE2
select binding_site, binding_site + byres A/259/OG1
select binding_site, binding_site + byres A/250/CE2
distance undefined-proximal, A/278/CZ, A/259/N
show sticks, byres A/278/CZ
show sticks, byres A/259/N
select binding_site, binding_site + byres A/278/CZ
select binding_site, binding_site + byres A/259/N
distance undefined-proximal, A/259/CB, A/278/CZ
show sticks, byres A/259/CB
show sticks, byres A/278/CZ
select binding_site, binding_site + byres A/259/CB
select binding_site, binding_site + byres A/278/CZ
distance undefined-proximal, A/278/CE2, A/259/N
show sticks, byres A/278/CE2
show sticks, byres A/259/N
select binding_site, binding_site + byres A/278/CE2
select binding_site, binding_site + byres A/259/N
distance undefined-proximal, A/278/CE2, A/259/CB
show sticks, byres A/278/CE2
show sticks, byres A/259/CB
select binding_site, binding_site + byres A/278/CE2
select binding_site, binding_site + byres A/259/CB
distance undefined-proximal, A/259/CA, A/278/CZ
show sticks, byres A/259/CA
show sticks, byres A/278/CZ
select binding_site, binding_site + byres A/259/CA
select binding_site, binding_site + byres A/278/CZ
distance undefined-proximal, A/259/CA, A/278/CE2
show sticks, byres A/259/CA
show sticks, byres A/278/CE2
select binding_site, binding_site + byres A/259/CA
select binding_site, binding_site + byres A/278/CE2
distance undefined-proximal, A/259/C, A/278/CZ
show sticks, byres A/259/C
show sticks, byres A/278/CZ
select binding_site, binding_site + byres A/259/C
select binding_site, binding_site + byres A/278/CZ
distance undefined-proximal, A/259/C, A/278/CE2
show sticks, byres A/259/C
show sticks, byres A/278/CE2
select binding_site, binding_site + byres A/259/C
select binding_site, binding_site + byres A/278/CE2
distance undefined-proximal, A/259/O, A/278/CZ
show sticks, byres A/259/O
show sticks, byres A/278/CZ
select binding_site, binding_site + byres A/259/O
select binding_site, binding_site + byres A/278/CZ
distance undefined-proximal, A/259/O, A/278/CE2
show sticks, byres A/259/O
show sticks, byres A/278/CE2
select binding_site, binding_site + byres A/259/O
select binding_site, binding_site + byres A/278/CE2
distance undefined-proximal, A/317/OE2, A/247/CG
show sticks, byres A/317/OE2
show sticks, byres A/247/CG
select binding_site, binding_site + byres A/317/OE2
select binding_site, binding_site + byres A/247/CG
distance undefined-proximal, A/317/OE2, A/280/CG
show sticks, byres A/317/OE2
show sticks, byres A/280/CG
select binding_site, binding_site + byres A/317/OE2
select binding_site, binding_site + byres A/280/CG
distance undefined-proximal, A/317/OE2, A/280/CB
show sticks, byres A/317/OE2
show sticks, byres A/280/CB
select binding_site, binding_site + byres A/317/OE2
select binding_site, binding_site + byres A/280/CB
distance ionic-vdw, A/317/OE2, A/280/CD2
show sticks, byres A/317/OE2
show sticks, byres A/280/CD2
select binding_site, binding_site + byres A/317/OE2
select binding_site, binding_site + byres A/280/CD2
distance weakhbond-proximal, A/317/OE2, A/280/CA
show sticks, byres A/317/OE2
show sticks, byres A/280/CA
select binding_site, binding_site + byres A/317/OE2
select binding_site, binding_site + byres A/280/CA
distance weakpolar-proximal, A/317/OE2, A/280/CA
show sticks, byres A/317/OE2
show sticks, byres A/280/CA
select binding_site, binding_site + byres A/317/OE2
select binding_site, binding_site + byres A/280/CA
distance undefined-proximal, A/317/OE2, A/278/CE1
show sticks, byres A/317/OE2
show sticks, byres A/278/CE1
select binding_site, binding_site + byres A/317/OE2
select binding_site, binding_site + byres A/278/CE1
distance undefined-proximal, A/317/OE1, A/247/CG
show sticks, byres A/317/OE1
show sticks, byres A/247/CG
select binding_site, binding_site + byres A/317/OE1
select binding_site, binding_site + byres A/247/CG
distance undefined-proximal, A/317/OE1, A/280/CD2
show sticks, byres A/317/OE1
show sticks, byres A/280/CD2
select binding_site, binding_site + byres A/317/OE1
select binding_site, binding_site + byres A/280/CD2
distance undefined-proximal, A/317/OE1, A/280/CA
show sticks, byres A/317/OE1
show sticks, byres A/280/CA
select binding_site, binding_site + byres A/317/OE1
select binding_site, binding_site + byres A/280/CA
distance undefined-proximal, A/317/OE1, A/278/CD1
show sticks, byres A/317/OE1
show sticks, byres A/278/CD1
select binding_site, binding_site + byres A/317/OE1
select binding_site, binding_site + byres A/278/CD1
distance weakpolar-vdwclash, A/317/OE1, A/278/CE1
show sticks, byres A/317/OE1
show sticks, byres A/278/CE1
select binding_site, binding_site + byres A/317/OE1
select binding_site, binding_site + byres A/278/CE1
distance undefined-proximal, A/317/CD, A/280/CD2
show sticks, byres A/317/CD
show sticks, byres A/280/CD2
select binding_site, binding_site + byres A/317/CD
select binding_site, binding_site + byres A/280/CD2
distance undefined-proximal, A/317/CD, A/280/CA
show sticks, byres A/317/CD
show sticks, byres A/280/CA
select binding_site, binding_site + byres A/317/CD
select binding_site, binding_site + byres A/280/CA
distance undefined-proximal, A/317/CD, A/278/CD1
show sticks, byres A/317/CD
show sticks, byres A/278/CD1
select binding_site, binding_site + byres A/317/CD
select binding_site, binding_site + byres A/278/CD1
distance undefined-proximal, A/317/CD, A/278/CE1
show sticks, byres A/317/CD
show sticks, byres A/278/CE1
select binding_site, binding_site + byres A/317/CD
select binding_site, binding_site + byres A/278/CE1
distance undefined-proximal, A/317/CG, A/280/CA
show sticks, byres A/317/CG
show sticks, byres A/280/CA
select binding_site, binding_site + byres A/317/CG
select binding_site, binding_site + byres A/280/CA
distance hydrophobic-proximal, A/317/CG, A/278/CE1
show sticks, byres A/317/CG
show sticks, byres A/278/CE1
select binding_site, binding_site + byres A/317/CG
select binding_site, binding_site + byres A/278/CE1
distance undefined-proximal, A/280/CG, A/247/CG
show sticks, byres A/280/CG
show sticks, byres A/247/CG
select binding_site, binding_site + byres A/280/CG
select binding_site, binding_site + byres A/247/CG
distance hydrophobic-proximal, A/280/CB, A/247/CG
show sticks, byres A/280/CB
show sticks, byres A/247/CG
select binding_site, binding_site + byres A/280/CB
select binding_site, binding_site + byres A/247/CG
distance undefined-proximal, A/280/CD2, A/247/CG
show sticks, byres A/280/CD2
show sticks, byres A/247/CG
select binding_site, binding_site + byres A/280/CD2
select binding_site, binding_site + byres A/247/CG
distance undefined-proximal, A/280/CA, A/247/CG
show sticks, byres A/280/CA
show sticks, byres A/247/CG
select binding_site, binding_site + byres A/280/CA
select binding_site, binding_site + byres A/247/CG
distance hydrophobic-proximal, A/278/CD1, A/247/CG
show sticks, byres A/278/CD1
show sticks, byres A/247/CG
select binding_site, binding_site + byres A/278/CD1
select binding_site, binding_site + byres A/247/CG
distance hydrophobic-proximal, A/278/CE1, A/247/CG
show sticks, byres A/278/CE1
show sticks, byres A/247/CG
select binding_site, binding_site + byres A/278/CE1
select binding_site, binding_site + byres A/247/CG
distance hbond-vdwclash, A/317/OE2, A/281/N
show sticks, byres A/317/OE2
show sticks, byres A/281/N
select binding_site, binding_site + byres A/317/OE2
select binding_site, binding_site + byres A/281/N
distance polar-vdwclash, A/317/OE2, A/281/N
show sticks, byres A/317/OE2
show sticks, byres A/281/N
select binding_site, binding_site + byres A/317/OE2
select binding_site, binding_site + byres A/281/N
distance undefined-proximal, A/317/OE1, A/281/N
show sticks, byres A/317/OE1
show sticks, byres A/281/N
select binding_site, binding_site + byres A/317/OE1
select binding_site, binding_site + byres A/281/N
distance undefined-proximal, A/317/CD, A/281/N
show sticks, byres A/317/CD
show sticks, byres A/281/N
select binding_site, binding_site + byres A/317/CD
select binding_site, binding_site + byres A/281/N
distance undefined-proximal, A/278/OH, A/317/OE2
show sticks, byres A/278/OH
show sticks, byres A/317/OE2
select binding_site, binding_site + byres A/278/OH
select binding_site, binding_site + byres A/317/OE2
distance hbond-vdwclash, A/278/OH, A/317/OE1
show sticks, byres A/278/OH
show sticks, byres A/317/OE1
select binding_site, binding_site + byres A/278/OH
select binding_site, binding_site + byres A/317/OE1
distance polar-vdwclash, A/278/OH, A/317/OE1
show sticks, byres A/278/OH
show sticks, byres A/317/OE1
select binding_site, binding_site + byres A/278/OH
select binding_site, binding_site + byres A/317/OE1
distance undefined-proximal, A/278/OH, A/317/CD
show sticks, byres A/278/OH
show sticks, byres A/317/CD
select binding_site, binding_site + byres A/278/OH
select binding_site, binding_site + byres A/317/CD
distance undefined-proximal, A/317/CG, A/281/N
show sticks, byres A/317/CG
show sticks, byres A/281/N
select binding_site, binding_site + byres A/317/CG
select binding_site, binding_site + byres A/281/N
distance weakhbond-proximal, A/317/CG, A/278/OH
show sticks, byres A/317/CG
show sticks, byres A/278/OH
select binding_site, binding_site + byres A/317/CG
select binding_site, binding_site + byres A/278/OH
distance undefined-proximal, A/317/CB, A/281/N
show sticks, byres A/317/CB
show sticks, byres A/281/N
select binding_site, binding_site + byres A/317/CB
select binding_site, binding_site + byres A/281/N
distance undefined-proximal, A/317/CB, A/278/OH
show sticks, byres A/317/CB
show sticks, byres A/278/OH
select binding_site, binding_site + byres A/317/CB
select binding_site, binding_site + byres A/278/OH
color salmon, polar-proximal
set dash_radius, 0.04, polar-proximal
set dash_gap, 0.45, polar-proximal
set dash_length, 0.04, polar-proximal
color red, hbond-vdwclash
set dash_radius, 0.12, hbond-vdwclash
set dash_gap, 0.25, hbond-vdwclash
set dash_length, 0.08, hbond-vdwclash
color deepsalmon, hbond-vdw
set dash_radius, 0.08, hbond-vdw
set dash_gap, 0.35, hbond-vdw
set dash_length, 0.06, hbond-vdw
color grey60, undefined-vdw
set dash_radius, 0.08, undefined-vdw
set dash_gap, 0.35, undefined-vdw
set dash_length, 0.06, undefined-vdw
color orange, weakpolar-vdwclash
set dash_radius, 0.12, weakpolar-vdwclash
set dash_gap, 0.25, weakpolar-vdwclash
set dash_length, 0.08, weakpolar-vdwclash
color moss_green, hydrophobic-proximal
set dash_radius, 0.04, hydrophobic-proximal
set dash_gap, 0.45, hydrophobic-proximal
set dash_length, 0.04, hydrophobic-proximal
color peach, weakpolar-proximal
set dash_radius, 0.04, weakpolar-proximal
set dash_gap, 0.45, weakpolar-proximal
set dash_length, 0.04, weakpolar-proximal
color red, polar-vdwclash
set dash_radius, 0.12, polar-vdwclash
set dash_gap, 0.25, polar-vdwclash
set dash_length, 0.08, polar-vdwclash
color peach, weakhbond-proximal
set dash_radius, 0.04, weakhbond-proximal
set dash_gap, 0.45, weakhbond-proximal
set dash_length, 0.04, weakhbond-proximal
color deepsalmon, polar-vdw
set dash_radius, 0.08, polar-vdw
set dash_gap, 0.35, polar-vdw
set dash_length, 0.06, polar-vdw
color maize, ionic-vdw
set dash_radius, 0.08, ionic-vdw
set dash_gap, 0.35, ionic-vdw
set dash_length, 0.06, ionic-vdw
color grey30, undefined-proximal
set dash_radius, 0.04, undefined-proximal
set dash_gap, 0.45, undefined-proximal
set dash_length, 0.04, undefined-proximal
color electric_blue, aromatic-vdw
set dash_radius, 0.08, aromatic-vdw
set dash_gap, 0.35, aromatic-vdw
set dash_length, 0.06, aromatic-vdw
color grey90, undefined-vdwclash
set dash_radius, 0.12, undefined-vdwclash
set dash_gap, 0.25, undefined-vdwclash
set dash_length, 0.08, undefined-vdwclash
color baby_blue, aromatic-proximal
set dash_radius, 0.04, aromatic-proximal
set dash_gap, 0.45, aromatic-proximal
set dash_length, 0.04, aromatic-proximal
pseudoatom ring_centers, pos=[13.6554, 7.0444, 51.0982]
pseudoatom ring_centers, pos=[9.064599999999999, 4.5484, 51.076]
pseudoatom ring_centers, pos=[18.3584, 13.3278, 57.85560000000001]
pseudoatom ring_centers, pos=[2.6460000000000004, 8.593200000000001, 58.13060000000001]
pseudoatom ring_centers, pos=[14.0738, 7.2344, 57.8232]
pseudoatom ring_centers, pos=[9.449600000000002, 3.7227999999999994, 60.581800000000015]
pseudoatom ring_centers, pos=[12.565999999999999, 7.811166666666667, 52.798833333333334]
pseudoatom ring_centers, pos=[7.275, 16.941499999999998, 52.11783333333333]
pseudoatom ring_centers, pos=[3.7736666666666663, 10.273, 57.3585]
pseudoatom ring_centers, pos=[15.353166666666663, 8.314499999999999, 59.17933333333333]
pseudoatom ring_centers, pos=[4.942833333333333, 1.9253333333333331, 57.75516666666667]
pseudoatom ring_centers, pos=[9.176666666666666, 3.9979999999999998, 62.710499999999996]
hide everything, ring_centers
pseudoatom pt1, pos=[13.6554, 7.0444, 51.0982]
pseudoatom pt2, pos=[9.064599999999999, 4.5484, 51.076]
distance ring-OT, pt1, pt2
delete pt1
delete pt2
pseudoatom pt1, pos=[9.064599999999999, 4.5484, 51.076]
pseudoatom pt2, pos=[13.6554, 7.0444, 51.0982]
distance ring-ET, pt1, pt2
delete pt1
delete pt2
pseudoatom pt1, pos=[9.064599999999999, 4.5484, 51.076]
pseudoatom pt2, pos=[12.565999999999999, 7.811166666666667, 52.798833333333334]
distance ring-OT, pt1, pt2
delete pt1
delete pt2
pseudoatom pt1, pos=[18.3584, 13.3278, 57.85560000000001]
pseudoatom pt2, pos=[15.353166666666663, 8.314499999999999, 59.17933333333333]
distance ring-ET, pt1, pt2
delete pt1
delete pt2
pseudoatom pt1, pos=[14.0738, 7.2344, 57.8232]
pseudoatom pt2, pos=[12.565999999999999, 7.811166666666667, 52.798833333333334]
distance ring-ET, pt1, pt2
delete pt1
delete pt2
pseudoatom pt1, pos=[9.449600000000002, 3.7227999999999994, 60.581800000000015]
pseudoatom pt2, pos=[4.942833333333333, 1.9253333333333331, 57.75516666666667]
distance ring-FT, pt1, pt2
delete pt1
delete pt2
pseudoatom pt1, pos=[12.565999999999999, 7.811166666666667, 52.798833333333334]
pseudoatom pt2, pos=[9.064599999999999, 4.5484, 51.076]
distance ring-OT, pt1, pt2
delete pt1
delete pt2
pseudoatom pt1, pos=[12.565999999999999, 7.811166666666667, 52.798833333333334]
pseudoatom pt2, pos=[14.0738, 7.2344, 57.8232]
distance ring-OT, pt1, pt2
delete pt1
delete pt2
pseudoatom pt1, pos=[15.353166666666663, 8.314499999999999, 59.17933333333333]
pseudoatom pt2, pos=[18.3584, 13.3278, 57.85560000000001]
distance ring-ET, pt1, pt2
delete pt1
delete pt2
pseudoatom pt1, pos=[4.942833333333333, 1.9253333333333331, 57.75516666666667]
pseudoatom pt2, pos=[9.449600000000002, 3.7227999999999994, 60.581800000000015]
distance ring-OT, pt1, pt2
delete pt1
delete pt2
set dash_radius, 0.25, ring-*
set dash_gap, 0.1, ring-*
color white, ring-*
pseudoatom pt1, pos=[9.064599999999999, 4.5484, 51.076]
distance CARBONPI, pt1, A/247/CG
delete pt1
pseudoatom pt1, pos=[2.6460000000000004, 8.593200000000001, 58.13060000000001]
distance CARBONPI, pt1, X/2/C6
delete pt1
pseudoatom pt1, pos=[2.6460000000000004, 8.593200000000001, 58.13060000000001]
distance DONORPI, pt1, X/2/O6
delete pt1
pseudoatom pt1, pos=[2.6460000000000004, 8.593200000000001, 58.13060000000001]
distance CARBONPI, pt1, X/2/C5
delete pt1
pseudoatom pt1, pos=[14.0738, 7.2344, 57.8232]
distance CARBONPI, pt1, X/1/C5
delete pt1
pseudoatom pt1, pos=[14.0738, 7.2344, 57.8232]
distance CARBONPI, pt1, X/1/C3
delete pt1
pseudoatom pt1, pos=[14.0738, 7.2344, 57.8232]
distance CARBONPI, pt1, X/1/C2
delete pt1
pseudoatom pt1, pos=[14.0738, 7.2344, 57.8232]
distance CARBONPI, pt1, X/1/C1
delete pt1
pseudoatom pt1, pos=[7.275, 16.941499999999998, 52.11783333333333]
distance CARBONPI, pt1, A/359/CB
delete pt1
pseudoatom pt1, pos=[3.7736666666666663, 10.273, 57.3585]
distance CARBONPI, pt1, X/2/C4
delete pt1
pseudoatom pt1, pos=[3.7736666666666663, 10.273, 57.3585]
distance CARBONPI, pt1, X/2/C6
delete pt1
pseudoatom pt1, pos=[3.7736666666666663, 10.273, 57.3585]
distance CARBONPI, pt1, X/2/C5
delete pt1
pseudoatom pt1, pos=[15.353166666666663, 8.314499999999999, 59.17933333333333]
distance CARBONPI, pt1, X/1/C2
delete pt1
pseudoatom pt1, pos=[15.353166666666663, 8.314499999999999, 59.17933333333333]
distance CARBONPI, pt1, X/1/C1
delete pt1
pseudoatom pt1, pos=[15.353166666666663, 8.314499999999999, 59.17933333333333]
distance DONORPI, pt1, X/1/O1
delete pt1
pseudoatom pt1, pos=[9.176666666666666, 3.9979999999999998, 62.710499999999996]
distance CARBONPI, pt1, X/1/C8
delete pt1
set dash_radius, 0.15, *PI
set dash_gap, 0.1, *PI
color white, CARBONPI
color blue, DONORPI
color green, HALOGENPI
color red, CATIONPI
color yellow, METSULPHURPI
pseudoatom pt1, pos=[19.9595, 9.2975, 56.904]
pseudoatom pt2, pos=[15.353166666666663, 8.314499999999999, 59.17933333333333]
distance amide-ring, pt1, pt2
delete pt1
delete pt2
set dash_radius, 0.25, amide-ring
set dash_gap, 0.2, amide-ring
set dash_length, 0.5, amide-ring
color white, amide-ring
set dash_radius, 0.25, amide-amide
set dash_gap, 0.2, amide-amide
set dash_length, 0.5, amide-amide
color blue, amide-amide
hide labels
util.cbaw
bg_color white
show cartoon
set cartoon_side_chain_helper, 1
hide lines
hide everything, het
show sticks, het
show spheres, het
disable undefined-proximal
set defer_update, 0
save /home/tbnguyen/Desktop/csm_carbohydrate/mmCSM-NA-change/csm_carbohydrate_app/predictions/data/162684640047/wt.pse
quit
