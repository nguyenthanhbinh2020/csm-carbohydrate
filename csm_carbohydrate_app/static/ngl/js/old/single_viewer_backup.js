var structure3DApp = {
    viewer: null,
    component: null,
    viewerEl: null,
    pdbUri: null,
    chains: null,
    mut_res: null,
    contacts: null,
    PIint:null,
    PIint_residues:null,

    initEvents() {
        let self = this;
        let v = self.viewer;
    },

    getResidue: function (arpeggio_interactions, interaction) {
        var result = [];
        for (var each in arpeggio_interactions) {
            if (arpeggio_interactions[each].length > 0) {
                if (each == interaction) {
                    var interaction = arpeggio_interactions[interaction];
                    interaction.forEach(element => {
                        result.push(element)
                    });
                }
            }
        }
        return result;
    },
// Highlight mutation site
    getMutationSiteHighlight: function(mutation){
        let self = this;
        self.component.addRepresentation("ball+stick",{
            name: 'mutation_site',
            sele: mutation,
            color:'#00FF00'
            });
        },
// Might need to get original Arpeggio contacts
    getResidues: function (Arpeggio_contacts) {
        let self = this;
        var interaction_list = ['vdw', 'clash', 'hbond', 'ionic', 'aromatic', 'hydrophobic', 'carbonyl', 'polar'];
        var all_residues = [];

        interaction_list.forEach(element => {
            all_residues.push(self.getResidue(Arpeggio_contacts, element))

        });
        var merged = [].concat.apply([], all_residues);
        return new Set(merged);
    },

// For labeling (from Arpeggio_contacts)
    getResiduesForLabel: function (Arpeggio_contacts) {
        let self = this;
        var result = [];
        self.getResidues(Arpeggio_contacts).forEach(element => {
            element.forEach(elementt => {
                result.push(elementt.split('.')[0] + ".CA")
            })
        });
        return new Set(result);
    },

// For showing residues those are in interaction
    getResiduesForDisplay: function () {
        let self = this;
        var result = [];

        self.getResidues(self.contacts).forEach(element => {
            element.forEach(elementt => {
                result.push(elementt.split('.')[0])
            })
        });
        return new Set(result);
    },

// Default Representation of NGLViewer
    setDefault: function () {
        let self = this;
        self.viewer.setParameters({backgroundColor: 'white'});
        self.component.addRepresentation("cartoon",{
            name: 'proteins',
            sele: 'protein',
            wireframe: false,
            quality: 'high'
        });
        self.component.addRepresentation("ball+stick",{
            name: 'heteros',
            sele: 'hetero',
            wireframe: false,
            quality: 'high'
        });
        self.component.addRepresentation("ribbon",{
            name: 'dna_backbone',
            sele: 'nucleic',
            wireframe: false,
            quality: 'high'
        });
        self.component.addRepresentation("base",{
            name: 'dna_base',
            sele: 'nucleic',
            color: 'resname',
            quality: 'high'
        });
        self.showInteraction();
        self.showPIinteraction();
//       self.showTargetPosition(mut);
        self.getMutationSiteHighlight(self.mut_res);
//        self.viewer.toggleSpin();
//        self.viewer.setQuality('high');
        self.component.autoView(self.mut_res);
        // self.component.addRepresentation("contact",{PiStacking:true}); This is for native interaction views.

    },

    setReset: function () {
        let self = this;
        self.component.removeAllRepresentations();
        self.setDefault();
    },

    residueLabel: function () {
        let self = this;

        if (Array.from(self.getResiduesForLabel(self.contacts)).length > 0){

            if (self.viewer.parameters.backgroundColor == "black") {

                // residue labeling
                self.component.addRepresentation("label", {
                    sele: Array.from(self.getResiduesForLabel(self.contacts)).join(' '),
                    name: 'active_label',
                    color: "yellow",
                    labelType: "format",
                    labelFormat: "%(resname)s%(resno)s"
                })
            }
            if (self.viewer.parameters.backgroundColor == "white") {

                // residue labeling
                self.component.addRepresentation("label", {
                    sele: Array.from(self.getResiduesForLabel(self.contacts)).join(' '),
                    name: 'active_label',
                    color: "black",
                    labelType: "format",
                    labelFormat: "%(resname)s%(resno)s"
                })
            }
        }
    },

    PIresidueLabel: function () {
        let self = this;
        // add residue label
        var result = [];

        self.PIint_residues.forEach(element => {

            result.push(element + ".CA")

        });

        if (self.viewer.parameters.backgroundColor == "black") {
            // residue labeling
            self.component.addRepresentation("label", {
                sele: Array.from(result).join(' '),
                name: 'active_label',
                color: "yellow",
                labelType: "format",
                labelFormat: "%(resname)s%(resno)s"
            })
        }

        if (self.viewer.parameters.backgroundColor == "white") {
            // residue labeling
            self.component.addRepresentation("label", {
                sele: Array.from(result).join(' '),
                name: 'active_label',
                color: "black",
                labelType: "format",
                labelFormat: "%(resname)s%(resno)s"
            })
        }
    },

    showTargetPosition: function () {
        let self = this;

        self.component.addRepresentation("ball+stick", {
            name: 'target_residue',
            multipleBond: "symmetric",
            sele: self.mut_res
        });

        if (self.viewer.parameters.backgroundColor == "black") {

            // residue labeling
            self.component.addRepresentation("label", {
                sele: self.mut_res + ".CA",
                name: 'active_label',
                color: "yellow",
                labelType: "format",
                labelFormat: "%(resname)s%(resno)s"
            })
        }
        if (self.viewer.parameters.backgroundColor == "white") {

            // residue labeling
            self.component.addRepresentation("label", {
                sele: self.mut_res + ".CA",
                name: 'active_label',
                color: "black",
                labelType: "format",
                labelFormat: "%(resname)s%(resno)s"
            })
        }

    },

    showPIinteraction: function() {
        let self = this;

        if (self.PIint_residues.length > 0) {

            // add ball+stick representation
            self.component.addRepresentation("ball+stick", {
                name: 'active_sites_PI',
                multipleBond: "symmetric",
                sele: self.PIint_residues.join(" ")
            });

            self.PIresidueLabel();

            var shape = new NGL.Shape("shape", {dashedCylinder: true, radialSegments: 60});

            if (self.PIint['carbonpi'].length > 0) {
                var carbonpi = self.getResidue(self.PIint, 'carbonpi');
                for (var i = 0; i < carbonpi.length; i++) {
                    shape.addCylinder(self.component.getCenter(carbonpi[i][0]), [parseFloat(carbonpi[i][1].split(",")[0]), parseFloat(carbonpi[i][1].split(",")[1]), parseFloat(carbonpi[i][1].split(",")[2])], [235, 0, 1], 0.08, "carbonpi_" + i);
                }
                ;
            }

            if (self.PIint['cationpi'].length > 0) {
                var cationpi = self.getResidue(self.PIint, 'cationpi');
                for (var i = 0; i < cationpi.length; i++) {
                    shape.addCylinder(self.component.getCenter(cationpi[i][0]), [parseFloat(cationpi[i][1].split(",")[0]), parseFloat(cationpi[i][1].split(",")[1]), parseFloat(cationpi[i][1].split(",")[2])], [235, 0, 1], 0.08, "cationpi_" + i);
                }
                ;
            }

            if (self.PIint['donorpi'].length > 0) {
                var donorpi = self.getResidue(self.PIint, 'donorpi');
                for (var i = 0; i < donorpi.length; i++) {
                    shape.addCylinder(self.component.getCenter(donorpi[i][0]), [parseFloat(donorpi[i][1].split(",")[0]), parseFloat(donorpi[i][1].split(",")[1]), parseFloat(donorpi[i][1].split(",")[2])], [235, 0, 1], 0.08, "donorpi_" + i);
                }
                ;
            }

            if (self.PIint['halogenpi'].length > 0) {
                var halogenpi = self.getResidue(self.PIint, 'halogenpi');
                for (var i = 0; i < halogenpi.length; i++) {
                    shape.addCylinder(self.component.getCenter(halogenpi[i][0]), [parseFloat(halogenpi[i][1].split(",")[0]), parseFloat(halogenpi[i][1].split(",")[1]), parseFloat(halogenpi[i][1].split(",")[2])], [235, 0, 1], 0.08, "halogenpi_" + i);
                }
                ;
            }


            if (self.PIint['metsulphurpi'].length > 0) {
                var metsulphurpi = self.getResidue(self.PIint, 'metsulphurpi');
                for (var i = 0; i < metsulphurpi.length; i++) {
                    shape.addCylinder(self.component.getCenter(metsulphurpi[i][0]), [parseFloat(metsulphurpi[i][1].split(",")[0]), parseFloat(metsulphurpi[i][1].split(",")[1]), parseFloat(metsulphurpi[i][1].split(",")[2])], [235, 0, 1], 0.08, "metsulphurpi_" + i);
                }
                ;
            }

            if (self.PIint['pipi'].length > 0) {
                var pipi = self.getResidue(self.PIint, 'pipi');
                for (var i = 0; i < pipi.length; i++) {
                    shape.addCylinder([parseFloat(pipi[i][0].split(",")[0]), parseFloat(pipi[i][0].split(",")[1]), parseFloat(pipi[i][0].split(",")[2])], [parseFloat(pipi[i][1].split(",")[0]), parseFloat(pipi[i][1].split(",")[1]), parseFloat(pipi[i][1].split(",")[2])], [235, 0, 1], 0.08, "pipi_" + i);
                }
                ;
            }

            if (self.PIint['amideamide'].length > 0) {
                var amideamide = self.getResidue(self.PIint, 'amideamide');
                for (var i = 0; i < amideamide.length; i++) {
                    shape.addCylinder([parseFloat(amideamide[i][0].split(",")[0]), parseFloat(amideamide[i][0].split(",")[1]), parseFloat(amideamide[i][0].split(",")[2])], [parseFloat(amideamide[i][1].split(",")[0]), parseFloat(amideamide[i][1].split(",")[1]), parseFloat(amideamide[i][1].split(",")[2])], [235, 0, 1], 0.08, "amideamide_" + i);
                }
                ;
            }

            if (self.PIint['amidering'].length > 0) {
                var amidering = self.getResidue(self.PIint, 'amidering');
                for (var i = 0; i < amidering.length; i++) {
                    shape.addCylinder([parseFloat(amidering[i][0].split(",")[0]), parseFloat(amidering[i][0].split(",")[1]), parseFloat(amidering[i][0].split(",")[2])], [parseFloat(amidering[i][1].split(",")[0]), parseFloat(amidering[i][1].split(",")[1]), parseFloat(amidering[i][1].split(",")[2])], [235, 0, 1], 0.08, "amidering_" + i);
                }
                ;
            }

            var shapeComp = self.viewer.addComponentFromObject(shape);
            shapeComp.addRepresentation('buffer');
        }

    },


    showInteraction: function () {
        let self = this;

        if(self.getResiduesForDisplay().size > 0) {

            self.component.addRepresentation("ball+stick", {
                name: 'active_sites',
                multipleBond: "symmetric",
                sele: Array.from(self.getResiduesForDisplay()).join(' ')
            });

            // residue labeling
            self.residueLabel();

            // interaction viewing
            // if (self.getResidue(self.contacts, 'clash').length > 0) {
            //     self.component.addRepresentation('distance', {
            //         name: 'clash',
            //         atomPair: self.getResidue(self.contacts, 'clash'),
            //         color: '#FF00FF',
            //         labelVisible: false,
            //     })
            // }
            if (self.getResidue(self.contacts, 'vdw').length > 0) {
                self.component.addRepresentation('distance', {
                    name: 'vdw',
                    atomPair: self.getResidue(self.contacts, 'vdw'),
                    color: '#61DBDD',
                    labelVisible: false,
                })
            }

            if (self.getResidue(self.contacts, 'hbond').length > 0) {
                self.component.addRepresentation('distance', {
                    name: 'hbond',
                    atomPair: self.getResidue(self.contacts, 'hbond'),
                    color: '#FF0000',
                    labelVisible: false,
                })
            }
            if (self.getResidue(self.contacts, 'ionic').length > 0) {
                self.component.addRepresentation('distance', {
                    name: 'ionic',
                    atomPair: self.getResidue(self.contacts, 'ionic'),
                    color: '#FAFA30',
                    labelVisible: false,
                })
            }
            if (self.getResidue(self.contacts, 'aromatic').length > 0) {
                self.component.addRepresentation('distance', {
                    name: 'aromatic',
                    atomPair: self.getResidue(self.contacts, 'aromatic'),
                    color: '#90EE90',
                    labelVisible: false,
                })
            }
            if (self.getResidue(self.contacts, 'hydrophobic').length > 0) {
                self.component.addRepresentation('distance', {
                    name: 'hydrophobic',
                    atomPair: self.getResidue(self.contacts, 'hydrophobic'),
                    color: '#137B13',
                    labelVisible: false,

                })
            }
            if (self.getResidue(self.contacts, 'carbonyl').length > 0) {
                self.component.addRepresentation('distance', {
                    name: 'carbonyl',
                    atomPair: self.getResidue(self.contacts, 'carbonyl'),
                    color: '#334CFF',
                    labelVisible: false,
                })
            }
            if (self.getResidue(self.contacts, 'polar').length > 0) {
                self.component.addRepresentation('distance', {
                    name: 'polar',
                    atomPair: self.getResidue(self.contacts, 'polar'),
                    color: '#F58805',
                    labelVisible: false,
                })
            }
        }

    },

    init: function (el, pdbUri, chains, mut_res, main_residues, contacts, PIint, PIint_residues) {
        let self = this;

        self.viewerEl = el;

        self.pdbUri = pdbUri;
        self.chains = chains;
        self.mut_res = mut_res;
        self.contacts = contacts;
        self.PIint = PIint;
        self.PIint_residues = PIint_residues;

        self.viewer = new NGL.Stage(self.viewerEl);
        self.viewer.loadFile(pdbUri).then(function (component) {
            self.component = component;
        }).then(function () {
            self.initEvents();
            self.setDefault();
        });
        var tooltip = document.createElement('div');
        Object.assign(tooltip.style, {
            display: 'none',
            position: 'fixed',
            zIndex: 10,
            pointerEvents: 'none',
            backgroundColor: 'rgba( 0, 0, 0, 0.6 )',
            color: 'lightgrey',
            padding: '8px',
            fontFamily: 'sans-serif'
        });
        document.body.appendChild(tooltip);
        self.viewer.mouseControls.remove('hoverPick');
        self.viewer.mouseControls.remove("drag-middle");
        self.viewer.signals.hovered.add(function (pickingProxy) {
            if (pickingProxy && (pickingProxy.atom || pickingProxy.bond)) {
                var atom = pickingProxy.atom || pickingProxy.closestBondAtom;
                var mp = pickingProxy.mouse.position;
                tooltip.innerText = 'ATOM: ' + atom.qualifiedName();
                tooltip.style.bottom = window.innerHeight - mp.y + 3 + 'px';
                tooltip.style.left = mp.x + 3 + 'px';
                tooltip.style.display = 'block';
            } else {
                tooltip.style.display = 'none';
            }
        })

    }
};


$("#PIints").on("click", function (e) {

    if (PIints.checked == true){
        structure3DApp.viewer.getRepresentationsByName('buffer').setVisibility(true);
    }
    else {
        structure3DApp.viewer.getRepresentationsByName('buffer').setVisibility(false);
    }

    // if (PIints.checked == true){
    //     structure3DApp.viewer.getRepresentationsByName('clash').setVisibility(true);
    // }
    // else {
    //     structure3DApp.viewer.getRepresentationsByName('clash').setVisibility(false);
    // }
});
$("#vdw").on("click", function (e) {

    if (vdw.checked == true){
        structure3DApp.viewer.getRepresentationsByName('vdw').setVisibility(true);
    }
    else {
        structure3DApp.viewer.getRepresentationsByName('vdw').setVisibility(false);
    }
});

$("#hbond").on("click", function (e) {

    if (hbond.checked == true){
        structure3DApp.viewer.getRepresentationsByName('hbond').setVisibility(true);
    }
    else {
        structure3DApp.viewer.getRepresentationsByName('hbond').setVisibility(false);
    }
});
$("#ionic").on("click", function (e) {

    if (ionic.checked == true){
        structure3DApp.viewer.getRepresentationsByName('ionic').setVisibility(true);
    }
    else {
        structure3DApp.viewer.getRepresentationsByName('ionic').setVisibility(false);
    }
});
$("#aromatic").on("click", function (e) {

    if (aromatic.checked == true){
        structure3DApp.viewer.getRepresentationsByName('aromatic').setVisibility(true);
    }
    else {
        structure3DApp.viewer.getRepresentationsByName('aromatic').setVisibility(false);
    }
});
$("#hydrophobic").on("click", function (e) {

    if (hydrophobic.checked == true){
        structure3DApp.viewer.getRepresentationsByName('hydrophobic').setVisibility(true);
    }
    else {
        structure3DApp.viewer.getRepresentationsByName('hydrophobic').setVisibility(false);
    }
});
$("#carbonyl").on("click", function (e) {

    if (carbonyl.checked == true){
        structure3DApp.viewer.getRepresentationsByName('carbonyl').setVisibility(true);
    }
    else {
        structure3DApp.viewer.getRepresentationsByName('carbonyl').setVisibility(false);
    }
});
$("#polar").on("click", function (e) {

    if (polar.checked == true){
        structure3DApp.viewer.getRepresentationsByName('polar').setVisibility(true);
    }
    else {
        structure3DApp.viewer.getRepresentationsByName('polar').setVisibility(false);
    }
});


$("button#setspin").on("click", function (e) {
    structure3DApp.viewer.toggleSpin();
});

$("button#screenshot").on("click", function (e) {
    structure3DApp.viewer.makeImage({
        factor: 1,
        antialias: true,
        trim: false,
        transparent: true
    }).then(function (blob) {
        NGL.download(blob, "screenshot.png");
    });
});

$("button#fullscreen").on("click", function (e) {
    structure3DApp.viewer.toggleFullscreen();
});

$("button#resetView").on("click", function (e) {
    structure3DApp.component.removeAllRepresentations();
    structure3DApp.setReset();
    $('select').prop('selectedIndex', 0);
});
$("button#showCDRs").on("click", function (e) {
    if (light_CDR.length == 0 && heavy_CDR.length == 0){
        alert("NO CDRs detected")
    }
    if (light_CDR.length != 0){
        structure3DApp.getCDRHighlight(light_CDR,"cyan");
    }
    if (heavy_CDR.length != 0){
        structure3DApp.getCDRHighlight(heavy_CDR,"magenta");
    };
});

$("button#gotomut").on("click", function (e) {
    structure3DApp.component.autoView(mut);
});

$("select#backgroundColor").change(function () {
    var current_bg = document.getElementById('backgroundColor').value;

    structure3DApp.viewer.setParameters({backgroundColor: current_bg});
    if (structure3DApp.viewer.getRepresentationsByName('active_label')['list'].length > 0){
        structure3DApp.residueLabel();
        structure3DApp.PIresidueLabel();
    }
});

$("button#showint").on("click", function (e) {
    structure3DApp.showInteraction();
    structure3DApp.showPIinteraction();
});

$("select#select-style_ab").change(function (e) {
    structure3DApp.viewer.getRepresentationsByName('antibody').dispose();
    ab_chains.forEach(element => {
        structure3DApp.component.addRepresentation(document.getElementById('select-style_ab').value, {
            name: 'antibody',
            sele: ":" + element.trim(),
            quality: "high",
            colorScheme: document.getElementById('select-colorScheme_ab').value
        });
    });
});


$("select#select-colorScheme_ab").change(function (e) {
    structure3DApp.viewer.getRepresentationsByName('antibody').dispose();
    ab_chains.forEach(element => {
        structure3DApp.component.addRepresentation(document.getElementById('select-style_ab').value, {
            name: 'antibody',
            sele: ":" + element.trim(),
            quality: "high",
            colorScheme: document.getElementById('select-colorScheme_ab').value
        });
    });
});

$("select#select-style_ag").change(function (e) {
    structure3DApp.viewer.getRepresentationsByName('antigen').dispose();
    ag_chains.forEach(element => {
        structure3DApp.component.addRepresentation(document.getElementById('select-style_ag').value, {
            name: 'antigen',
            sele: ":" + element.trim(),
            quality: "high",
            colorScheme: document.getElementById('select-colorScheme_ag').value
        });
    });
});


$("select#select-colorScheme_ag").change(function (e) {
    structure3DApp.viewer.getRepresentationsByName('antigen').dispose();
    ag_chains.forEach(element => {
        structure3DApp.component.addRepresentation(document.getElementById('select-style_ag').value, {
            name: 'antigen',
            sele: ":" + element.trim(),
            quality: "high",
            colorScheme: document.getElementById('select-colorScheme_ag').value
        });
    });
});

$("#switch_3D_protein").on("change", function (e) {
    if ($(this).prop('checked')){
        structure3DApp.viewer.getRepresentationsByName('proteins').setVisibility(true);
        }
        else
        {
        structure3DApp.viewer.getRepresentationsByName('proteins').setVisibility(false);
            }

    });

$("#switch_3D_dna").on("change", function (e) {
    if ($(this).prop('checked')){
        structure3DApp.viewer.getRepresentationsByName('dna_backbone').setVisibility(true);
        structure3DApp.viewer.getRepresentationsByName('dna_base').setVisibility(true);
        }
        else
        {
        structure3DApp.viewer.getRepresentationsByName('dna_backbone').setVisibility(false);
        structure3DApp.viewer.getRepresentationsByName('dna_base').setVisibility(false);
        }
    });

$("#switch_3D_hetero").on("change", function (e) {
    if ($(this).prop('checked')){
        structure3DApp.viewer.getRepresentationsByName('heteros').setVisibility(true);
        }
        else
        {
         structure3DApp.viewer.getRepresentationsByName('heteros').setVisibility(false);
         }
    });
