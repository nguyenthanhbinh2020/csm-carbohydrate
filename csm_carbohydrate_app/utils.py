# ********************************************************
# *   ----------------------------------------------     *
# * Douglas Eduardo Valente Pires - dpires@dcc.ufmg.br   *
# * www.dcc.ufmg.br/~dpires                              *
# * Last modification :: 15/04/2014                      *
# *   ----------------------------------------------     *
# ********************************************************

# ---------------------------------------------------------------------------------------------------------------------
import sys
import re
import os
import io
from StringIO import StringIO
import time
import csv
import subprocess
import pandas as pd
from Bio.PDB.PDBParser import PDBParser
from Bio.SeqUtils import seq1
from Bio.PDB import *

from subprocess import Popen, PIPE, check_output
from csm_carbohydrate_app import app
from flask import render_template, abort, request, flash, Response, Flask, redirect, url_for
from flask_mail import Message, Mail
from redis import Redis
from rq import Queue, Connection
from csm_carbohydrate_app import app, queue_single#, queue_design
from itertools import permutations

start_time = time.time()

def mergeArpeggioResultFiles(root_dir,mutation):
    # LOCATION
    mutation_dirname = mutation.replace(';','_')
    # working_dir = os.path.join(app.config['UPLOAD'],jobid,mutation_dirname) # for webserver
    working_dir = os.path.join(root_dir,mutation_dirname) # for HPC207
    # DIRECTORY BREAKDOWN
    mutation_list = mutation.split(';')
    # GET MERGED CONTACTS FILES
    os.system("find {} -name '*.forward.{}' | xargs cat > {}".format(working_dir,'contacts',os.path.join(working_dir,'wt_cleaned.forward.contacts')))
    os.system("find {} -name '*.forward.{}' | xargs cat > {}".format(working_dir,'amri',os.path.join(working_dir,'wt_cleaned.forward.amri')))
    os.system("find {} -name '*.forward.{}' | xargs cat > {}".format(working_dir,'ari',os.path.join(working_dir,'wt_cleaned.forward.ari')))
    os.system("find {} -name '*.forward.{}' | xargs cat > {}".format(working_dir,'amam',os.path.join(working_dir,'wt_cleaned.forward.amam')))
    os.system("find {} -name '*.forward.{}' | xargs cat > {}".format(working_dir,'rings',os.path.join(working_dir,'wt_cleaned.forward.rings')))
    os.system("find {} -name '*.forward.{}' | xargs cat > {}".format(working_dir,'ri',os.path.join(working_dir,'wt_cleaned.forward.ri')))
    return True

# ---------------------------------------------------------------------------------------------------------------------
# GENERAL FUNCTIONS
# ---------------------------------------------------------------------------------------------------------------------
# Check number of chains in provided PDB file
def check_num_chains(pdb_file):
    num_chains = 999
    cmd = "grep ^ATOM " + pdb_file + " | cut -c22 | grep \"^\w\" | sort -u | wc -l"
    num_chains = subprocess.check_output(cmd, shell=True)
    return int(num_chains)

def permutate(semicolon_separated_mutations):
    mutation_list = semicolon_separated_mutations.split(';')
    num_permutation = len(mutation_list)
    permutation_list = list()
    for each in permutations(mutation_list, num_permutation):
        permutation_list.append(each)
    return permutation_list



# ---------------------------------------------------------------------------------------------------------------------
## Check if PDB has multiple models and filters only first model
def check_models(pdb_file):
    num_models = 999
    cmd = "grep -e\"^MODEL\" " + pdb_file + " | wc -l"
    num_models = subprocess.check_output(cmd, shell=True)

    if int(num_models) > 1:
        # filter first model
        cmd = "perl " + app.config[
            'CODE_FOLDER'] + "/filter_first_model.pl " + pdb_file + " " + pdb_file + ".model1.pdb ; mv " + pdb_file + ".model1.pdb " + pdb_file
        os.system(cmd)
        return pdb_file

    return pdb_file


# ---------------------------------------------------------------------------------------------------------------------
# Check if PDB has multiple occupancies and filters only the occupancy "A" or " "
def check_multiple_occupancies(pdb_file):
    cmd = "grep -e \"^................A\" -e\"^................ \"  " + pdb_file + " > " + pdb_file + ".occup.pdb ; mv " + pdb_file + ".occup.pdb " + pdb_file
    os.system(cmd)
    return pdb_file


# ---------------------------------------------------------------------------------------------------------------------
# Check if PDB has multiple chains
def check_chains(pdb_file, pdb_sdm, chain):
    cmd = "perl " + app.config['CODE_FOLDER'] + "/filter_chain.pl " + pdb_file + " " + pdb_sdm + " " + chain
    os.system(cmd)
    return


# ---------------------------------------------------------------------------------------------------------------------
# ---------------------------------------------------------------------------------------------------------------------
# Check if string follows float format
def isFloat(string):
    try:
        float(string)
        return True
    except ValueError:
        return False


# ---------------------------------------------------------------------------------------------------------------------
# Check if provided file follows PDB format
def check_format(pdb_file):
    with open(pdb_file) as my_pdb_file:
        data = my_pdb_file.read()
        data = data.rstrip().split('\n')

        num_atom_lines = 0
        for line in (data):
            match = re.match('^ATOM', line)
            if match:
                num_atom_lines += 1
                x = line[30:37].strip()
                y = line[38:45].strip()
                z = line[46:53].strip()

                # try:
                # float(x)
                # float(y)
                # float(z)
                # except ValueError:
                # return 0

                if (not (isFloat(x)) or not (isFloat(y)) or not (isFloat(z))):
                    return 0

        if num_atom_lines == 0:
            return 0

    return 1


# ---------------------------------------------------------------------------------------------------------------------
# Check if mutation is consistent with provided PDB file
def check_mutation(pdb_file, wild_type, position, chain):
    # print("Checking mutation: " + wild_type + " " + position)
    wild_res = convert_res_code(wild_type)
    cmd = "grep ^ATOM " + pdb_file + " | grep " + wild_res + " | grep \"^.\{21\}" + chain + "\" | cut -c23-27 | sed 's/[ ]*//g' | awk {'if($1 == \"" + position + "\"){print}'} | wc -l"
    residue_found = subprocess.check_output(cmd, shell=True)
    # print("Found = " + residue_found)
    return residue_found


# ---------------------------------------------------------------------------------------------------------------------
# Check if mutations file has correct format
#def check_format_mutation_file(mut_file):
#    with open(mut_file) as my_mut_file:
#        data = my_mut_file.read()
#        data = data.rstrip().split('\n')
#
#        for line in (data):
#            info = line.split(' ')
#            if len(info[0]) != 1:
#                return 0
#            wild_type = info[1][0].upper()
#            position = info[1][1:len(info[1]) - 1]
#            mut_type = info[1][len(info[1]) - 1].upper()
#
#            if (convert_res_code(wild_type) == "ERROR" or convert_res_code(mut_type) == "ERROR"):
#                return 0
#    return 1

def check_format_mutation_file(mut_file):
    with open(mut_file) as my_mut_file:
        data = my_mut_file.read()
        data = data.rstrip().split('\n')
        # IF the number of mutation is over than 20, return error.
        if len(data) > 20:
            print("The number of mutation exceeds 20")
            exit()
        for each in data:
            for eeach in each.split(';'):
                wild_type = eeach[0].upper()
                chain = eeach[1].upper()
                position = eeach[2:len(eeach)-2]
                mut_type = eeach[-1].upper()
                if (convert_res_code(wild_type) == "ERROR" or convert_res_code(mut_type) == "ERROR" or wild_type == mut_type):
                    return 0
    return 1

# ---------------------------------------------------------------------------------------------------------------------
# Convert residue codes
def convert_res_code(res):
    if (res == "A"):
        return "ALA"
    if (res == "V"):
        return "VAL"
    if (res == "L"):
        return "LEU"
    if (res == "G"):
        return "GLY"
    if (res == "S"):
        return "SER"
    if (res == "W"):
        return "TRP"
    if (res == "T"):
        return "THR"
    if (res == "Q"):
        return "GLN"
    if (res == "E"):
        return "GLU"
    if (res == "C"):
        return "CYS"
    if (res == "R"):
        return "ARG"
    if (res == "P"):
        return "PRO"
    if (res == "D"):
        return "ASP"
    if (res == "F"):
        return "PHE"
    if (res == "I"):
        return "ILE"
    if (res == "H"):
        return "HIS"
    if (res == "N"):
        return "ASN"
    if (res == "M"):
        return "MET"
    if (res == "Y"):
        return "TYR"
    if (res == "K"):
        return "LYS"
    else:
        return "ERROR"

def reconvert_res_code(res):
    if (res == "ALA"):
        return "A"
    if (res == "VAL"):
        return "V"
    if (res == "LEU"):
        return "L"
    if (res == "GLY"):
        return "G"
    if (res == "SER"):
        return "S"
    if (res == "TRP"):
        return "W"
    if (res == "THR"):
        return "T"
    if (res == "GLN"):
        return "Q"
    if (res == "GLU"):
        return "E"
    if (res == "CYS"):
        return "C"
    if (res == "ARG"):
        return "R"
    if (res == "PRO"):
        return "P"
    if (res == "ASP"):
        return "D"
    if (res == "PHE"):
        return "F"
    if (res == "ILE"):
        return "I"
    if (res == "HIS"):
        return "H"
    if (res == "ASN"):
        return "N"
    if (res == "MET"):
        return "M"
    if (res == "TYR"):
        return "Y"
    if (res == "LYS"):
        return "K"
    else:
        return "ERROR"

# ---------------------------------------------------------------------------------------------------------------------
# Check if PDB has multiple models
def check_models(pdb_file):
    num_models = 999
    cmd = "grep -e\"^MODEL\" " + pdb_file + " | wc -l"

    num_models = subprocess.check_output(cmd, shell=True)
    return int(num_models)


# ---------------------------------------------------------------------------------------------------------------------
# Get reverse mutation information
def get_reverse_mutation(mutation):
    return mutation[-1] + mutation[1:-1] + mutation[0]


# def get_reverse_mutations(mutations):
# 	result=[]
#
# 	for each in mutations:
# 		# print(each,get_reverse_mutation(each))
# 		result.append(get_reverse_mutation(each))
#
# 	return result
#def cleanPDB(pdb_file):
#    output_pdb = pdb_file[:-4] + '_cleaned.pdb'
#    cmd = "cp " + pdb_file + ' ' + output_pdb
#    os.system(cmd)
#    output = []
#
#    parser = PDBParser()
#    if os.path.exists(output_pdb):
#        try:
#            s = parser.get_structure('input_pdb', output_pdb)
#            io = PDBIO()
#
#            class NotDisordered(Select):
#                def accept_atom(self, atom):
#                    return not atom.is_disordered() or atom.get_altloc() == 'A'
#
#            io.set_structure(s)
#            if io.save(output_pdb, select=NotDisordered()) == None:
#                with open(output_pdb, 'r') as inputpdb:
#                    for each in inputpdb:
#                        ### removeHETATM
#                        if len(each.strip()) > 16 and each.strip()[0:6].strip() == 'ATOM':
#                            ## remove 'A' in front of residue name
#                            if each.strip()[16] == 'A':
#                                new_each = each.strip()[:16] + ' ' + each.strip()[17:]
#                                output.append(new_each)
#                            else:
#                                output.append(each.strip())
#                with open(output_pdb, 'w') as outputpdb:
#                    wr = csv.writer(outputpdb, delimiter='\n')
#                    wr.writerow(output)
#                    outputpdb.close()
#                    # print("--- %s seconds(CleanPDB) ---" % round((time.time() - start_time), 3))
#                    return output_pdb
#        except:
#            return False

def cleanPDB(pdb_file):
    # Copy PDB file
    output_pdb = pdb_file[:-4] + '_cleaned.pdb'
    cmd = "cp " + pdb_file + ' ' + output_pdb
    os.system(cmd)

    parser = PDBParser()
    if os.path.exists(output_pdb):
        s = parser.get_structure('input_pdb', output_pdb)[0] # Choose only 1st Model.
        io = PDBIO()
        io.set_structure(s) # Check what the user is providing and build a structure.
        io.save(output_pdb,select=HetatmFilter())
    return output_pdb

class ChainAtomSelect(Select):
    def accept_atom(self, atom):
        if atom.get_full_id()[2] in chains:
            if atom.get_full_id()[3][0] == ' ' and atom.get_aniou() == None and atom.get_element != 'H':
                if not atom.is_disordered() or atom.get_altloc() == 'A':
                    atom.set_altloc(' ')
                    return True
                else:
                    return False

class HetatmFilter(Select):
    def accept_atom(self, atom):
        #if atom.get_full_id()[3][0] == ' ' and atom.get_anisou() == None and atom.element != 'H':
        if atom.get_full_id()[3][0] == ' ' and atom.element != 'H':
            if (not atom.is_disordered()) or (atom.get_altloc() == 'A'):
            #if atom.get_altloc() == 'A':
            #    print(atom)
                atom.set_altloc(' ')
                return True
            else:
                return False

def downloadPDBfromRCSB(pdb_accession_code, output_folder, output_filename):
    result = os.path.join(output_folder, output_filename)
    temp_pdb1_filename = os.path.join(output_folder, pdb_accession_code.upper())
    if not os.path.exists(output_folder):
        cmd = "mkdir " + output_folder
        os.system(cmd)

    if len(pdb_accession_code) == 4:

        # pdbl = PDBList()
        # retrieved_pdb = pdbl.retrieve_pdb_file(pdb_accession_code, file_format='pdb1', pdir=output_folder)
        get_pdb1 = "wget https://files.rcsb.org/download/{}.pdb1.gz -O {}/{}.pdb1.gz".format(pdb_accession_code, output_folder, pdb_accession_code)
        os.system(get_pdb1)
        os.system("gunzip {}.pdb1.gz".format(temp_pdb1_filename))
        os.system("mv {} {}.pdb".format(temp_pdb1_filename+".pdb1",temp_pdb1_filename))
        # if os.path.exists(retrieved_pdb):
        #     pass
        # else:
        #     print("please bare with me")
        #     time.sleep(2)
        while os.path.exists(temp_pdb1_filename+".pdb"):
            os.rename(temp_pdb1_filename+".pdb", result)
            return True
        # while os.path.exists(retrieved_pdb):
        #     os.rename(retrieved_pdb, result)
        #     return True

        # print(retrieved_pdb)
        if result.strip()[-3:] != 'ent':
            # print("No PDB exist")
            return False

    else:
        return False


def get_interface_positions(working_dir):
    # folder='/home/ymyung/PycharmProjects/mcsm_abv2/out_interface'

    txt1 = os.path.join(working_dir, 'molecule_1.txt')
    txt2 = os.path.join(working_dir, 'molecule_2.txt')

    temp1 = pd.read_csv(txt1, delimiter=' ', names=['chain', 'resnum', 'C/I']).drop(columns=['C/I'])
    # print(temp1)

    new = temp1['chain'] + '|' + temp1['resnum'].astype(str)

    # print(new.tolist())

    temp2 = pd.read_csv(txt2, delimiter=' ', names=['chain', 'resnum', 'C/I']).drop(columns=['C/I'])

    new2 = temp2['chain'] + '|' + temp2['resnum'].astype(str)
    # print(new2.tolist())

    new3 = new.tolist() + new2.tolist()
    # print(new3)

    # print(len(new),len(new2),len(new3))
    # print(temp2)

    return new3


def get_rest_of_mutations(pdb_file, resnum, mut_chain):
    AA = set(['A', 'R', 'N', 'D', 'C', 'Q', 'E', 'G', 'H', 'I', 'L', 'K', 'M', 'F', 'P', 'S', 'T', 'W', 'Y', 'V'])

    ## assign
    # wild = mutation[0]
    # mut = mutation[-1]

    icode = " "

    if resnum[-1].isalpha():
        icode = resnum[-1].upper()
        res_num = int(resnum[0:-1])
    else:
        res_num = int(resnum)
    # partner_chains = partner_chains.split(',')

    ## code start
    p = PDBParser()
    structure = p.get_structure('input', pdb_file)
    model = structure[0]

    # from mut_chain
    chain = model[mut_chain]
    mut_residue = chain[(' ', res_num, icode)]

    mutagenesis_list = sorted(list(AA - set(seq1(mut_residue.get_resname()))))
    # print(resnum)

    # return pdb_file,resnum,mut_chain,seq1(mut_residue.get_resname()),mutagenesis_list
    # result = [seq1(mut_residue.get_resname()), ''.join(mutagenesis_list)]
    return seq1(mut_residue.get_resname()), mutagenesis_list


def get_partner_chains(pdb_file, selected_chain_list, antibody_chain_list):
    all_chain_list = []
    p = PDBParser()

    structure = p.get_structure('input', pdb_file)
    model = structure[0]

    for each in model:
        # print(each.get_id())
        all_chain_list.append(each.get_id())

    temp_result = []

    if len(list(selected_chain_list)) == 1:  # single point mutation

        if selected_chain_list in antibody_chain_list:  # H in H, L

            partner_chains =  ''.join(list(set(all_chain_list) - set(antibody_chain_list)))  # A, B, H, L - H,L = AB

            if len(partner_chains) == 0:
                first_set = antibody_chain_list[0:2]
                second_set = antibody_chain_list[2:]
                print(first_set,second_set)

                if selected_chain_list in first_set:
                    return ''.join(second_set)
                else:
                    return ''.join(first_set)
            else:
                return partner_chains

        else:

            return antibody_chain_list

    elif len(list(selected_chain_list)) > 1:  # list mutation ['V','H','L','W], ['LH']

        for x in range(0, len(selected_chain_list)):

            if selected_chain_list[x] in antibody_chain_list:

                temp_result.append(
                    ''.join(list(set(all_chain_list) - set(antibody_chain_list))))  # 'L','H','V','W' - 'V' = LHW

            else:

                temp_result.append(''.join(antibody_chain_list))

        return temp_result


def get_antigen_chain_list(pdb_file):
    all_chain_list = []
    p = PDBParser()
    antibody_chains = list(getAntibodyChains(pdb_file))
    structure = p.get_structure('input', pdb_file)
    model = structure[0]
    for each in model:
        # print(each.get_id())
        all_chain_list.append(each.get_id())
    return ''.join(set(all_chain_list) - set(antibody_chains))

def getFASTAnPDBnumberMap(input_pdb,chain_id):
    pdb_id = input_pdb.split('/')[-1]
    new_dic = dict()
    nn = list()
    out_list = pd.DataFrame()

    for each in new_dic.values():
        nn.append(" ".join(each).split(" "))

    structure = PDBParser(QUIET=True).get_structure('input_pdb', input_pdb)
    tempp_list = list()

    for model in structure:
        if len(structure) == 1:
            new_number = 1
            for residue in model[chain_id]:
                tempp_list.append([new_number, str(residue)])
                chain_ID = str(model[chain_id]).rsplit('id=')[1][0].strip()
                amino_acid_name = str(residue).split('<Residue')[1].strip().split(' ')[0]
                amino_acid_number = str(residue).split('resseq=')[1].split('icode')[0].strip()
                icode_code = str(residue).rsplit('icode=')[1].strip()
                if len(icode_code) != 1:
                    icode_code = icode_code[0].strip()
                    amino_acid_number = amino_acid_number + icode_code
                out_list = out_list.append({'chain':chain_id,'wild':amino_acid_name,'pdb_numb':amino_acid_number,'fasta_numb':str(new_number)},ignore_index=True)
                new_number += 1
        else:
            print("Your PDB has more than one model structure", pdb_id)
            print("If some pdb has more than one model, user should remove unnecessary models, otherwise edit the above code!")

    return out_list


def getFASTAnum(input_pdb, chain_id, amino_acid, res_num):
    pdb_id = input_pdb.split('/')[-1]
    new_dic = {}
    nn = []
    out_list = []

    for each in new_dic.values():
        nn.append(" ".join(each).split(" "))

    structure = PDBParser(QUIET=True).get_structure('input_pdb', input_pdb)
    tempp_list = []

    for model in structure:
        if len(structure) == 1:
            new_number = 1
            for residue in model[chain_id]:
                tempp_list.append([new_number, str(residue)])
                chain_ID = str(model[chain_id]).rsplit('id=')[1][0].strip()
                amino_acid_name = str(residue).split('<Residue')[1].strip().split(' ')[0]
                amino_acid_number = str(residue).split('resseq=')[1].split('icode')[0].strip()
                icode_code = str(residue).rsplit('icode=')[1].strip()
                if len(icode_code) != 1:
                    icode_code = icode_code[0].strip()
                    amino_acid_number = amino_acid_number + icode_code
                if res_num == amino_acid_number:
                    # print(pdb_id,",",chain_id,",",res_num,"==>",chain_ID,",", amino_acid_name,",",new_number)
                    out_list.extend(
                        [pdb_id, chain_id, amino_acid, res_num, "-->", chain_ID, amino_acid_name, new_number])
                # print(chain_ID,amino_acid_name,amino_acid_number,icode_code,'==>',new_number)
                new_number += 1
        else:
            print("Your PDB has more than one model structure", pdb_id)
            print("If some pdb has more than one model, user should remove unnecessary models, otherwise edit the above code!")

    # print(out_list)

    return out_list[-1]


## Check number of chains in provided PDB file
def check_num_chains(pdb_file):
    num_chains = 999
    cmd = "grep ^ATOM " + pdb_file + " | cut -c22 | grep \"^\w\" | sort -u | wc -l"
    num_chains = subprocess.check_output(cmd, shell=True)
    return int(num_chains)


## Check if mutation is consistent with provided PDB file
def check_mutation(pdb_file, wild_type, position, chain):
    # print("Checking mutation: " + wild_type + " " + position)
    wild_res = convert_res_code(wild_type)
    cmd = "grep ^ATOM " + pdb_file + " | grep " + wild_res + " | grep \"^.\{21\}" + chain + "\" | cut -c23-27 | sed 's/[ ]*//g' | awk {'if($1 == \"" + position + "\"){print}'} | wc -l"
    residue_found = subprocess.check_output(cmd, shell=True)
    # print("Found = ", residue_found)
    return residue_found


## Check if mutations file has correct format
#def check_format_mutation_file(mut_file):
#    with open(mut_file) as my_mut_file:
#        data = my_mut_file.read()
#        data = data.rstrip().split('\n')
#
#        for line in (data):
#            info = line.split(' ')
#            if len(info[0]) != 1:
#                return 0
#            wild_type = info[1][0].upper()
#            position = info[1][1:len(info[1]) - 1]
#            mut_type = info[1][len(info[1]) - 1].upper()
#
#            if (convert_res_code(wild_type) == "ERROR" or convert_res_code(mut_type) == "ERROR"):
#                return 0
#    return 1


## Get all chains in given PDB
def getAllChains(pdb_file):
    chain_list = []
    p = PDBParser()
    #print(pdb_file)
    structure = p.get_structure('input_pdb', pdb_file)
    for each in structure[0]:
        chain_list.append(each.get_id())
    return ''.join(chain_list)

#def checkAbAg(pdb_file):
#    chain_list = list(getAllChains(pdb_file))
#    result = []
#
#    for chain in chain_list:
#        annotation_result_temp = runDirectly(pdbtofasta(pdb_file, chain), 'chothia')
#        # print(annotation_result_temp)
#        buf = StringIO(annotation_result_temp)
#        temp_pd = pd.DataFrame()
#
#        if len(annotation_result_temp) < 30:
#            # print("Given chain doesn't have any ANTIBODY sequence.")
#            # temp_pd = temp_pd.append({'chain': 'antigen'}, ignore_index=True)
#            temp_pd = 'antigen'
#            # print(temp_pd)
#        else:
#
#            try:
#
#                for each in buf.readlines():
#
#                    if not each.startswith('#'):
#                        stripted = each.strip()
#                        trimed = list(filter(None, stripted.split(' ')))
#                        temp_pd = temp_pd.append([trimed[0]])
#
#                temp_pd.index += 1
#                temp_pd.columns = ['chain']
#                temp_pd_set = set(temp_pd['chain'].tolist())
#                # print(temp_pd)
#                temp_pd_set.remove('//')
#                # print(temp_pd_set)
#                if len(temp_pd_set) > 1:
#                    temp_pd = ''.join(sorted(temp_pd_set))
#                else:
#                    temp_pd = list(temp_pd_set)[0]
#
#            except (KeyError):
#                # print("Given information is wrong")
#                return 'error'
#
#        result.append([chain, temp_pd])
#
#    result_pd = pd.DataFrame()
#
#    for each in result:
#        result_pd = result_pd.append({'chain': each[0], 'type': each[1]}, ignore_index=True)
#
#    result_pd.index += 1
#
#    result_pd = result_pd.groupby(['type'])
#
#    return result_pd
#

def checkAbAg(pdb_file):
    chain_list = list(getAllChains(pdb_file))
    result_pd = pd.DataFrame()
    H_list = list()
    L_list = list()
    ag_list = list()
    try:
        for chain in chain_list:
            annotation_result_temp = runDirectly(pdbtofasta(pdb_file, chain), 'chothia')
            buf = StringIO(annotation_result_temp)
            if len(annotation_result_temp) < 30:
                ag_list.extend(chain)
            else:
                try:
                    temp_set = list()
                    for each in buf.readlines():
                        if not each.startswith('#'):
                            stripted = each.strip()
                            trimed = list(filter(None, stripted.split(' ')))
                            temp_set.extend([trimed[0]])
                    temp_set = set(temp_set[0:-1])
                    for each_set in temp_set:
                        if each_set == 'H':
                            H_list.extend(chain)
                        else:
                            L_list.extend(chain)

                except (KeyError):
                    return 'error'

        if len(L_list) == 0 and len(H_list) == 0 and len(ag_list) !=0:
            return ('NA',H_list,L_list,ag_list)
        elif len(H_list) > 0 and len(L_list) == 0 and len(ag_list) > 0:
            return ('Nanobody',H_list,L_list,ag_list)
        elif H_list == L_list and len(H_list) !=0 and len(ag_list) > 0:
            return ('scFv',H_list,L_list,ag_list)
        elif len(ag_list) == 0 :
            return ('Ab-Ab',H_list,L_list,ag_list)
        else:
            return ('Fab',H_list,L_list,ag_list)
    except:
        return ('NA',H_list,L_list,ag_list)

#def checkTypeofAb(pdb_file):
#    group_result = checkAbAg(pdb_file)
#
#    if len(group_result.groups.keys()) == 2:
#
#        if 'HL' in group_result.groups.keys():
#            return "scFv"
#        else:
#            return "Nanobody"
#
#    else:
#        return "Fab"


#def getLightChains(pdb_file):
#
#    group_result = checkAbAg(pdb_file)
#    print(group_result)
#    print(group_result.groups())
#    if len(group_result.groups.keys()) == 2:
#
#        if 'HL' in group_result.groups.keys():
#            #print("This is scFv.")
#            return group_result.get_group('HL')['chain'].tolist()
#        else:
#            # print("This is nanobody and there is no Light Chain!")
#            return False
#
#    else:
#        #print("This is Fab.")
#        return group_result.get_group('L')['chain'].tolist()
#
#def getHeavyChains(pdb_file):
#
#    group_result = checkAbAg(pdb_file)
#
#    if len(group_result.groups.keys()) == 2:
#
#        if 'HL' in group_result.groups.keys():
#            #print("This is scFv.")
#            return group_result.get_group('HL')['chain'].tolist()
#        else:
#            #print("This is nanobody!")
#            return group_result.get_group('H')['chain'].tolist()
#
#    else:
#        #print("This is Fab.")
#        return group_result.get_group('H')['chain'].tolist()



def getHeavyFasta(pdb_file):
    # runANARCI
    #heavy_chain = getHeavyChains(pdb_file)
    heavy_chain = checkAbAg(pdb_file)[1]
    if len(heavy_chain) > 1:
        heavy_chain = heavy_chain[0]

    return pdbtofasta(pdb_file, heavy_chain)


def getLightFasta(pdb_file):
    # runANARCI
    #light_chain = getLightChains(pdb_file)
    light_chain = checkAbAg(pdb_file)[2]
    #print(light_chain)
    #if light_chain == False:
    #    return False
    if len(light_chain) > 1:
        light_chain = light_chain[0]

    return pdbtofasta(pdb_file, light_chain)


# ## Check the jobs that have failed
# def check_failed(jobID):
#     from redis import StrictRedis
#     from rq import Queue, get_failed_queue, push_connection
#     con = Redis()
#     push_connection(con)
#     fq = get_failed_queue()
#     job_ids = fq.job_ids
#     fq_count = len(job_ids)
#
#     for job_id in job_ids:
#         if job_id == jobID:
#
#             return 1
#         else:
#             return 0


## Assign outcome regarding DDG value
def outcome(row):
    if row['DDG'] > 0.00:
        val = 'increased affinity'
    elif row['DDG'] < 0.00:
        val = 'decreased affinity'
    elif row['DDG'] == 0.00:
        val = 'neutral'

    return val


# Email Assistant
def send_contacts_email(name, message, email_from=""):
    with app.app_context():
        # recipients = ['ymyung@student.unimelb.edu.au','david.ascher@unimelb.edu.au','douglas.pires@minas.fiocruz.br']
        recipients = ['biosigab@gmail.com']
        msg = Message("New contact message - mmCSM-NA", sender="biosigab@gmail.com",
                      recipients=['ymyung@student.unimelb.edu.au'])
        msg.html = render_template("contact_mail.html", email=email_from, name=name, message=message)

        mail = Mail()
        mail.init_app(app)

        mail.send(msg)
        return True


def send_email(email, message):
    with app.app_context():
        if email == "":
            msg = "Email not provided"
            return False

        msg = Message("Info - mmCSM-NA", sender='biosigab@gmail.com', recipients=[email])
        msg.html = "<b>From:</b> biosigab@gmail.com <br/><b>Name:</b> mmCSM-NA Team<br/><b>Message:</b> %s<br/>" % (
            message)

        mail = Mail()
        mail.init_app(app)

        mail.send(msg)
        return True


def send_email_results(email, results_link):
    with app.app_context():
        # recipients = ['ymyung@student.unimelb.edu.au','david.ascher@unimelb.edu.au','douglas.pires@minas.fiocruz.br']
        recipients = ['biosigab@gmail.com']
        msg = Message("Submission results - mmCSM-NA", sender="biosigab@gmail.com", recipients=[email])
        msg.html = render_template("prediction_ready_mail.html", results_link=results_link)

        mail = Mail()
        mail.init_app(app)

        mail.send(msg)
        return True


def send_email_failed(email, results_link):
    with app.app_context():
        recipients = ['biosigab@gmail.com']
        msg = Message("Processing Failed - mmCSM-NA", sender="biosigab@gmail.com", recipients=[email])
        msg.html = render_template("processing_error.html", job_id=results_link)

        mail = Mail()
        mail.init_app(app)

        mail.send(msg)
        return True


# [ANARCI] Antibody Annotation
def runDirectly(fasta, scheme):
    # only for chothia

    anarci_run = Popen(["{}/bin/ANARCI".format(app.config.get('VIRTUALENV_PATH','')), "-i", fasta, "--scheme", scheme], stdout=PIPE, stderr=PIPE)
    stdout, stderr = anarci_run.communicate();  # print(len(stdout.decode('ascii')))
    return stdout.decode('ascii')


def analysisDirectly(annotated_seq, fasta_seq, wild, position):
    buf = StringIO(annotated_seq)
    temp_pd = pd.DataFrame()

    if len(annotated_seq) < 30:
        # print("Given chain doesn't have any ANTIBODY sequence.")
        return 'none'
    else:

        try:

            for each in buf.readlines():

                if not each.startswith('#'):
                    stripted = each.strip()
                    trimed = list(filter(None, stripted.split(' ')))
                    # print(trimed)

                    if len(trimed) == 3:
                        temp_pd = temp_pd.append([trimed], ignore_index=True)

                    if len(trimed) == 4:
                        temp_pd = temp_pd.append([[trimed[0], trimed[1] + trimed[2], trimed[3]]], ignore_index=True)

            temp_pd.index += 1
            temp_pd.columns = ['chain', 'resnum', 'wild']

            # fasta to fasta with seq#
            fasta_seq = pd.DataFrame({'fasta': list(fasta_seq)})
            fasta_seq.index += 1

            j = 1
            for index, each in temp_pd.iterrows():
                if each['wild'] != '-':
                    temp_pd.loc[index,'new_ID'] = int(j)
                    j +=1
            try :

                for index, each in temp_pd.iterrows():
                    if each['new_ID'] > 0:
                        if (int(each['new_ID']) == int(position)) & (each['wild'] == wild):
                            return ((each['chain'] + each['resnum']) + '(' + getCDR(temp_pd,each['chain'] + each['resnum']) + ')')

            except (TypeError):
                # print("happens")
                return ("cannot type")

        except (KeyError):
            # print("Given information is wrong")
            # print('keyError')
            return ("cannot type")


def readABannotation(annotated_file_csv):
    input_pd = pd.read_csv(annotated_file_csv)

    input_pd.drop(input_pd.columns[:13], axis=1, inplace=True)
    input_pd = input_pd.T
    input_pd.rename(columns={0: 'chothia_fasta'}, inplace=True)
    input_pd['chothia_numbering'] = input_pd.index
    input_pd = input_pd.reset_index()
    input_pd.index += 1
    return input_pd


def readFasta(fasta):
    f = open(fasta, 'r')
    next(f)

    fasta_list = list(f.read().strip())

    new_pd = pd.DataFrame(fasta_list, columns=['fasta'])
    new_pd.index += 1
    new_pd['fasta_numbering'] = new_pd.index

    return new_pd


def checkChothia(chothia_results):
    if '35A' in chothia_results:

        if '35B' in chothia_results:
            return 'both'

        return '35A'

    else:
        return '35'


def getCDRNumberH(chothia_mode, chothia_number):
    chothia_number = str(chothia_number)
    chothia_mode = str(chothia_mode)

    if chothia_number[-1].isalpha():
        if chothia_number == 'nan':
            return '-'
        else:
            position = int(chothia_number[:-1])
    else:
        position = int(chothia_number)

    if chothia_mode == 'both':
        if position < 26:
            return "HFR1"
        if (26 <= position) & (position <= 34):
            return "CDR-H1"
        if (34 < position) & (position < 52):
            return "HFR2"
        if (52 <= position) & (position <= 56):
            return "CDR-H2"
        if (56 < position) & (position < 95):
            return "HFR3"
        if (95 <= position) & (position <= 102):
            return "CDR-H3"
        if (103 <= position) & (position <= 113):
            return "HFR4"
        else:
            return "-"
    elif chothia_mode == '35A':
        if position < 26:
            return "HFR1"
        if (26 <= position) & (position <= 33):
            return "CDR-H1"
        if (33 < position) & (position < 52):
            return "HFR2"
        if (52 <= position) & (position <= 56):
            return "CDR-H2"
        if (56 < position) & (position < 95):
            return "HFR3"
        if (95 <= position) & (position <= 102):
            return "CDR-H3"
        if (103 <= position) & (position <= 113):
            return "HFR4"
        else:
            return "-"

    elif chothia_mode == '35':
        if position < 26:
            return "HFR1"
        if (26 <= position) & (position <= 32):
            return "CDR-H1"
        if (32 < position) & (position < 52):
            return "HFR2"
        if (52 <= position) & (position <= 56):
            return "CDR-H2"
        if (56 < position) & (position < 95):
            return "HFR3"
        if (95 <= position) & (position <= 102):
            return "CDR-H3"
        if (103 <= position) & (position <= 113):
            return "HFR4"
        else:
            return "-"


def getCDRNumberL(chothia_number):
    chothia_number = str(chothia_number)

    if chothia_number[-1].isalpha():
        if chothia_number == 'nan':
            return '-'
        else:
            position = int(chothia_number[:-1])

    else:

        position = int(chothia_number)

    if position < 24:
        return "LFR1"
    if (24 <= position) & (position <= 34):
        return "CDR-L1"
    if (34 < position) & (position < 50):
        return "LFR2"
    if (50 <= position) & (position <= 56):
        return "CDR-L2"
    if (56 < position) & (position < 89):
        return "LFR3"
    if (89 <= position) & (position <= 97):
        return "CDR-L3"
    if (98 <= position) & (position <= 110):
        return "LFR4"
    else:
        return "-"


def addChaintoChothiaNumbering(letter, row):
    row = str(row)
    if row == 'nan':
        return '-'
    else:
        return letter.upper() + row


def getCDR(annotated_seq, chothia_number):

    if chothia_number[-1].isalpha():
        chain = chothia_number[0]
        position = int(chothia_number[1:-1])
        icode = chothia_number[-1]
    else:
        chain = chothia_number[0]
        position = int(chothia_number[1:])

    if chain == 'H' and len(annotated_seq[annotated_seq['resnum'].str.match('35A')]):

        if chain == 'H' and len(annotated_seq[annotated_seq['resnum'].str.match('35B')]):

            # with H35A and H35B
            if position < 26:
                return "HFR1"
            if (26 <= position) & (position <= 34):
                return "CDR-H1"
            if (34 < position) & (position < 52):
                return "HFR2"
            if (52 <= position) & (position <= 56):
                return "CDR-H2"
            if (56 < position) & (position < 95):
                return "HFR3"
            if (95 <= position) & (position <= 102):
                return "CDR-H3"
            else:
                return "HFR4"

        else:

            # with H35A
            if position < 26:
                return "HFR1"
            if (26 <= position) & (position <= 33):
                return "CDR-H1"
            if (33 < position) & (position < 52):
                return "HFR2"
            if (52 <= position) & (position <= 56):
                return "CDR-H2"
            if (56 < position) & (position < 95):
                return "HFR3"
            if (95 <= position) & (position <= 102):
                return "CDR-H3"
            else:
                return "HFR4"

    else:

        if chain == 'L':
            if position < 24:
                return "LFR1"
            if (24 <= position) & (position <= 34):
                return "CDR-L1"
            if (34 < position) & (position < 50):
                return "LFR2"
            if (50 <= position) & (position <= 56):
                return "CDR-L2"
            if (56 < position) & (position < 89):
                return "LFR3"
            if (89 <= position) & (position <= 97):
                return "CDR-L3"
            else:
                return "LFR4"

        if chain == 'H':  # without H35A or H35B
            if position < 26:
                return "HFR1"
            if (26 <= position) & (position <= 32):
                return "CDR-H1"
            if (32 < position) & (position < 52):
                return "HFR2"
            if (52 <= position) & (position <= 56):
                return "CDR-H2"
            if (56 < position) & (position < 95):
                return "HFR3"
            if (95 <= position) & (position <= 102):
                return "CDR-H3"
            else:
                return "HFR4"


def pdbtofasta(pdb_file, selected_chain):
    aa3to1 = {
        'ALA': 'A', 'VAL': 'V', 'PHE': 'F', 'PRO': 'P', 'MET': 'M',
        'ILE': 'I', 'LEU': 'L', 'ASP': 'D', 'GLU': 'E', 'LYS': 'K',
        'ARG': 'R', 'SER': 'S', 'THR': 'T', 'TYR': 'Y', 'HIS': 'H',
        'CYS': 'C', 'ASN': 'N', 'GLN': 'Q', 'TRP': 'W', 'GLY': 'G',
    }
    ca_pattern = re.compile("^ATOM\s{2,6}\d{1,5}\s{2}CA\s[\sA]([A-Z]{3})\s([\s\w])")
    # ca_pattern = re.compile("^ATOM\s{2,6}\d{1,5}\s{2}CA\s[\sA]([A-Z]{3})\s([\s\w])|^HETATM\s{0,4}\d{1,5}\s{2}CA\s[\sA](MSE)\s([\s\w])")
    chain_dict = dict()
    chain_list = []

    fp = open(pdb_file, 'rU')
    for line in fp.read().splitlines():
        if line.startswith("ENDMDL"):
            break
        match_list = ca_pattern.findall(line)

        if match_list:
            # resn = match_list[0][0] + match_list[0][2]
            # chain = match_list[0][1] + match_list[0][3]
            resn = match_list[0][0]
            chain = match_list[0][1]
            if chain in chain_dict:
                chain_dict[chain] += aa3to1[resn]
            else:
                chain_dict[chain] = aa3to1[resn]
                chain_list.append(chain)
    fp.close()
    # result = str('>%s:%s\n%s\n' % (os.path.split(pdb_file)[1], chain, chain_dict[chain]))
    # print(selected_chain)
    result = chain_dict.get(selected_chain[0])
    return result


def getAbAnnotation(pdb_file, wild, chain, position):
    fasta = pdbtofasta(pdb_file, chain)
    return analysisDirectly(runDirectly(fasta, 'chothia'), fasta, wild, position)


def getAntibodyChains(pdb_file):
    chain_list = list(getAllChains(pdb_file))
    result = []
    for chain in chain_list:
        annotation_result_temp = runDirectly(pdbtofasta(pdb_file, chain), 'chothia')
        # print(annotation_result_temp)
        if len(annotation_result_temp) > 30:
            result.append(chain)
    return ''.join(result)

def readFastaFile(fasta_file):
    f = open(fasta_file, 'r')
    next(f)

    fasta_list = list(f.read().strip())

    new_pd = pd.DataFrame(fasta_list, columns=['fasta'])
    new_pd.index += 1
    new_pd['fasta_numbering'] = new_pd.index

    return new_pd


def readFasta(fasta):
    buf = StringIO(fasta)
    new_pd = pd.DataFrame(list(fasta), columns=['fasta'])
    new_pd.index += 1
    new_pd['fasta_numbering'] = new_pd.index

    return new_pd


def getCDRnumbForHighlight(fasta,fastanPDBMap):
    CDR_dictionary = dict()
    result_list = list()
    annotated_seq = runDirectly(fasta, 'chothia')
    buf = StringIO(annotated_seq)
    temp_pd = pd.DataFrame()
    fasta = readFasta(fasta)

    if len(annotated_seq) < 30:
        # print("Given chain doesn't have any ANTIBODY Light sequence.")
        return 'none'
    else:
        try:
            for each in buf.readlines():
                if not each.startswith('#'):
                    stripted = each.strip()
                    trimed = list(filter(None, stripted.split(' ')))
                    if len(trimed) == 3:
                        temp_pd = temp_pd.append([trimed], ignore_index=True)
                    if len(trimed) == 4:
                        temp_pd = temp_pd.append([[trimed[0], trimed[1] + trimed[2], trimed[3]]], ignore_index=True)

            temp_pd.index += 1
            temp_pd.columns = ['chain', 'chothia_numbering', 'chothia_fasta']

            result = pd.merge(temp_pd, fasta, left_index=True, right_index=True, how='outer')
            result['CDR'] = result.apply(lambda row: getCDRNumberL(row['chothia_numbering']), axis=1)
            #result['chothia_numbering'] = result.apply(
            #    lambda row: addChaintoChothiaNumbering('L', row['chothia_numbering']), axis=1)

            #print(result.to_string())
            #print(result[['fasta','fasta_numbering','chothia_numbering','CDR']].T)
            # return result[['fasta', 'fasta_numbering', 'chothia_numbering', 'CDR']].T
            #CDR_L1 = list()
            #CDR_L2 = list()
            #CDR_L3 = list()
            for index, row in result.iterrows():
                if row['CDR'].startswith('CDR'):
                    pdb_numb = fastanPDBMap.query('fasta_numb == {}'.format(row['fasta_numbering']))['pdb_numb'].values[0]
                    result_list.extend([pdb_numb])
                    #result_list.extend([row['fasta_numbering']])
           #     if row['CDR'].startswith('CDR-L1'):
           #         CDR_L1.append(row['fasta_numbering'])
           #     elif row['CDR'].startswith('CDR-L2'):
           #         CDR_L2.append(row['fasta_numbering'])
           #     elif row['CDR'].startswith('CDR-L3'):
           #         CDR_L3.append(row['fasta_numbering'])
           # CDR_dictionary['CDR-L1'] = CDR_L1
           # CDR_dictionary['CDR-L2'] = CDR_L2
           # CDR_dictionary['CDR-L3'] = CDR_L3

            #return result['fasta'].values,result['fasta_numbering'].values,result['chothia_numbering'].values,CDR_dictionary
            return result_list
        except (KeyError):
            # print("Given information is wrong")
            return 'error'


def heavySeqView(heavy_fasta):
    CDR_dictionary = {}

    annotated_seq = runDirectly(heavy_fasta, 'chothia')

    buf = StringIO(annotated_seq)
    temp_pd = pd.DataFrame()

    fasta = readFasta(heavy_fasta)

    if len(annotated_seq) < 30:
        # print("Given chain doesn't have any ANTIBODY Light sequence.")
        return 'none'
    else:
        try:
            for each in buf.readlines():

                if not each.startswith('#'):
                    stripted = each.strip()
                    trimed = list(filter(None, stripted.split(' ')))

                    if len(trimed) == 3:
                        temp_pd = temp_pd.append([trimed], ignore_index=True)

                    if len(trimed) == 4:
                        temp_pd = temp_pd.append([[trimed[0], trimed[1] + trimed[2], trimed[3]]], ignore_index=True)

            temp_pd.index += 1
            temp_pd.columns = ['chain', 'chothia_numbering', 'chothia_fasta']

            result = pd.merge(temp_pd, fasta, left_index=True, right_index=True, how='outer')
            result['CDR'] = result.apply(lambda row: getCDRNumberL(row['chothia_numbering']), axis=1)
            result['chothia_numbering'] = result.apply(
                lambda row: addChaintoChothiaNumbering('L', row['chothia_numbering']), axis=1)

            #print(result.to_string())
            #print(result[['fasta','fasta_numbering','chothia_numbering','CDR']].T)
            # return result[['fasta', 'fasta_numbering', 'chothia_numbering', 'CDR']].T
            CDR_H1 = list()
            CDR_H2 = list()
            CDR_H3 = list()
            for index, row in result.iterrows():
                if row['CDR'].startswith('CDR-H1'):
                    CDR_L1.append(row['fasta_numbering'])
                    #print("startswith{}".format(row))
                    #CDR_dictionary[row['CDR']] = row['fasta_numbering']
                elif row['CDR'].startswith('CDR-H2'):
                    CDR_L2.append(row['fasta_numbering'])
                elif row['CDR'].startswith('CDR-H3'):
                    CDR_L3.append(row['fasta_numbering'])
            CDR_dictionary['CDR-H1'] = CDR_H1
            CDR_dictionary['CDR-H2'] = CDR_H2
            CDR_dictionary['CDR-H3'] = CDR_H3
            #print(CDR_dictionary)
            return result['fasta'].values,result['fasta_numbering'].values,result['chothia_numbering'].values,CDR_dictionary

        except (KeyError):
            # print("Given information is wrong")
            return 'error'

def scFVSeqView(scFV_fasta):
    CDRH_dictionary = {}
    CDRL_dictionary = {}

    annotated_seq = runDirectly(scFV_fasta, 'chothia')
    buf = StringIO(annotated_seq)
    temp_pdH = pd.DataFrame()
    temp_pdL = pd.DataFrame()

    fasta = readFasta(scFV_fasta)

    if len(annotated_seq) < 30:
        # print("Given chain doesn't have any ANTIBODY Heavy sequence.")
        return 'none'
    else:

        try:
            heavy_start_index = {};
            light_start_index = {};


            for each in buf.readlines():
                if each.startswith('#|') and not each.startswith('#|species'):

                    stripted = each.strip()
                    trimed = list(filter(None, stripted.split('|')))[1:]

                    if trimed[1] == 'H':
                        heavy_start_index['H'] = trimed[-2]

                    else:
                        light_start_index['L'] = trimed[-2]


                if not each.startswith('#'):
                    stripted = each.strip()
                    trimed = list(filter(None, stripted.split(' ')))

                    if trimed[0] =='H':

                        if len(trimed) == 3:
                            temp_pdH = temp_pdH.append([trimed], ignore_index=True)

                        if len(trimed) == 4:
                            temp_pdH = temp_pdH.append([[trimed[0], trimed[1] + trimed[2], trimed[3]]], ignore_index=True)
                    else:
                        if len(trimed) == 3:
                            temp_pdL = temp_pdL.append([trimed], ignore_index=True)

                        if len(trimed) == 4:
                            temp_pdL = temp_pdL.append([[trimed[0], trimed[1] + trimed[2], trimed[3]]],
                                                       ignore_index=True)

            temp_pdH.index = temp_pdH.index + int(heavy_start_index.get('H')) + 1
            temp_pdH.columns = ['chain', 'chothia_numbering', 'chothia_fasta']
            temp_pdL.index = temp_pdL.index +int(light_start_index.get('L')) + 1
            temp_pdL.columns = ['chain', 'chothia_numbering', 'chothia_fasta']

            resultH = pd.merge(temp_pdH, fasta, left_index=True, right_index=True, how='outer')
            resultH['CDR'] = resultH.apply(
                lambda row: getCDRNumberH(checkChothia(resultH['chothia_numbering']), row['chothia_numbering']), axis=1)
            resultH['chothia_numbering'] = resultH.apply(
                lambda row: addChaintoChothiaNumbering('H', row['chothia_numbering']), axis=1)

            i = 1
            for eeach in resultH['CDR'].values:
                CDRH_dictionary[i] = eeach
                i += 1

            CDR_H1 = []
            for key, value in CDRH_dictionary.items():
                if value == 'CDR-H1':
                    CDR_H1.append(key)
            CDRH1 = str("{x:" + str(CDR_H1[0]) + ",y:" + str(CDR_H1[-1]) + ",description:\"H1\",id:\"H1\"}")

            CDR_H2 = []
            for key, value in CDRH_dictionary.items():
                if value == 'CDR-H2':
                    CDR_H2.append(key)
            CDRH2 = str("{x:" + str(CDR_H2[0]) + ",y:" + str(CDR_H2[-1]) + ",description:\"H2\",id:\"H2\"}")

            CDR_H3 = []
            for key, value in CDRH_dictionary.items():
                if value == 'CDR-H3':
                    CDR_H3.append(key)
            CDRH3 = str("{x:" + str(CDR_H3[0]) + ",y:" + str(CDR_H3[-1]) + ",description:\"H3\",id:\"H3\"}")

            CDRH_string = "[" + CDRH1 + "," + CDRH2 + "," + CDRH3 + "]"

            resultL = pd.merge(temp_pdL, fasta, left_index=True, right_index=True, how='outer')
            resultL['CDR'] = resultL.apply(lambda row: getCDRNumberL(row['chothia_numbering']), axis=1)
            resultL['chothia_numbering'] = resultL.apply(
                lambda row: addChaintoChothiaNumbering('L', row['chothia_numbering']), axis=1)

            i = 1
            for eeach in resultL['CDR'].values:
                CDRL_dictionary[i] = eeach
                i += 1

            CDR_L1 = []
            for key, value in CDRL_dictionary.items():
                if value == 'CDR-L1':
                    CDR_L1.append(key)
            CDRL1 = str("{x:" + str(CDR_L1[0]) + ",y:" + str(CDR_L1[-1]) + ",description:\"L1\",id:\"L1\"}")

            CDR_L2 = []
            for key, value in CDRL_dictionary.items():
                if value == 'CDR-L2':
                    CDR_L2.append(key)
            CDRL2 = str("{x:" + str(CDR_L2[0]) + ",y:" + str(CDR_L2[-1]) + ",description:\"L2\",id:\"L2\"}")

            CDR_L3 = []
            for key, value in CDRL_dictionary.items():
                if value == 'CDR-L3':
                    CDR_L3.append(key)
            CDRL3 = str("{x:" + str(CDR_L3[0]) + ",y:" + str(CDR_L3[-1]) + ",description:\"L3\",id:\"L3\"}")

            CDRL_string = "[" + CDRL1 + "," + CDRL2 + "," + CDRL3 + "]"

            result_H = resultH['fasta'].values, resultH['fasta_numbering'].values, resultH['chothia_numbering'].values,CDRH_string
            result_L = resultL['fasta'].values, resultL['fasta_numbering'].values, resultL['chothia_numbering'].values,CDRL_string

            return result_H, result_L

        except (KeyError):
            # print("Given information is wrong")
            return False

## Check Interface
def checkInterfaceorNot(interface_list, chain, position):
    if chain + '|' + position in interface_list:
        return True
    else:
        return False


## Check if antibody chains are valid
def isAntibody(pdb_file):
    # print(len(getAntibodyChains(pdb_file)))
    if len(getAntibodyChains(pdb_file)) > 0:
        return True,getAllChains(pdb_file)
    else:
        return False,getAllChains(pdb_file)

# print(isAntibody('/home/ymyung/Public/Project/mCSM_AB_v2/calculation/data/pdb/mCSM_AB_v2/all_PDBs/4KRL.filtered.SKEMPI2.pdb'))
def evaluateDistance(distance):
    # Set Confidence Level according distance between given mutation position and its closest binding partner.
    if float(distance) < 5.0:
        return str("Your Prediction is in GOOD Confidence Level")

    if float(distance) > 5.0:
        return str("Your Prediction is in MODERATE Confidence Level")

    if float(distance) > 10.0:
        return str("Your Prediction is in LOW Confidence Level")


# ##--------------------------------------------------------------------------------------------------
# ## GENERAL FUNCTIONS
## Check if string follows float format
def isFloat(string):
    try:
        float(string)
        return True
    except ValueError:
        return False


## Check if provided file follows PDB format
def check_format(pdb_file):
    with open(pdb_file) as my_pdb_file:
        data = my_pdb_file.read()
        data = data.rstrip().split('\n')

        num_atom_lines = 0
        for line in (data):
            match = re.match('^ATOM', line)
            if match:
                num_atom_lines += 1
                x = line[30:37].strip()
                y = line[38:45].strip()
                z = line[46:53].strip()

                if (not (isFloat(x)) or not (isFloat(y)) or not (isFloat(z))):
                    return 0

        if num_atom_lines == 0:
            return 0

    return 1


def writeDDGasBfactor(input_pdb, list_result):
    filename = input_pdb
    ## change datatype of DDG(string to float)
    list_result['DDG'] = list_result['DDG'].astype('float')

    ## grouping and averaging the 'DDG' value
    list_result = list_result.groupby(['chain', 'position'])['DDG'].mean().round(3).reset_index()
    list_result.index += 1

    mut_list = list_result[['chain', 'position', 'DDG']].values.tolist()

    initialised_pdb = []
    ## bfactor substitution part1 (bfactor to 0)
    with open(input_pdb, 'r') as input_pdb:
        for each in input_pdb.readlines():
            if each.strip()[0:4] == 'ATOM':
                initialised_pdb.extend([each.strip()[:60] + str(' 0.000')])

    remove_list = []
    sub_list = []

    ### generate remove_list and atoms with DDG in bfactor position
    for x in range(0, len(initialised_pdb)):

        chain = initialised_pdb[x].strip()[21:22].strip()
        resnum = initialised_pdb[x].strip()[22:27].strip()
        # print(resnum)

        for eeach in mut_list:
            if chain == eeach[0] and resnum == eeach[1]:
                sub_list.extend([initialised_pdb[x].strip()[:60] + str(eeach[2])])
                remove_list.append(x)

    # Remove atoms based on remove_list
    for each in sorted(remove_list, reverse=True):
        del initialised_pdb[each]

    ## Merge two list into one list
    whole_new = initialised_pdb + sub_list
    output = filename[:-4] + '_ddG_added.pdb'

    ## Write new PDB file
    with open(output, 'w') as output_pdb:
        wr = csv.writer(output_pdb, quoting=csv.QUOTE_NONE, delimiter='\n')
        wr.writerow(whole_new)
        output_pdb.close()

    ## rearranging

    cmd = "sort -gk 2 " + output + " > " + output[:-4] + "reordered.pdb"
    os.system(cmd)

def generateHeatMapTable(pandas_table, output_folder):
    pandas_table['DDG'] = pandas_table['DDG'].round(2)
    group1 = pandas_table.groupby(['chain', 'position', 'wild'])

    result_pd = pd.DataFrame({'amino_acids':[],'chain_position':[],'DDG':[]})
    common_list=[]
    heatmap_y=[]
    for common, pre_group in group1:
        wild = common[2]
        dic = dict(zip(pre_group['mutant'], pre_group['DDG']))

        temp_aa_dic = {'A': '-', 'R': '-', 'N': '-', 'D': '-', 'C': '-', 'Q': '-', 'E': '-', 'G': '-', 'H': '-',
                       'I': '-', 'L': '-',
                       'K': '-', 'M': '-', 'F': '-', 'P': '-', 'S': '-', 'T': '-', 'W': '-', 'Y': '-', 'V': '-'}

        temp_aa_dic.update(dic)
        temp_aa_dic[wild]='wild'

        common_list.extend([common[0]+common[1]])
        heatmap_y.extend([common[0]+':'+common[1]])

        for key,value in temp_aa_dic.items():

            result_pd = result_pd.append({'chain_position':''.join(common[:-1]),'amino_acids':key,'DDG':value},ignore_index=True)

        temp_aa_dic.clear()
    result_pd.index +=1
    y_remap=dict()
    for x in range(0,len(common_list)):
        y_remap[common_list[x]]=x
    aa_remap = {'A': 0, 'R': 1, 'N': 2, 'D': 3, 'C': 4, 'Q': 5, 'E': 6, 'G': 7, 'H': 8, 'I': 9, 'L': 10, 'K': 11,
                   'M': 12, 'F': 13, 'P': 14, 'S': 15, 'T': 16, 'W': 17, 'Y': 18, 'V': 19}

    result_pd['y'] = result_pd['chain_position']
    result_pd['y'].replace(y_remap,inplace=True)
    result_pd['x'] = result_pd['amino_acids']
    result_pd['x'].replace(aa_remap,inplace=True)

    ## # xAxis: Not needed because xAxis can be pre-defined.
    ## # yAxis: Should be defined based on 'chain_position'

    ## # This is for JavaScript Data
    final_list=[]
    #print(result_pd[['x','y','DDG']].to_csv(header=False, index=False).split('\n'))
    for each in result_pd[['x','y','DDG']].to_csv(header=False, index=False).split('\n'):
        if len(each) > 1:
            final_list.append([int(each.split(',')[0]),int(each.split(',')[1]),each.split(',')[2]])

    # Obsolete
    # output_path = os.path.join(output_folder,'heatmap.csv')
    # result_pd.to_csv(output_path,index=False)
    # print(group1.describe().to_string())


    return heatmap_y, final_list


def pml2pse(pml_file):
    session_file = pml_file.replace('.pml','.pse')
    if not os.path.isfile(session_file):
        with open(pml_file, "a") as f:
            f.write("save {}".format(session_file))
        os.system('{}/bin/pymol -qcr {}'.format(app.config['VIRTUALENV_PATH'],pml_file))
        #output = check_output(['pymol', '-qc', pml_file])
    return session_file

def job_still_processing(job_id):
    job = None
    job = queue_single.fetch_job(job_id)
    return not job.is_failed

def getInterfaceResidueAtoms(input_pdb, protein_chains, nucleic_acid_chains, cutoff):
    try:
        cutoff = float(cutoff)
        # print("Processing ...:"+input_pdb)
        protein_chain_list=list(protein_chains.strip())
        nucleic_acid_chain_list=list(nucleic_acid_chains.strip())

        p = PDBParser()
        structure = p.get_structure('input',input_pdb)
        model = structure[0]

        Pro_outlist_al=[]
        NA_outlist_al=[]

        Pro_outlist_rl=[]
        NA_outlist_rl=[]

        for each_Prochain in protein_chain_list:
            for Proresidue in model[each_Prochain]:
                for atom_of_Prochain in Proresidue:
                    for each_NAchain in nucleic_acid_chain_list:
                        for NAresidue in model[each_NAchain]:
                            for atom_of_NAchain in NAresidue:
                                temp_dist = atom_of_Prochain-atom_of_NAchain
                                if temp_dist <= cutoff:
                                    # Pro_outlist_al.append([Proresidue.get_full_id()[2] + ","+(str(Proresidue.get_full_id()[3][1]) + Proresidue.get_full_id()[3][2]).strip()+ ","+(atom_of_Prochain.get_full_id()[4][0]).strip()])
                                    # NA_outlist_al.append([NAresidue.get_full_id()[2] + ","+(str(NAresidue.get_full_id()[3][1]) + NAresidue.get_full_id()[3][2]).strip()+ ","+(atom_of_NAchain.get_full_id()[4][0]).strip()])
                                    Pro_outlist_rl.append([Proresidue.get_full_id()[2] + "|"+(str(Proresidue.get_full_id()[3][1]) + Proresidue.get_full_id()[3][2]).strip()])
                                    NA_outlist_rl.append([NAresidue.get_full_id()[2] + "|"+(str(NAresidue.get_full_id()[3][1]) + NAresidue.get_full_id()[3][2]).strip()])

        # Pro_result_al = set(tuple(row)[0] for row in Pro_outlist_al)
        # NA_result_al = set(tuple(row)[0] for row in NA_outlist_al)

        Pro_result_rl = set(tuple(row)[0] for row in Pro_outlist_rl)
        NA_result_rl = set(tuple(row)[0] for row in NA_outlist_rl)

        # result_al = list(Pro_result_al)+list(NA_result_al)
        # outputfilename_al = os.path.join(os.path.split(input_pdb)[0],os.path.split(input_pdb)[1][:-4]+"_atom_level.txt")

        # with open(outputfilename_al,'w') as output_al:
        #     for each in result_al:
        #         output_al.write("%s\n" %each)
        # output_al.close()

        result_rl = list(Pro_result_rl)+list(NA_result_rl)
        # outputfilename_rl = os.path.join(os.path.split(input_pdb)[0],os.path.split(input_pdb)[1][:-4]+"_residue_level.txt")
        #print(result_rl)
        return result_rl
        # with open(outputfilename_rl,'w') as output_rl:
        #     for each in result_rl:
        #         output_rl.write("%s\n" %each)
        # output_rl.close()
    except:
        print("Error Occured :"+input_pdb)

def cleanner(working_dir):
    for fname in os.listdir(working_dir):

        if fname == 'Summary_forward_AC.fxout' or fname == 'Summary_reverse_AC.fxout':
            os.remove(os.path.join(working_dir, fname))

        if fname == 'temp1_infos.p' or fname == 'temp2_infos.p':
            os.remove(os.path.join(working_dir, fname))

def mm_checker(multiple_mutation_query):
    # Convert all lowercase characters in a list into uppercase characters.
    query_list = multiple_mutation_query.upper().split(';')
    #import pdb; pdb.set_trace()
    # 0. Check Very first/last characters.
    if not multiple_mutation_query[0].isalpha() or not multiple_mutation_query[-1].isalpha():
        return False,"> Wrong input format given: {}".format(multiple_mutation_query)
    # 1. delimiter/symbols
    elif set('[~!@#$%^&*()_+{}":,\']+$').intersection(set(multiple_mutation_query.upper())):
        return False,"> Wrong delimiter given: {}".format(multiple_mutation_query)
    elif len(query_list) == 0:
        return False,"> Check delimiter or the number of mutations: {}".format(multiple_mutation_query)
    # 2. number of mutation (2<= mutation < 10)
    elif len(query_list) > 9:
        return False,"> Check the number of mutations: {}".format(multiple_mutation_query)
    else:
        temp_list = list()
        # 3. size of each mutation
        for each in query_list:
            # 4. for each mutation: check min. length of information
            if len(each) < 3:
                return False,"> Check your mutation information: {}".format(multiple_mutation_query)
            # 5. for each mutation: compare wild-type and mutant
            elif each[0] == each[-1]:
                return False,"> At least one mutation has same wild-type and mutant: {}".format(multiple_mutation_query)
            else:
                temp_list.append(each[1:-1])

                # for each mutation: check wild-type and chain identifier
                try:
                    if isinstance(int(each[0]),int) or isinstance(int(each[1]),int) :
                        return False,"> At least one mutation has no wild-type or chain information: {}".format(multiple_mutation_query)
                except:
                    pass

                # for each mutation: check whether there is mutant
                try:
                    if isinstance(int(each[-1]),int):
                        # print("No mutant information was given")
                        return False,"> At least one mutation has no mutant information: {}".format(multiple_mutation_query)
                except:
                    pass

               # #check whether there are more characters than actually it needs.
               # try:
               #     # extract position and check wether there is any string
               #     position = each[2:-1]
               #     if isinstance(int(position),int):
               #         pass
               # except:
               #     return False,"> At least one mutation has chracter in position: {}".format(multiple_mutation_query)
        if len(temp_list) != len(list(set(temp_list))):
            return False, "> You have more than one mutation at the same position: {}".format(multiple_mutation_query)

        return True,"no error found"

def query_converter(mutation_list):
    temp_mutation_list = re.sub('\s+','',mutation_list)
    temp_list = list()
    new_mutation_list = str()

    for each in temp_mutation_list.split(';'):
        temp_list.append(each[1]+each[0]+each[2:])
    new_mutation_list = ';'.join(temp_list)

    return new_mutation_list

def query_converter_for_file(mutation_file):
    try:
        input_file = open(mutation_file,'r').readlines()
        new_mutation_list = list()
        for each in input_file:
            new_mutation_list.append(query_converter(each))
        for each in new_mutation_list:
            query_error,query_error_msg = mm_checker(each)
            if query_error == False:
                return query_error,query_error_msg
        with open(mutation_file,'w+') as output:
            for each in new_mutation_list:
                output.write(each+'\n')
        output.close()
        return True,"No problem"
    except:
        return False,"Your mutation information have a format problem."

if __name__=="__main__":
    print("Hello, you've just run utils.py directly!")
