from fabric import task, Connection

host = '128.250.89.201'
user = 'ymyung'
key_filename = '/home/ymyung/.ssh/id_rsa'


PROJECT_DIR = "/var/www/mmcsm_na"

@task(hosts=[host])
def deploy(c):
    with Connection(host=host, user=user, connect_kwargs={'key_filename':key_filename}) as conn:
        with conn.cd(PROJECT_DIR):
            conn.run("git fetch origin")
            conn.run("git merge origin/master")
            conn.run('/home/ymyung/anaconda2/bin/conda env update --file requirements.yml')
            conn.run('sed -i "s/XXXX/MODELIRANJE/" /home/ymyung/anaconda2/envs/mmcsm_na/lib/modeller-9.21/modlib/modeller/config.py')
        conn.run('sudo apachectl -k graceful', pty=True)
        print("DONE")
