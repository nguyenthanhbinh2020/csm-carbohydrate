#!/usr/bin/env python
from csm_carbohydrate_app import app
if __name__ == '__main__':
    app.run(host='0.0.0.0', debug=True, port=6677)
